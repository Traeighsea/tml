#!/bin/bash

# Note: If you want to use doxybook 2, found here then you can pass in "doxybook" as the first argument.
#  This will generate docs in markdown
#  https://github.com/matusnovak/doxybook2

# Cd into the location of the shell script
cd "$(dirname "$0")"
pwd

# Yeet the old docs
rm -rf ./docs/doxyout/*

# If graphs passed in, enable DOT in the Doxyfile
if [ "$1" = "graphs" ] || [ "$1" = "all" ]
then
   time ( cat Doxyfile ; echo "HAVE_DOT = YES" ) | doxygen -

# Else run doxygen with default settings
else
   time doxygen Doxyfile
fi

# If doxybook passed in generate the doxybook output
if [ "$1" = "doxybook" ] || [ "$1" = "all" ]
then
   # Copy the files over
   mkdir -p ./docs/doxyout/doxybook
   rm -rf ./docs/doxyout/doxybook/*
   cp -r ./docs/doxytheme/mkdocs/* ./docs/doxyout/doxybook

   # Run doxybook
   doxybook2 --input docs/doxyout/xml --output docs/doxyout/doxybook/docs --config docs/doxytheme/mkdocs/.doxybook/config.json

   # If you want to read the markdown files with the readthedocs format with links you can host it with the mkdocs cli
fi