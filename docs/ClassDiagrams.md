# Class Diagrams

The design consists of three distinct categories of objects; Data Objects, Informational Objects, and Container Objects. Each one of these layers add a little bit more information such that high level decisions can be made by the user. The data objects, for the most part, only hold the data such as the pitch information or duration of a note. The informational objects contain information with how the data can be analyzed in relation to one another such as what notes belong in a key or scale. The container objects hold this data for structural analysis such as with a motif or use in a section of a song.

## Data Objects

These classes are the underlying data of everything in the library. Composition is preferred and inheritance is avoided, though at the cost of duplicated functions.

```mermaid
classDiagram

   class Note {
      -Pitch pitch_
      -Duration duration_
      -Dynamic dynamic_
   }
   Note *-- Pitch
   Note *-- Duration
   Note *-- Dynamic

   class Pitch {
      -PitchName pitch_name_
      -Octave octave_
   }
   Pitch *-- PitchName
   Pitch *-- Octave

   class PitchName {
      -PitchLetter pitch_letter_
      -Accidental accidental_
   }
   PitchName *-- PitchLetter
   PitchName *-- Accidental

   class PitchLetter {
      <<enumeration : int>>
      A
      B
      C
      D
      E
      F
      G
   }
    
   class Accidental {
      <<enumeration : int>>
      DoubleFlat
      Flat
      Natural
      Sharp
      DoubleSharp
   }

   class Octave {
      <<typedef int>>
   }

   class Duration {
      -NoteValue note_value_
      -Dot dot_modifier_
      -Tuplet tuplet_
   }
   Duration *-- NoteValue
   Duration *-- Dot
   Duration *-- Tuplet

   class NoteValue {
      <<enumeration : int>>
      Whole
      Half
      Quarter
      Eighth
      Sixteenth
      ThirtySecond
      SixtyFourth
      HundredTwentyEighth
      TwoHundredFiftySixth
      DoubleWhole
      Longa
      Maxima
   }

   class Dot {
      <<enum : unsigned int>>
      None
      Single
      Double
      Triple
      Quadruple
   }

   class Tuplet {
      <<enum : unsigned int>>
      Duplet
      Triplet
      Quadruplet
      Quintuplet
      Sextuplet
      Septuplet
      Octuplet
   }

   class Dynamic {
      -DynamicMarking dynamic_marking_
   }
   Dynamic *-- DynamicMarking

   class DynamicMarking {
      <<enum : unsigned int>>
      fff
      ff
      f
      mf
      m
      mp
      p
      pp
      ppp
      none
      Fortississimo
      Fortissimo
      Forte
      MezzoForte
      Mezzo
      MezzoPiano
      Piano
      Pianissimo
      Pianississimo
   }

```

```mermaid
classDiagram

   class PitchClass {
      -unsigned int pc_
   }

   class IntervalClass {
      -unsigned int ic_
   }

   class Instrument {
      <<enum : unsigned int>>
   }

   class NoteOnset {
      -int beat_
      -bool has_duration_offset_
      -Duration duration_offset_
   }
   NoteOnset *-- Duration

   class ScientificNote {
      -double frequency_
      -double time_
      -double decibels_
   }

```

## Relational Objects

These data types contain high level music theory information as they relate to one another. The general form consists of Formula classes that describe the relationship of adjacent notes, as well as Dictionary classes that store and retain a collection of registered formulas to be applied in whatever arbitrary context. The smallest unit for these purposes are Intervals and Degrees.

These are still very much a concept and very subject to change.

```mermaid
classDiagram

   class Interval {
      -std::string name_
      -int steps_
      -bool ascending_
      -IntervalQuality interval_quality_
      -int interval_degree_
   }
   Interval *-- IntervalQuality

   class IntervalQuality {
      <<enum : int>>
      Minor
      Major
      Diminished
      Augmented
      Perfect
   }
   
   class Rhythm {
      -std::string name_
      -std::vector~Duration~ duration_sequence_
   }
   Rhythm *-- Duration

   class RhythmDictionary {
      -std::map~std::string__Rhythm~ dictionary_
   }
   RhythmDictionary *-- Rhythm
```

```mermaid
classDiagram

   class Scale {
      -std::string name_
      -std::vector~Pitch~ pitch_sequence_
   }
   Scale *-- Pitch

   class ScaleDegree {
      -int degree_
      -Accidental accidental_
   }
   ScaleDegree *-- Accidental

   class ScaleFormula {
      -std::string name_
      -std::vector~ScaleDegree~ formula_
   }
   ScaleFormula *-- ScaleDegree

   class ScaleDictionary {
      -std::map~std::string__ScaleFormula~ dictionary_
   }
   ScaleDictionary *-- ScaleFormula

   class Chord {
      -std::string name_
      -PitchName root_
      -std::vector~Pitch~ pitches_
   }
   Chord *-- PitchName
   Chord *-- Pitch

   class ChordDegree {
      -int degree_
      -Accidental accidental_
   }
   ChordDegree *-- Accidental

   class ChordFormula {
      -std::string name_
      -std::vector~ChordDegree~ formula_
   }
   ChordFormula *-- ChordDegree

   class ChordDictionary {
      -std::map~std::string__ScaleFormula~ dictionary_
   }
   ChordDictionary *-- ScaleFormula

   class ChordProgression {
      -std::vector~Chord~ progression_
   }
   ChordProgression *-- Chord

   class ChordProgressionFormula {
      -std::string name_
      -std::vector~ChordFormula~ formula_
   }
   ChordProgressionFormula *-- ChordFormula

   class ChordProgressionDictionary {
      -std::map~std::string__ChordProgressionFormula~ dictionary_
   }
   ChordProgressionDictionary *-- ChordProgressionFormula

```

```mermaid
classDiagram

   class PitchSet {
      -std::array~bool__12~ pitches_
   }

   class TimeSignature {
      -unsigned int beats_
      -unsigned int note_value_
   }
    
   class KeySignature {
      -int accidentals_
      -std::array~PitchName__7~ notes_
   }
   KeySignature *-- PitchName

   class Tempo {
      <<enum : unsigned int>>
      Larghissimo
      Adagissimo
      Grave
      Largo
      Lento
      Larghetto
      Adagio
      Adagietto
      Andante
      Andantino
      MarciaModerato
      AndanteModerato
      Moderato
      Allegretto
      AllegroModerato
      Allegro
      MoltoAllegro
      Vivace
      Vivacissimo
      Allegrissimo
      Presto
      Prestissimo
   }

```

## Organizational Objects

Container classes for storing music information that could theoretically be played by a human or exported as a song. The Composition object holds the data for any arbitrary song while the Group classes contain descriptions of how groups of notes are organized. For the purposes of these objects Measures (or Bars) are the smallest unit of organization. Other objects agnostic to measure/bar line division are also included such as Sequence and Motif.

These are still very much a high level concept and very subject to change.

```mermaid
classDiagram

   class Composition {
      -std::string name_
      -std::vector~Part~ parts_
      -double bpm_
   }
   Composition *-- Part

   class Part {
      -Voice voice_
      -std::vector~Measure~ measures_
   }
   Part *-- Voice
   Part *-- Measure

   class Measure {
      -KeySignature key_signature_
      -TimeSignature time_signature_
      -std::vector~NoteOnset__Note~ notes_
   }
   Measure *-- KeySignature
   Measure *-- TimeSignature
   Measure *-- NoteOnset
   Measure *-- Note

   class Voice {
      -std::string name_
      -Instrument instrument_
   }
   Voice *-- Instrument

   class Piece {
      Composition composition_
      std::vector<StructuralGroup> structural_groups_
   }
   Piece <|-- Composition
   Piece *-- StructuralGroup

   class StructuralGroupLabel {
      <<enumeration : int>>
      Other
      Section
      SubSection
      Theme
      Variation
      Intro
      Verse
      Chorus
      Bridge
      Outro
      Module
      Cycle
   }

   class StructuralGroup {
      -std::string name_
      -StructuralGroupLabel group_label_
      -std::map::~int__std::vector::Measure&~ measures_
      -std::map::~int__Voice&~ voice_mapping_
      -std::vector~StructuralGroup~ structural_sub_groups_
      -std::vector~NoteGroup~ note_sub_groups_

   }
   StructuralGroup *-- StructuralGroupLabel
   StructuralGroup *-- Measure
   StructuralGroup *-- Voice
   StructuralGroup *-- StructuralGroup
   StructuralGroup *-- NoteGroup

   class NoteGroupLabel {
      <<enumeration : int>>
      Other
      Phrase
      SubPhrase
      Period
      Sentence
      Antecedent
      Consequence
      Continuation
      Cadential
      BasicIdea
      CompoundBasicIdea
   }

   class NoteGroup {
      -std::string name_
      -NoteGroupLabel group_label_
      -std::vector~Measure&~ measures_
      -std::vector~NoteGroup~ note_sub_groups_
   }
   NoteGroup *-- NoteGroupLabel
   NoteGroup *-- Measure
   NoteGroup *-- NoteGroup

   class Form {

   }

```

```mermaid
classDiagram

   class Sequence {
      -std::string name_
      -std::vector~Note~ sequence_
   }
   Sequence *-- Note

   class Motif {
      -std::string name_
      -std::vector::~NoteOnset__Note~ notes_
   }
   Motif *-- Note
   Motif *-- NoteOnset

```
