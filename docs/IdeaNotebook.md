# Ideas

- Add music unicode characters and include a default music font that can be set by the user
- Add an output system with a configurable directory to output to the user whenever issues may occur based on the given information or if there say isn't enough information to do something
- Add string hashes to many of the current enum structure
