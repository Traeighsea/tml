#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestVoice, TestVoiceTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Voice test_voice;
   std::cout << "    Voice: " << test_voice << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   test_voice.SetName("");
   test_voice.SetInstrument(tml::Instrument::ElectricGrandPiano);

   std::cout << "    Voice: " << test_voice << std::endl;

   test_voice.SetName("Destroyer of Worlds 1");
   test_voice.SetInstrument(tml::Instrument::Glockenspiel);

   std::cout << "    Voice: " << test_voice << std::endl;

   EXPECT_TRUE(false);
}
