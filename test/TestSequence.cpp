#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestSequence, TestSequenceTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Sequence test_sequence;
   std::cout << "    Sequence: " << test_sequence << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   test_sequence.AppendNote({{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Eighth, 0, 0},
                             {tml::DynamicMarking::m}});

   test_sequence.AppendNote({{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Eighth, 0, 0},
                             {tml::DynamicMarking::m}});

   test_sequence.AppendNote({{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Eighth, 0, 0},
                             {tml::DynamicMarking::m}});

   test_sequence.AppendNote({{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Quarter, 0, 0},
                             {tml::DynamicMarking::p}});

   test_sequence.AppendNote({{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Eighth, 0, 0},
                             {tml::DynamicMarking::m}});

   test_sequence.AppendNote({{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                             {tml::NoteValue::Quarter, 1, 0},
                             {tml::DynamicMarking::m}});

   test_sequence.PrependNote({{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                              {tml::NoteValue::Eighth, 0, 0},
                              {tml::DynamicMarking::m}});

   std::cout << "    Sequence: " << test_sequence << std::endl;

   test_sequence.Remove(4);
   test_sequence.Insert(4, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                            {tml::NoteValue::Quarter, 0, 0},
                            {tml::DynamicMarking::f}});

   std::cout << "    Sequence: " << test_sequence << std::endl;

   // Test bounds
   std::cout << "Testing limit checking: " << std::endl;

   auto note = test_sequence.GetNote(8);

   std::cout << "    Note: " << note << std::endl;

   note = test_sequence.GetNote(-8);

   std::cout << "    Note: " << note << std::endl;

   test_sequence.Clear();
   test_sequence.Remove(2);

   tml::Note note_error = test_sequence.GetNote(12, false);

   std::cout << "    Note: " << note_error << std::endl;

   std::cout << "    Sequence: " << test_sequence << std::endl;

   EXPECT_TRUE(false);
}
