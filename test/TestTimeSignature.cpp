#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestTimeSignature, TestTimeSignatureTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::TimeSignature test_time_signature;

   std::cout << "    Time Signature: " << test_time_signature << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   test_time_signature.SetBeats(7);
   test_time_signature.SetNoteValue(8);

   std::cout << "    Time Signature: " << test_time_signature << std::endl;

   test_time_signature.SetBeats(5);
   test_time_signature.SetNoteValue(tml::NoteValue::Quarter);

   std::cout << "    Time Signature: " << test_time_signature << std::endl;

   EXPECT_TRUE(false);
}
