#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestScaleFormula, TestScaleFormulaTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ScaleFormula test_scale_formula;

   std::cout << "    ScaleFormula: " << test_scale_formula << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::ScaleDegree scale_degree;

   test_scale_formula.AddScaleDegree(scale_degree);
   scale_degree.SetDegree(2);
   test_scale_formula.AddScaleDegree(scale_degree);
   scale_degree.SetDegree(3);
   test_scale_formula.AddScaleDegree(scale_degree);

   std::cout << "    ScaleFormula: " << test_scale_formula << std::endl;

   test_scale_formula.RemoveScaleDegree(2);

   std::cout << "    Remove: " << test_scale_formula << std::endl;

   test_scale_formula.Clear();

   std::cout << "    Clear: " << test_scale_formula << std::endl;

   // Bounds Checking (errors are good)
   std::cout << "Bounds checking (errors are good): " << test_scale_formula << std::endl;

   scale_degree.SetDegree(1);
   test_scale_formula.AddScaleDegree(scale_degree);

   std::cout << "    Remove (min-1): " << std::endl;
   test_scale_formula.RemoveScaleDegree(-1);

   std::cout << "    Remove (max+1): " << std::endl;
   test_scale_formula.RemoveScaleDegree(7);

   test_scale_formula.Clear();

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   for (int i = 0; i < 7; i++) {
      scale_degree.SetDegree(i + 1);
      scale_degree.SetAccidental(tml::Accidental::Natural);
      test_scale_formula.AddScaleDegree(scale_degree);
   }
   test_scale_formula.SetName("Major");

   std::cout << "    ScaleFormula: " << test_scale_formula << std::endl;

   /* TODO: Fix intervals
   std::vector<tml::Interval> intervals = test_scale_formula.GetIntervalDistances();

   for (auto it : intervals)
      std::cout << "    Interval: " << it << std::endl;

   test_scale_formula.Clear();

   for (int i = 0; i < 7; i++)
   {
      scale_degree.SetDegree(i + 1);
      // Essentially build a C minor scale
      if (i == 2 || i == 5 || i == 6)
         scale_degree.SetAccidental(tml::Accidental::Flat);
      else
         scale_degree.SetAccidental(tml::Accidental::Natural);
      test_scale_formula.AddScaleDegree(scale_degree);
   }

   std::cout << "    ScaleFormula: " << test_scale_formula << std::endl;
   */

   tml::Scale test_scale;
   test_scale = test_scale_formula.ApplyScale(
         tml::PitchName(tml::PitchLetter::G, tml::Accidental::Natural));

   std::cout << "    ApplyScale(): " << test_scale << std::endl;

   // build the formula for the minor scale
   test_scale_formula.Clear();
   for (int i = 0; i < 7; i++) {
      scale_degree.SetDegree(i + 1);
      if (i == 2 || i == 5 || i == 6)
         scale_degree.SetAccidental(tml::Accidental::Flat);
      else
         scale_degree.SetAccidental(tml::Accidental::Natural);
      test_scale_formula.AddScaleDegree(scale_degree);
   }
   test_scale_formula.SetName("Minor");

   std::cout << "    ScaleFormula: " << test_scale_formula << std::endl;

   test_scale.Clear();
   test_scale = test_scale_formula.ApplyScale(
         tml::PitchName(tml::PitchLetter::G, tml::Accidental::Natural));

   std::cout << "    ApplyScale(): " << test_scale << std::endl;

   EXPECT_TRUE(false);
}
