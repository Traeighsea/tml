#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestScaleDegree, TestScaleDegreeTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ScaleDegree test_scale_degree;

   std::cout << "    ScaleDegree: " << test_scale_degree << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   test_scale_degree.SetDegree(9);
   test_scale_degree.SetAccidental(tml::Accidental::Sharp);

   std::cout << "    ScaleDegree: " << test_scale_degree << std::endl;

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   tml::Scale test_scale;

   auto pitch_letters_C_major = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                                 tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                                 tml::PitchLetter::B};

   auto pitches_Bb_major = {tml::Pitch(tml::PitchLetter::B, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::C, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::E, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::F, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::A, tml::Accidental::Natural, 5)};

   for (auto it : pitch_letters_C_major) {
      test_scale.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
   }
   test_scale.SetName("Major");

   test_scale_degree.SetAccidental(tml::Accidental::Natural);
   for (int i = 1; i <= 7; i++) {
      test_scale_degree.SetDegree(i);
      tml::Pitch test_pitch = test_scale_degree.GetPitchFromScale(test_scale);
      std::cout << "    ScaleDegree: " << test_scale_degree << " -> " << test_pitch << std::endl;
   }

   test_scale.Clear();
   for (auto it : pitches_Bb_major) {
      test_scale.AddPitch(it);
   }
   test_scale.SetName("Major");

   for (int i = 1; i <= 7; i++) {
      test_scale_degree.SetDegree(i);
      std::cout << "    ScaleDegree: " << test_scale_degree << " -> "
                << test_scale_degree.GetPitchNameFromScale(test_scale) << std::endl;
   }

   test_scale_degree.SetAccidental(tml::Accidental::Flat);
   for (int i = 1; i <= 7; i++) {
      test_scale_degree.SetDegree(i);
      std::cout << "    ScaleDegree: " << test_scale_degree << " -> "
                << test_scale_degree.GetPitchFromScale(test_scale) << std::endl;
   }

   EXPECT_TRUE(false);
}
