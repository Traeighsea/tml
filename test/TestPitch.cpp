#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestPitch, TestPitchTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Pitch test_pitch = tml::Pitch();

   std::cout << "    Pitch: " << test_pitch << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::PitchName n = tml::PitchName(tml::PitchLetter::B, tml::Accidental::Flat);
   tml::Octave o = 6;

   test_pitch.SetPitchName(n);
   test_pitch.SetOctave(o);

   std::cout << "    Pitch: " << test_pitch << std::endl;

   // Test limit checking
   std::cout << "Testing limit checking: " << std::endl;

   test_pitch.SetOctave(-2);

   std::cout << "    Pitch: " << test_pitch << std::endl;

   test_pitch.SetOctave(10);

   std::cout << "    Pitch: " << test_pitch << std::endl;

   EXPECT_TRUE(false);
}
