# Data Objects
add_executable(TestPitchName TestPitchName.cpp)
target_link_libraries(TestPitchName tml GTest::gtest_main)

add_executable(TestPitch TestPitch.cpp)
target_link_libraries(TestPitch tml GTest::gtest_main)

add_executable(TestDuration TestDuration.cpp)
target_link_libraries(TestDuration tml GTest::gtest_main)

add_executable(TestDynamic TestDynamic.cpp)
target_link_libraries(TestDynamic tml GTest::gtest_main)

add_executable(TestNote TestNote.cpp)
target_link_libraries(TestNote tml GTest::gtest_main)

# TODO
# add_executable(TestPitchClass TestPitchClass.cpp)
# target_link_libraries(TestPitchClass tml GTest::gtest_main)

# TODO
# add_executable(TestIntervalClass TestIntervalClass.cpp)
# target_link_libraries(TestIntervalClass tml GTest::gtest_main)
add_executable(TestNoteOnset TestNoteOnset.cpp)
target_link_libraries(TestNoteOnset tml GTest::gtest_main)

# Relational Objects
add_executable(TestInterval TestInterval.cpp)
target_link_libraries(TestInterval tml GTest::gtest_main)

add_executable(TestScale TestScale.cpp)
target_link_libraries(TestScale tml GTest::gtest_main)

add_executable(TestChord TestChord.cpp)
target_link_libraries(TestChord tml GTest::gtest_main)

add_executable(TestScaleDegree TestScaleDegree.cpp)
target_link_libraries(TestScaleDegree tml GTest::gtest_main)

add_executable(TestScaleFormula TestScaleFormula.cpp)
target_link_libraries(TestScaleFormula tml GTest::gtest_main)

add_executable(TestScaleDictionary TestScaleDictionary.cpp)
target_link_libraries(TestScaleDictionary tml GTest::gtest_main)

add_executable(TestChordDegree TestChordDegree.cpp)
target_link_libraries(TestChordDegree tml GTest::gtest_main)

add_executable(TestChordFormula TestChordFormula.cpp)
target_link_libraries(TestChordFormula tml GTest::gtest_main)

add_executable(TestChordDictionary TestChordDictionary.cpp)
target_link_libraries(TestChordDictionary tml GTest::gtest_main)

add_executable(TestKeySignature TestKeySignature.cpp)
target_link_libraries(TestKeySignature tml GTest::gtest_main)

add_executable(TestTimeSignature TestTimeSignature.cpp)
target_link_libraries(TestTimeSignature tml GTest::gtest_main)

# Organizational Objects
add_executable(TestSequence TestSequence.cpp)
target_link_libraries(TestSequence tml GTest::gtest_main)

add_executable(TestMotif TestMotif.cpp)
target_link_libraries(TestMotif tml GTest::gtest_main)

add_executable(TestMeasure TestMeasure.cpp)
target_link_libraries(TestMeasure tml GTest::gtest_main)

add_executable(TestVoice TestVoice.cpp)
target_link_libraries(TestVoice tml GTest::gtest_main)

add_executable(TestPart TestPart.cpp)
target_link_libraries(TestPart tml GTest::gtest_main)

add_executable(TestComposition TestComposition.cpp)
target_link_libraries(TestComposition tml GTest::gtest_main)

# TODO
# add_executable(TestNoteGroup TestNoteGroup.cpp)
# target_link_libraries(TestNoteGroup tml GTest::gtest_main)

# TODO
# add_executable(TestStructuralGroup TestStructuralGroup.cpp)
# target_link_libraries(TestStructuralGroup tml GTest::gtest_main)

# TODO
# add_executable(TestPiece TestPiece.cpp)
# target_link_libraries(TestPiece tml GTest::gtest_main)

# Test Examples
add_executable(TestGuitarFretboard TestGuitarFretboard.cpp)
target_link_libraries(TestGuitarFretboard tml GTest::gtest_main)

add_executable(TestJson TestJson.cpp)
target_link_libraries(TestJson tml GTest::gtest_main)

add_executable(TestAll TestAllMain.cpp TestPitchName.cpp TestPitch.cpp TestDuration.cpp TestDynamic.cpp TestNote.cpp TestInterval.cpp TestScale.cpp TestChord.cpp TestScaleDegree.cpp TestScaleFormula.cpp TestScaleDictionary.cpp TestChordDegree.cpp TestChordFormula.cpp TestChordDictionary.cpp TestKeySignature.cpp TestTimeSignature.cpp TestSequence.cpp TestMotif.cpp TestMeasure.cpp TestVoice.cpp TestPart.cpp TestComposition.cpp TestGuitarFretboard.cpp TestJson.cpp)
target_link_libraries(TestAll tml GTest::gtest_main)
