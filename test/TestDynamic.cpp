#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestDynamic, TestDynamicTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Dynamic test_dynamic;

   std::cout << "Dynamic: " << test_dynamic << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::DynamicMarking dynamic_marking = tml::DynamicMarking::Fortissimo;

   test_dynamic.SetDynamicMarking(dynamic_marking);

   std::cout << "    Dynamic: " << test_dynamic << std::endl;

   // Test all combinations
   std::cout << "Testing all combinations: " << std::endl;

   auto dynamic_values = {tml::DynamicMarking::fff, tml::DynamicMarking::ff,
                          tml::DynamicMarking::f,   tml::DynamicMarking::mf,
                          tml::DynamicMarking::m,   tml::DynamicMarking::mp,
                          tml::DynamicMarking::p,   tml::DynamicMarking::pp,
                          tml::DynamicMarking::ppp, tml::DynamicMarking::none};

   auto dynamic_values_full = {tml::DynamicMarking::Fortississimo, tml::DynamicMarking::Fortissimo,
                               tml::DynamicMarking::Forte,         tml::DynamicMarking::MezzoForte,
                               tml::DynamicMarking::Mezzo,         tml::DynamicMarking::MezzoPiano,
                               tml::DynamicMarking::Piano,         tml::DynamicMarking::Pianissimo,
                               tml::DynamicMarking::Pianississimo};

   for (auto dynamic_values_it : dynamic_values) {
      test_dynamic.SetDynamicMarking(dynamic_values_it);
      std::cout << "    Dynamic: " << test_dynamic << std::endl;
   }

   tml::cfg::USE_SHORTENED_DYNAMIC_NAMES = false;

   for (auto dynamic_values_it : dynamic_values_full) {
      test_dynamic.SetDynamicMarking(dynamic_values_it);
      std::cout << "    Dynamic: " << test_dynamic << std::endl;
   }

   EXPECT_TRUE(false);
}
