#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestPitchName, TestPitchNameTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::PitchName test_pitch_name = tml::PitchName();

   std::cout << "    Pitch Name: " << test_pitch_name << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::PitchLetter letter = tml::PitchLetter::D;
   tml::Accidental accidental = tml::Accidental::Flat;

   test_pitch_name.SetPitchLetter(letter);
   test_pitch_name.SetAccidental(accidental);

   std::cout << "    Pitch Name: " << test_pitch_name << std::endl;

   test_pitch_name.Flatten();

   std::cout << "    Pitch Name: " << test_pitch_name << std::endl;

   test_pitch_name.AddAccidental(tml::Accidental::DoubleSharp);

   std::cout << "    Pitch Name: " << test_pitch_name << std::endl;

   // Test all combinations
   std::cout << "Testing all combinations: " << std::endl;

   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};

   auto accidentals = {tml::Accidental::DoubleFlat, tml::Accidental::Flat, tml::Accidental::Natural,
                       tml::Accidental::Sharp, tml::Accidental::DoubleSharp};

   tml::cfg::USE_UNICODE_ACCIDENTALS = false;
   for (auto it_letter : pitch_letters) {
      test_pitch_name.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         test_pitch_name.SetAccidental(it_accidental);
         std::cout << "    Pitch Name: " << test_pitch_name << std::endl;
      }
   }

   tml::cfg::USE_UNICODE_ACCIDENTALS = true;
   tml::cfg::USE_NATURALS_ALWAYS = true;
   for (auto it_letter : pitch_letters) {
      test_pitch_name.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         test_pitch_name.SetAccidental(it_accidental);
         std::cout << "    Pitch Name: " << test_pitch_name << std::endl;
      }
   }

   EXPECT_TRUE(false);
}
