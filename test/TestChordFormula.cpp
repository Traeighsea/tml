#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestChordFormula, TestChordFormulaTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ChordFormula test_chord_formula;

   std::cout << "    ChordFormula: " << test_chord_formula << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::ChordDegree chord_degree;

   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   std::cout << "    ChordFormula: " << test_chord_formula << std::endl;

   test_chord_formula.RemoveChordDegree(1);

   std::cout << "    Remove: " << test_chord_formula << std::endl;

   test_chord_formula.Clear();

   std::cout << "    Clear: " << test_chord_formula << std::endl;

   // Bounds Checking (errors are good)
   std::cout << "Bounds checking (errors are good): " << test_chord_formula << std::endl;

   chord_degree.SetDegree(1);
   test_chord_formula.AddChordDegree(chord_degree);

   std::cout << "    Remove (min-1): " << std::endl;
   test_chord_formula.RemoveChordDegree(-1);

   std::cout << "    Remove (max+1): " << std::endl;
   test_chord_formula.RemoveChordDegree(7);

   test_chord_formula.Clear();

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   test_chord_formula.SetName("Major");

   std::cout << "    ChordFormula: " << test_chord_formula << std::endl;

   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};
   auto accidentals = {tml::Accidental::Flat, tml::Accidental::Natural, tml::Accidental::Sharp};
   auto chord = tml::Chord();
   auto root = tml::Pitch();

   std::cout << "    ApplyChord(): " << std::endl;
   for (auto it_letter : pitch_letters) {
      root.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         root.SetAccidental(it_accidental);
         chord = test_chord_formula.ApplyChord(root);

         std::cout << "                  " << chord << std::endl;
      }
   }
   // build the formula for the minor chord
   chord.Clear();
   test_chord_formula.Clear();

   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   test_chord_formula.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_chord_formula.AddChordDegree(chord_degree);

   test_chord_formula.SetName("Minor");

   std::cout << "    ChordFormula: " << test_chord_formula << std::endl;

   std::cout << "    ApplyChord(): " << std::endl;
   for (auto it_letter : pitch_letters) {
      root.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         root.SetAccidental(it_accidental);
         chord = test_chord_formula.ApplyChord(root);

         std::cout << "                  " << chord << std::endl;
      }
   }

   /* TODO: Fix intervals
   std::vector<tml::Interval> intervals = test_chord_formula.GetIntervalDistances();

   for (auto it : intervals)
      std::cout << "    Interval: " << it << std::endl;

   test_chord_formula.Clear();

   for (int i = 0; i < 7; i++)
   {
      chord_degree.SetDegree(i + 1);
      // Essentially build a C minor chord
      if (i == 2 || i == 5 || i == 6)
         chord_degree.SetAccidental(tml::Accidental::Flat);
      else
         chord_degree.SetAccidental(tml::Accidental::Natural);
      test_chord_formula.AddChordDegree(chord_degree);
   }

   std::cout << "    ChordFormula: " << test_chord_formula << std::endl;
   */

   EXPECT_TRUE(false);
}
