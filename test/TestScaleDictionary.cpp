#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestScaleDictionary, TestScaleDictionaryTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ScaleDictionary test_scale_dictionary;

   std::cout << "    ScaleDictionary: " << test_scale_dictionary << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::ScaleDegree scale_degree;
   tml::ScaleFormula minor;
   tml::ScaleFormula major;

   // build the formula for the minor scale
   for (int i = 0; i < 7; i++) {
      scale_degree.SetDegree(i + 1);
      if (i == 2 || i == 5 || i == 6)
         scale_degree.SetAccidental(tml::Accidental::Flat);
      else
         scale_degree.SetAccidental(tml::Accidental::Natural);
      minor.AddScaleDegree(scale_degree);
   }
   minor.SetName("Test Minor");
   test_scale_dictionary.Register(minor);

   // build the formula for the Major scale
   scale_degree.SetAccidental(tml::Accidental::Natural);
   for (int i = 0; i < 7; i++) {
      scale_degree.SetDegree(i + 1);
      major.AddScaleDegree(scale_degree);
   }
   major.SetName("Test Major");
   test_scale_dictionary.Register(major);

   std::cout << "    ScaleFormula(Minor): " << minor << std::endl;
   std::cout << "    ScaleFormula(Major): " << major << std::endl;

   test_scale_dictionary.Register(minor);
   test_scale_dictionary.Register(major);

   std::cout << "    Register: " << test_scale_dictionary << std::endl;

   test_scale_dictionary.Unregister("Minor");

   std::cout << "    Unregister: " << test_scale_dictionary << std::endl;

   test_scale_dictionary.Reset();

   std::cout << "    Reset: " << test_scale_dictionary << std::endl;

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   tml::Scale scale;

   scale = test_scale_dictionary.GetScale(
         tml::PitchName(tml::PitchLetter::E, tml::Accidental::Flat), "Major");
   std::cout << "    GetScale: " << scale << std::endl;

   scale = test_scale_dictionary.GetScale(
         tml::PitchName(tml::PitchLetter::E, tml::Accidental::Flat), "Minor");
   std::cout << "    GetScale: " << scale << std::endl;

   scale = test_scale_dictionary.GetScale(
         tml::PitchName(tml::PitchLetter::F, tml::Accidental::Sharp), "Major");
   std::cout << "    GetScale: " << scale << std::endl;

   scale = test_scale_dictionary.GetScale(
         tml::PitchName(tml::PitchLetter::F, tml::Accidental::Sharp), "Minor");
   std::cout << "    GetScale: " << scale << std::endl;

   // Testing every registered scale, starting from C
   tml::PitchName pitch;

   std::vector<std::string> scale_list = test_scale_dictionary.GetRegisteredNames();

   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};

   pitch.SetAccidental(tml::Accidental::Natural);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All scales with root " << pitch << ": " << std::endl;
      for (auto it : scale_list) {
         scale = test_scale_dictionary.GetScale(pitch, it);
         std::cout << "    GetScale: " << scale << std::endl;
      }
   }

   pitch.SetAccidental(tml::Accidental::Flat);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All scales with root " << pitch << ": " << std::endl;
      for (auto it : scale_list) {
         scale = test_scale_dictionary.GetScale(pitch, it);
         std::cout << "    GetScale: " << scale << std::endl;
      }
   }

   pitch.SetAccidental(tml::Accidental::Sharp);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All scales with root " << pitch << ": " << std::endl;
      for (auto it : scale_list) {
         scale = test_scale_dictionary.GetScale(pitch, it);
         std::cout << "    GetScale: " << scale << std::endl;
      }
   }

   EXPECT_TRUE(false);
}
