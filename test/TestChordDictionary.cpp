#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestChordDictionary, TestChordDictionaryTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ChordDictionary test_chord_dictionary;

   std::cout << "    ChordDictionary: " << test_chord_dictionary << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::ChordDegree chord_degree;
   tml::ChordFormula minor;
   tml::ChordFormula major;

   // build the formula for the minor chord
   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   minor.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   minor.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   minor.AddChordDegree(chord_degree);

   minor.SetName("Test Minor");
   test_chord_dictionary.Register(minor);

   // build the formula for the Major chord
   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   major.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   major.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   major.AddChordDegree(chord_degree);

   major.SetName("Test Major");
   test_chord_dictionary.Register(major);

   std::cout << "    ChordFormula(Minor): " << minor << std::endl;
   std::cout << "    ChordFormula(Major): " << major << std::endl;

   test_chord_dictionary.Register(minor);
   test_chord_dictionary.Register(major);

   std::cout << "    Register: " << test_chord_dictionary << std::endl;

   test_chord_dictionary.Unregister("Minor");

   std::cout << "    Unregister: " << test_chord_dictionary << std::endl;

   test_chord_dictionary.Reset();

   std::cout << "    Reset: " << test_chord_dictionary << std::endl;

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   tml::Chord chord;

   chord = test_chord_dictionary.GetChord(
         tml::PitchName(tml::PitchLetter::E, tml::Accidental::Flat), "Major");
   std::cout << "    GetChord: " << chord << std::endl;

   chord = test_chord_dictionary.GetChord(
         tml::PitchName(tml::PitchLetter::E, tml::Accidental::Flat), "Minor");
   std::cout << "    GetChord: " << chord << std::endl;

   chord = test_chord_dictionary.GetChord(
         tml::PitchName(tml::PitchLetter::F, tml::Accidental::Sharp), "Major");
   std::cout << "    GetChord: " << chord << std::endl;

   chord = test_chord_dictionary.GetChord(
         tml::PitchName(tml::PitchLetter::F, tml::Accidental::Sharp), "Minor");
   std::cout << "    GetChord: " << chord << std::endl;

   // Testing every registered chord, starting from C
   tml::PitchName pitch;

   std::vector<std::string> chord_list = test_chord_dictionary.GetRegisteredNames();

   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};

   pitch.SetAccidental(tml::Accidental::Natural);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All chords with root " << pitch << ": " << std::endl;
      for (auto it : chord_list) {
         chord = test_chord_dictionary.GetChord(pitch, it);
         std::cout << "    GetChord: " << chord << std::endl;
      }
   }

   pitch.SetAccidental(tml::Accidental::Flat);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All chords with root " << pitch << ": " << std::endl;
      for (auto it : chord_list) {
         chord = test_chord_dictionary.GetChord(pitch, it);
         std::cout << "    GetChord: " << chord << std::endl;
      }
   }

   pitch.SetAccidental(tml::Accidental::Sharp);
   for (auto it_letter : pitch_letters) {
      pitch.SetPitchLetter(it_letter);
      std::cout << "All chords with root " << pitch << ": " << std::endl;
      for (auto it : chord_list) {
         chord = test_chord_dictionary.GetChord(pitch, it);
         std::cout << "    GetChord: " << chord << std::endl;
      }
   }

   EXPECT_TRUE(false);
}
