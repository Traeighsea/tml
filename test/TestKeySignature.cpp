#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestKeySignature, TestKeySignatureTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::KeySignature test_key;

   std::cout << "    KeySignature: " << test_key << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   test_key.Set(tml::PitchName(tml::PitchLetter::C, tml::Accidental::Natural), false);
   std::cout << "    KeySignature: " << test_key << std::endl;

   test_key.Set(4);
   std::cout << "    KeySignature: " << test_key << std::endl;

   // Test all combinations
   std::cout << "Testing all combinations: " << std::endl;

   tml::PitchName root;

   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};

   auto accidentals = {tml::Accidental::Flat, tml::Accidental::Natural, tml::Accidental::Sharp};

   // Major keys
   for (auto it_letter : pitch_letters) {
      root.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         root.SetAccidental(it_accidental);
         test_key.Set(root);
         std::cout << "    KeySignature: " << test_key << std::endl;
      }
   }
   std::cout << std::endl;

   // Minor keys
   for (auto it_letter : pitch_letters) {
      root.SetPitchLetter(it_letter);
      for (auto it_accidental : accidentals) {
         root.SetAccidental(it_accidental);
         test_key.Set(root, false);
         std::cout << "    KeySignature: " << test_key << std::endl;
      }
   }
   std::cout << std::endl;

   // Major
   for (int i = -10; i <= 10; i++) {
      test_key.Set(i);
      std::cout << "    KeySignature: " << test_key << std::endl;
   }
   std::cout << std::endl;

   // Minor
   for (int i = -10; i <= 10; i++) {
      test_key.Set(i, false);
      std::cout << "    KeySignature: " << test_key << std::endl;
   }
   std::cout << std::endl;

   EXPECT_TRUE(false);
}
