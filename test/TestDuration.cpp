#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestDuration, TestDurationTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Duration test_duration;

   std::cout << "    Duration: " << test_duration << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::NoteValue note_value = tml::NoteValue::Quarter;
   tml::Dotted dotted = tml::Dotted::Single;
   tml::NamedTuplet tuplet = tml::NamedTuplet::Triplet;

   test_duration.SetNoteValue(note_value);
   test_duration.SetDotModifier(dotted);
   test_duration.SetTuplet(tuplet);

   std::cout << "    Duration: " << test_duration << std::endl;

   // Test all combinations
   std::cout << "Testing all combinations: " << std::endl;

   auto note_values = {tml::NoteValue::Whole,
                       tml::NoteValue::Half,
                       tml::NoteValue::Quarter,
                       tml::NoteValue::Eighth,
                       tml::NoteValue::Sixteenth,
                       tml::NoteValue::ThirtySecond,
                       tml::NoteValue::SixtyFourth,
                       tml::NoteValue::HundredTwentyEighth,
                       tml::NoteValue::TwoHundredFiftySixth,
                       tml::NoteValue::DoubleWhole,
                       tml::NoteValue::Longa,
                       tml::NoteValue::Maxima};

   for (auto note_value_it : note_values) {
      for (unsigned int dot_it = 0; dot_it <= 5; dot_it++) {
         for (unsigned int tuplet_it = 0; tuplet_it <= 9; tuplet_it++) {
            test_duration.SetNoteValue(note_value_it);
            test_duration.SetDotModifier(dot_it);
            test_duration.SetTuplet(tuplet_it);
            std::cout << "    Duration: " << test_duration << std::endl;
         }
      }
   }

   EXPECT_TRUE(false);
}
