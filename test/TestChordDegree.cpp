#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestChordDegree, TestChordDegreeTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::ChordDegree test_chord_degree;

   std::cout << "    ChordDegree: " << test_chord_degree << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   test_chord_degree.SetDegree(9);
   test_chord_degree.SetAccidental(tml::Accidental::Sharp);

   std::cout << "    ChordDegree: " << test_chord_degree << std::endl;

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   tml::Chord test_chord;

   auto pitch_letters_C_major = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                                 tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                                 tml::PitchLetter::B};

   auto pitches_Bb_major = {tml::Pitch(tml::PitchLetter::B, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::C, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::E, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::F, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::A, tml::Accidental::Natural, 5)};

   for (auto it : pitch_letters_C_major) {
      test_chord.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
   }
   test_chord.SetName("Major");

   test_chord_degree.SetAccidental(tml::Accidental::Natural);
   for (int i = 1; i <= 7; i++) {
      test_chord_degree.SetDegree(i);
      tml::Pitch test_pitch = test_chord_degree.GetPitchFromChord(test_chord);
      std::cout << "    ChordDegree: " << test_chord_degree << " -> " << test_pitch << std::endl;
   }

   test_chord.Clear();
   for (auto it : pitches_Bb_major) {
      test_chord.AddPitch(it);
   }
   test_chord.SetName("Major");

   for (int i = 1; i <= 7; i++) {
      test_chord_degree.SetDegree(i);
      std::cout << "    ChordDegree: " << test_chord_degree << " -> "
                << test_chord_degree.GetPitchNameFromChord(test_chord) << std::endl;
   }

   test_chord_degree.SetAccidental(tml::Accidental::Flat);
   for (int i = 1; i <= 7; i++) {
      test_chord_degree.SetDegree(i);
      std::cout << "    ChordDegree: " << test_chord_degree << " -> "
                << test_chord_degree.GetPitchFromChord(test_chord) << std::endl;
   }

   EXPECT_TRUE(false);
}
