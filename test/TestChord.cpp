#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestChord, TestChordTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Chord test_chord;

   std::cout << "    Chord: " << test_chord << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   auto pitch_letters_C_major = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                                 tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                                 tml::PitchLetter::B};

   int i = 1;
   for (auto it : pitch_letters_C_major) {
      if (i % 2) {
         test_chord.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
      }
      i++;
   }
   test_chord.SetRoot(tml::PitchName(tml::PitchLetter::C, tml::Accidental::Natural));
   test_chord.SetName("Major 7");

   std::cout << "    Chord: " << test_chord << std::endl;

   test_chord.RemovePitch(3);
   test_chord.SetName("Major");
   std::cout << "    Remove: " << test_chord << std::endl;

   test_chord.Clear();
   std::cout << "    Clear: " << test_chord << std::endl;

   // Bounds Checking (errors are good)
   std::cout << "Bounds checking (errors are good): " << test_chord << std::endl;

   i = 1;
   for (auto it : pitch_letters_C_major) {
      if (i % 2) {
         test_chord.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
      }
      i++;
   }

   std::cout << "    Remove (min-1): " << std::endl;
   test_chord.RemovePitch(-1);

   std::cout << "    Remove (max+1): " << std::endl;
   test_chord.RemovePitch(4);

   test_chord.Clear();

   EXPECT_TRUE(false);
}
