#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestScale, TestScaleTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Scale test_scale;

   std::cout << "    Scale: " << test_scale << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   auto pitch_letters_C_major = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                                 tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                                 tml::PitchLetter::B};

   auto pitches_Bb_major = {tml::Pitch(tml::PitchLetter::B, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::C, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::E, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::F, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::A, tml::Accidental::Natural, 5)};

   auto pitches_non_diatonic = {tml::Pitch(tml::PitchLetter::B, tml::Accidental::Flat, 4),
                                tml::Pitch(tml::PitchLetter::C, tml::Accidental::Natural, 4),
                                tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 4),
                                tml::Pitch(tml::PitchLetter::E, tml::Accidental::Flat, 4),
                                tml::Pitch(tml::PitchLetter::F, tml::Accidental::Natural, 4),
                                tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 4),
                                tml::Pitch(tml::PitchLetter::A, tml::Accidental::Flat, 5)};

   for (auto it : pitch_letters_C_major) {
      test_scale.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
   }
   test_scale.SetName("Major");

   std::cout << "    Scale: " << test_scale << std::endl;

   test_scale.RemovePitch(6);
   test_scale.RemovePitch(0);
   std::cout << "    Remove: " << test_scale << std::endl;

   test_scale.Clear();
   std::cout << "    Clear: " << test_scale << std::endl;

   // Bounds Checking (errors are good)
   std::cout << "Bounds checking (errors are good): " << test_scale << std::endl;

   for (auto it : pitch_letters_C_major) {
      test_scale.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
   }

   std::cout << "    Remove (min-1): " << std::endl;
   test_scale.RemovePitch(-1);

   std::cout << "    Remove (max+1): " << std::endl;
   test_scale.RemovePitch(7);

   test_scale.Clear();

   // Testing functionality
   std::cout << "Testing functionality: " << std::endl;

   for (auto it : pitch_letters_C_major) {
      test_scale.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
   }

   std::cout << "    IsDiatonic: " << ((test_scale.IsDiatonic()) ? "true" : "false") << std::endl;

   test_scale.Clear();
   for (auto it : pitches_Bb_major) {
      test_scale.AddPitch(it);
   }
   test_scale.SetName("Major");

   std::cout << "    Scale: " << test_scale << std::endl;
   std::cout << "    IsDiatonic: " << ((test_scale.IsDiatonic()) ? "true" : "false") << std::endl;

   test_scale.Clear();
   for (auto it : pitches_non_diatonic) {
      test_scale.AddPitch(it);
   }

   std::cout << "    Scale: " << test_scale << std::endl;

   std::cout << "    IsDiatonic: " << ((test_scale.IsDiatonic()) ? "true" : "false") << std::endl;

   test_scale.RemovePitch(0);
   std::cout << "    RemovePitch: " << test_scale << std::endl;

   std::cout << "    GetRoot: " << test_scale.GetRoot().GetPitchName() << std::endl;

   EXPECT_TRUE(false);
}
