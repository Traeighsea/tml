#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestNote, TestNoteTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Note test_note;

   std::cout << "    Note: " << test_note << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::PitchLetter letter = tml::PitchLetter::D;
   tml::Accidental accidental = tml::Accidental::Flat;
   tml::Octave o = 6;
   tml::NoteValue note_value = tml::NoteValue::Quarter;
   tml::Dotted dotted = tml::Dotted::Single;
   tml::NamedTuplet tuplet = tml::NamedTuplet::Triplet;
   tml::DynamicMarking dynamic_marking = tml::DynamicMarking::Fortissimo;

   test_note.SetPitchLetter(letter);
   test_note.SetAccidental(accidental);
   test_note.SetOctave(o);
   test_note.SetNoteValue(note_value);
   test_note.SetDotModifier(dotted);
   test_note.SetTuplet(tuplet);
   test_note.SetDynamicMarking(dynamic_marking);

   std::cout << "    Note: " << test_note << std::endl;

   // TODO: I think I want to overload all these = and == operators
   /*
   // Test conversions
   std::cout << "Testing conversions: " << std::endl;

   letter = tml::PitchLetter::G;
   accidental = tml::Accidental::Sharp;
   o = 3;
   note_value = tml::NoteValue::Half;
   dotted = tml::Dotted::None
   tuplet = tml::NamedTuplet::Duplet;
   dynamic_marking = tml::DynamicMarking::Pianissimo;

   test_note = letter;
   test_note = accidental;
   test_note = o;
   test_note = note_value;
   test_note = dotted;
   test_note = tuplet;
   test_note = dynamic_marking;

   std::cout << "    Note: " << test_note << std::endl;
   */

   EXPECT_TRUE(false);
}
