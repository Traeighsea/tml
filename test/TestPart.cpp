#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestPart, TestPartTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Part test_part;
   std::cout << "    Part: " << test_part << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   // Adding the lick in D minor
   tml::Measure measure_one{{{tml::PitchLetter::D, tml::Accidental::Natural}, false}, {4, 4}};
   measure_one.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_one.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   // Adding the lick in A minor
   tml::Measure measure_two{{{tml::PitchLetter::A, tml::Accidental::Natural}, false}, {4, 4}};
   measure_two.Add({1}, {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({2}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({3}, {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_two.Add({4}, {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   test_part.Append(measure_one);
   test_part.Append(measure_two);

   test_part.SetVoice({"Cello", tml::Instrument::Cello});

   std::cout << "    Part: " << test_part << std::endl;

   test_part.Remove(1);
   test_part.Insert(1, measure_one);

   std::cout << "    Part: " << test_part << std::endl;

   // Test bounds
   std::cout << "Testing limit checking: " << std::endl;

   auto measure = test_part.GetMeasure(8);

   std::cout << "    Measure: " << measure << std::endl;

   measure = test_part.GetMeasure(-8);

   std::cout << "    Measure: " << measure << std::endl;

   test_part.Clear();
   test_part.Remove(2);

   auto measure_error = test_part.GetMeasure(12, false);

   std::cout << "    Measure: " << measure_error << std::endl;

   std::cout << "    Part: " << test_part << std::endl;

   EXPECT_TRUE(false);
}
