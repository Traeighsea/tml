#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

// Enable this to generate a list of all possible scales, otherwise only root C are generated
const bool GENERATE_ALL = false;

TEST(TestGuitarFretboard, TestGuitarFretboardTodo) {
   tml::PitchName root; // Defaults to C
   tml::Scale scale;
   tml::ScaleFormula scale_formula;

   tml::ScaleDictionary test_scale_dictionary;
   std::vector<std::string> scale_list = test_scale_dictionary.GetRegisteredNames();
   std::cout << "    ScaleDictionary: " << test_scale_dictionary << std::endl;

   tml::GuitarFretboard guitar_fretboard;

   // Output fretboards for each registered scale in the dictionary using scale degrees
   guitar_fretboard.EnableOutputScaleDegrees();
   for (auto it : scale_list) {
      scale = test_scale_dictionary.GetScale(root, it);
      scale_formula = test_scale_dictionary.GetScaleFormula(it);
      std::cout << "    Scale: " << scale_formula << std::endl;

      guitar_fretboard.SetFromScaleAndScaleFormula(scale, scale_formula);
      std::cout << guitar_fretboard << std::endl;
   }
   guitar_fretboard.DisableOutputScaleDegrees();

   // If enabled we will generate fretboards for all 36 potential note names and all registered
   // scales in the dictionary
   if (GENERATE_ALL) {
      auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                            tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                            tml::PitchLetter::B};

      root.SetAccidental(tml::Accidental::Natural);
      for (auto it_letter : pitch_letters) {
         root.SetPitchLetter(it_letter);
         for (auto it : scale_list) {
            scale = test_scale_dictionary.GetScale(root, it);
            std::cout << "    Scale: " << scale << std::endl;

            guitar_fretboard.SetFromScale(scale);
            std::cout << guitar_fretboard << std::endl;
         }
      }

      root.SetAccidental(tml::Accidental::Flat);
      for (auto it_letter : pitch_letters) {
         root.SetPitchLetter(it_letter);
         for (auto it : scale_list) {
            scale = test_scale_dictionary.GetScale(root, it);
            std::cout << "    Scale: " << scale << std::endl;

            guitar_fretboard.SetFromScale(scale);
            std::cout << guitar_fretboard << std::endl;
         }
      }

      root.SetAccidental(tml::Accidental::Sharp);
      for (auto it_letter : pitch_letters) {
         root.SetPitchLetter(it_letter);
         for (auto it : scale_list) {
            scale = test_scale_dictionary.GetScale(root, it);
            std::cout << "    Scale: " << scale << std::endl;

            guitar_fretboard.SetFromScale(scale);
            std::cout << guitar_fretboard << std::endl;
         }
      }
   } else {
      // All scales for root C
      for (auto it : scale_list) {
         scale = test_scale_dictionary.GetScale(root, it);
         std::cout << "    Scale: " << scale << std::endl;

         guitar_fretboard.SetFromScale(scale);
         std::cout << guitar_fretboard << std::endl;
      }
   }

   EXPECT_TRUE(false);
};
