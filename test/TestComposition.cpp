#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestComposition, TestCompositionTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Composition test_composition;
   std::cout << "    Composition: " << test_composition << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   // Adding the lick in D minor
   tml::Measure measure_one{{{tml::PitchLetter::D, tml::Accidental::Natural}, false}, {4, 4}};
   measure_one.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_one.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   // Adding the lick in A minor
   tml::Measure measure_two{{{tml::PitchLetter::A, tml::Accidental::Natural}, false}, {4, 4}};
   measure_two.Add({1}, {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({2}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({3}, {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_two.Add({4}, {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});
   unsigned part_voice = test_composition.AddPart("Voice", tml::Instrument::Violin, 0);
   unsigned part_piano = test_composition.AddPart("Piano", tml::Instrument::AcousticGrandPiano, 0);
   unsigned part_bass = test_composition.AddPart("Bass", tml::Instrument::AcousticBass, 4);

   test_composition[part_voice].Append(measure_one);
   test_composition[part_voice].Append(measure_two);
   test_composition[part_voice].Append(measure_one);
   test_composition[part_voice].Append(measure_two);

   test_composition[part_piano].Append(measure_one);
   test_composition[part_piano].Append(measure_two);
   test_composition[part_piano].Append(measure_one);
   test_composition[part_piano].Append(measure_two);

   test_composition[part_bass][0] = measure_one;
   test_composition[part_bass][1] = measure_two;
   test_composition[part_bass][2] = measure_one;
   test_composition[part_bass][3] = measure_two;

   tml::Part temp = test_composition[part_piano];
   test_composition.RemovePart(part_piano);
   test_composition.InsertPart(1, "", tml::Instrument::None, 0);

   std::cout << "    Composition: " << test_composition << std::endl;

   // Test bounds
   std::cout << "Testing limit checking: " << std::endl;

   test_composition.RemovePart(2);
   test_composition.RemovePart(1);
   test_composition.RemovePart(0);

   test_composition.RemovePart(2);

   auto part = test_composition.GetPart(8);

   std::cout << "    Part: " << part << std::endl;

   EXPECT_TRUE(false);
}
