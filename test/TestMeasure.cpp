#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestMeasure, TestMeasureTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Measure test_measure;
   std::cout << "    Measure: " << test_measure << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   test_measure.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                    {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                     {tml::NoteValue::Eighth, 0, 0},
                     {tml::DynamicMarking::m}});

   test_measure.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                          {tml::NoteValue::Eighth, 0, 0},
                          {tml::DynamicMarking::m}});

   test_measure.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                    {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                     {tml::NoteValue::Eighth, 0, 0},
                     {tml::DynamicMarking::m}});

   test_measure.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                          {tml::NoteValue::Quarter, 0, 0},
                          {tml::DynamicMarking::p}});

   test_measure.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                          {tml::NoteValue::Eighth, 0, 0},
                          {tml::DynamicMarking::m}});

   test_measure.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                    {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                     {tml::NoteValue::Quarter, 1, 0},
                     {tml::DynamicMarking::m}});

   test_measure.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                          {tml::NoteValue::Eighth, 0, 0},
                          {tml::DynamicMarking::m}});

   std::cout << "    Measure: " << test_measure << std::endl;

   test_measure.Remove(4);
   test_measure.Add({4}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                          {tml::NoteValue::Quarter, 0, 0},
                          {tml::DynamicMarking::f}});

   std::cout << "    Measure: " << test_measure << std::endl;

   // Test bounds
   std::cout << "Testing limit checking: " << std::endl;

   auto note = test_measure.GetNote(8);

   std::cout << "    Note: " << note.first << ": " << note.second << std::endl;

   note = test_measure.GetNote(-8);

   std::cout << "    Note: " << note.first << ": " << note.second << std::endl;

   test_measure.Clear();
   test_measure.Remove(2);

   auto note_error = test_measure.GetNote(12, false);

   std::cout << "    Note: " << note_error.first << ": " << note_error.second << std::endl;

   std::cout << "    Measure: " << test_measure << std::endl;

   EXPECT_TRUE(false);
}
