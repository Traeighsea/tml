#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestMotif, TestMotifTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Motif test_motif;
   std::cout << "    Motif: " << test_motif << std::endl;

   // Test container functionality
   std::cout << "Testing setters: " << std::endl;

   test_motif.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                  {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                   {tml::NoteValue::Eighth, 0, 0},
                   {tml::DynamicMarking::m}});

   test_motif.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_motif.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                  {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                   {tml::NoteValue::Eighth, 0, 0},
                   {tml::DynamicMarking::m}});

   test_motif.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Quarter, 0, 0},
                        {tml::DynamicMarking::p}});

   test_motif.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_motif.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                  {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                   {tml::NoteValue::Quarter, 1, 0},
                   {tml::DynamicMarking::m}});

   test_motif.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   std::cout << "    Sequence: " << test_motif << std::endl;

   test_motif.Remove(4);
   test_motif.Add({4}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Quarter, 0, 0},
                        {tml::DynamicMarking::f}});

   std::cout << "    Sequence: " << test_motif << std::endl;

   // Test bounds
   std::cout << "Testing limit checking: " << std::endl;

   auto note = test_motif.GetNote(8);

   std::cout << "    Note: " << note.first << ": " << note.second << std::endl;

   note = test_motif.GetNote(-8);

   std::cout << "    Note: " << note.first << ": " << note.second << std::endl;

   test_motif.Clear();
   test_motif.Remove(2);

   auto note_error = test_motif.GetNote(12, false);

   std::cout << "    Note: " << note_error.first << ": " << note_error.second << std::endl;

   std::cout << "    Motif: " << test_motif << std::endl;

   EXPECT_TRUE(false);
}
