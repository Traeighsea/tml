#include "tml.hpp"
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <nlohmann/json.hpp>

/// Enable to save the data used in the test
const bool SaveJsonSamplesToFile = true;

TEST(TestJson, ChordSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonChord.json";
   tml::Chord test_obj;

   auto pitch_letters_C_major = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                                 tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                                 tml::PitchLetter::B};

   int i = 1;
   for (auto it : pitch_letters_C_major) {
      if (i % 2) {
         test_obj.AddPitch(tml::Pitch(it, tml::Accidental::Natural, 4));
      }
      i++;
   }
   test_obj.SetRoot(tml::PitchName(tml::PitchLetter::C, tml::Accidental::Natural));
   test_obj.SetName("Major 7");

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Chord test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ChordDegreeSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonChordDegree.json";
   tml::ChordDegree test_obj;
   test_obj.SetDegree(9);
   test_obj.SetAccidental(tml::Accidental::Sharp);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ChordDegree test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ChordDictionarySerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonChordDictionary.json";
   tml::ChordDictionary test_obj;

   tml::ChordDegree chord_degree;
   tml::ChordFormula minor;

   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   minor.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   minor.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   minor.AddChordDegree(chord_degree);

   minor.SetName("Test Minor");
   test_obj.Register(minor);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ChordDictionary test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ChordFormulaSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonChordFormula.json";
   tml::ChordFormula test_obj;

   tml::ChordDegree chord_degree;

   chord_degree.SetDegree(1);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_obj.AddChordDegree(chord_degree);

   chord_degree.SetDegree(3);
   chord_degree.SetAccidental(tml::Accidental::Flat);
   test_obj.AddChordDegree(chord_degree);

   chord_degree.SetDegree(5);
   chord_degree.SetAccidental(tml::Accidental::Natural);
   test_obj.AddChordDegree(chord_degree);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ChordFormula test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, CircleOfFifthsSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonCircleOfFifths.json";
   tml::CircleOfFifths test_obj;

   test_obj.CycleUp();

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::CircleOfFifths test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, CompositionSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonComposition.json";
   tml::Composition test_obj;

   // Adding the lick in D minor
   tml::Measure measure_one{{{tml::PitchLetter::D, tml::Accidental::Natural}, false}, {4, 4}};
   measure_one.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_one.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   // Adding the lick in A minor
   tml::Measure measure_two{{{tml::PitchLetter::A, tml::Accidental::Natural}, false}, {4, 4}};
   measure_two.Add({1}, {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({2}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({3}, {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_two.Add({4}, {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});
   unsigned part_voice = test_obj.AddPart("Voice", tml::Instrument::Violin, 0);
   unsigned part_piano = test_obj.AddPart("Piano", tml::Instrument::AcousticGrandPiano, 0);
   unsigned part_bass = test_obj.AddPart("Bass", tml::Instrument::AcousticBass, 4);

   test_obj[part_voice].Append(measure_one);
   test_obj[part_voice].Append(measure_two);
   test_obj[part_voice].Append(measure_one);
   test_obj[part_voice].Append(measure_two);

   test_obj[part_piano].Append(measure_one);
   test_obj[part_piano].Append(measure_two);
   test_obj[part_piano].Append(measure_one);
   test_obj[part_piano].Append(measure_two);

   test_obj[part_bass][0] = measure_one;
   test_obj[part_bass][1] = measure_two;
   test_obj[part_bass][2] = measure_one;
   test_obj[part_bass][3] = measure_two;

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Composition test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, DurationSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonDuration.json";
   tml::Duration test_obj;

   test_obj.SetNoteValue(tml::NoteValue::Quarter);
   test_obj.SetDotModifier(tml::Dotted::Single);
   test_obj.SetTuplet(tml::NamedTuplet::Triplet);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Duration test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, DynamicSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonDynamic.json";
   tml::Dynamic test_obj;

   test_obj.SetDynamicMarking(tml::DynamicMarking::Fortissimo);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Dynamic test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, GuitarFretboardSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonGuitarFretboard.json";
   tml::GuitarFretboard test_obj;

   tml::Scale scale;
   auto pitches_Bb_major = {tml::Pitch{tml::PitchLetter::B, tml::Accidental::Flat, 4},
                            tml::Pitch{tml::PitchLetter::C, tml::Accidental::Natural, 4},
                            tml::Pitch{tml::PitchLetter::D, tml::Accidental::Natural, 4},
                            tml::Pitch{tml::PitchLetter::E, tml::Accidental::Flat, 4},
                            tml::Pitch{tml::PitchLetter::F, tml::Accidental::Natural, 4},
                            tml::Pitch{tml::PitchLetter::G, tml::Accidental::Natural, 4},
                            tml::Pitch{tml::PitchLetter::A, tml::Accidental::Natural, 5}};
   for (auto it : pitches_Bb_major) {
      scale.AddPitch(it);
   }
   scale.SetName("Major");

   tml::ScaleFormula scale_formula;
   for (int i = 0; i < 7; i++) {
      scale_formula.AddScaleDegree({i + 1, tml::Accidental::Natural});
   }
   scale_formula.SetName("Major");

   test_obj.SetFromScaleAndScaleFormula(scale, scale_formula);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::GuitarFretboard test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, IntervalSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonInterval.json";
   tml::Interval test_obj;
   test_obj.Set(tml::NamedInterval::MinorThird);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Interval test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, IntervalClassSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonIntervalClass.json";
   tml::IntervalClass test_obj{2};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::IntervalClass test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, KeySignatureSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonKeySignature.json";
   tml::KeySignature test_obj{2, false};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::KeySignature test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, MeasureSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonMeasure.json";
   tml::Measure test_obj;

   test_obj.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                 {tml::NoteValue::Eighth, 0, 0},
                 {tml::DynamicMarking::m}});

   test_obj.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                      {tml::NoteValue::Eighth, 0, 0},
                      {tml::DynamicMarking::m}});

   test_obj.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                 {tml::NoteValue::Eighth, 0, 0},
                 {tml::DynamicMarking::m}});

   test_obj.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                      {tml::NoteValue::Quarter, 0, 0},
                      {tml::DynamicMarking::p}});

   test_obj.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                      {tml::NoteValue::Eighth, 0, 0},
                      {tml::DynamicMarking::m}});

   test_obj.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                 {tml::NoteValue::Quarter, 1, 0},
                 {tml::DynamicMarking::m}});

   test_obj.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                      {tml::NoteValue::Eighth, 0, 0},
                      {tml::DynamicMarking::m}});

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Measure test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, MotifSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonMotif.json";
   tml::Motif test_obj;
   test_obj.SetName("Trumpet Fanfare");

   test_obj.Add(tml::NoteOnset{1},
                tml::Note{tml::Pitch{tml::PitchLetter::A, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Quarter},
                          tml::Dynamic{tml::DynamicMarking::m}});
   test_obj.Add(tml::NoteOnset{2},
                tml::Note{tml::Pitch{tml::PitchLetter::B, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Eighth},
                          tml::Dynamic{tml::DynamicMarking::m}});
   test_obj.Add(tml::NoteOnset{2, tml::Duration{tml::NoteValue::Eighth}},
                tml::Note{tml::Pitch{tml::PitchLetter::C, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Eighth},
                          tml::Dynamic{tml::DynamicMarking::m}});
   test_obj.Add(tml::NoteOnset{3},
                tml::Note{tml::Pitch{tml::PitchLetter::D, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Quarter, 1},
                          tml::Dynamic{tml::DynamicMarking::m}});
   test_obj.Add(tml::NoteOnset{4, tml::Duration{tml::NoteValue::Eighth}},
                tml::Note{tml::Pitch{tml::PitchLetter::E, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Eighth},
                          tml::Dynamic{tml::DynamicMarking::f}});
   test_obj.Add(tml::NoteOnset{4, tml::Duration{tml::NoteValue::Eighth}},
                tml::Note{tml::Pitch{tml::PitchLetter::C, tml::Accidental::Natural, 4},
                          tml::Duration{tml::NoteValue::Eighth},
                          tml::Dynamic{tml::DynamicMarking::f}});
   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Motif test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, NoteSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonNote.json";
   tml::Note test_obj{tml::Pitch{tml::PitchLetter::D, tml::Accidental::Flat, 4},
                      tml::Duration{tml::NoteValue::Quarter, 1},
                      tml::Dynamic{tml::DynamicMarking::f}};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Note test_obj_serialized;
   test_obj_serialized = (tml::Note)json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, NoteOnsetSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonNoteOnset.json";
   tml::NoteOnset test_obj{2, tml::Duration{tml::NoteValue::Whole, 1, 3}};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::NoteOnset test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, PartSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonPart.json";
   tml::Part test_obj;

   // Adding the lick in D minor
   tml::Measure measure_one{{{tml::PitchLetter::D, tml::Accidental::Natural}, false}, {4, 4}};
   measure_one.Add({1}, {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({2}, {{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_one.Add({3}, {{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_one.Add({4}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_one.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   // Adding the lick in A minor
   tml::Measure measure_two{{{tml::PitchLetter::A, tml::Accidental::Natural}, false}, {4, 4}};
   measure_two.Add({1}, {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({1, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({2}, {{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({2, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Eighth, 0, 0},
                    {tml::DynamicMarking::m}});
   measure_two.Add({3}, {{tml::PitchLetter::B, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Quarter, 0, 0},
                         {tml::DynamicMarking::p}});
   measure_two.Add({4}, {{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});
   measure_two.Add({4, {tml::NoteValue::Eighth, 0, 0}},
                   {{tml::PitchLetter::A, tml::Accidental::Natural, 3},
                    {tml::NoteValue::Quarter, 1, 0},
                    {tml::DynamicMarking::m}});

   test_obj.Append(measure_one);
   test_obj.Append(measure_two);

   test_obj.SetVoice({"Cello", tml::Instrument::Cello});

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Part test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, PitchSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonPitch.json";
   tml::Pitch test_obj{tml::PitchName{tml::PitchLetter::F, tml::Accidental::Sharp}, 5};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Pitch test_obj_serialized;
   test_obj_serialized = (tml::Pitch)json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, PitchClassSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonPitchClass.json";
   tml::PitchClass test_obj{4};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::PitchClass test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, PitchClassSetSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonPitchClassSet.json";
   tml::PitchClassSet test_obj;
   test_obj.Enable(3);
   test_obj.Enable(6);
   test_obj.Enable(9);
   test_obj.Enable(11);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::PitchClassSet test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, PitchNameSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonPitchName.json";
   tml::PitchName test_obj{tml::PitchLetter::G, tml::Accidental::DoubleSharp};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::PitchName test_obj_serialized;
   test_obj_serialized = (tml::PitchName)json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, RhythmSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonRhythm.json";
   tml::Rhythm test_obj;
   test_obj.SetName("AAAAAA");
   test_obj.Append(tml::Duration{tml::NoteValue::TwoHundredFiftySixth, 0, 3});
   test_obj.Append(tml::Duration{tml::NoteValue::TwoHundredFiftySixth, 0, 3});
   test_obj.Append(tml::Duration{tml::NoteValue::TwoHundredFiftySixth, 0, 3});

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Rhythm test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ScaleSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonScale.json";
   tml::Scale test_obj;

   auto pitches_Bb_major = {tml::Pitch(tml::PitchLetter::B, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::C, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::E, tml::Accidental::Flat, 4),
                            tml::Pitch(tml::PitchLetter::F, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 4),
                            tml::Pitch(tml::PitchLetter::A, tml::Accidental::Natural, 5)};

   for (auto it : pitches_Bb_major) {
      test_obj.AddPitch(it);
   }
   test_obj.SetName("Major");

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Scale test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ScaleDegreeSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonScaleDegree.json";
   tml::ScaleDegree test_obj{3, tml::Accidental::Flat};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ScaleDegree test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ScaleDictionarySerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonScaleDictionary.json";
   tml::ScaleDictionary test_obj;

   test_obj.Register(tml::ScaleFormula("AAAAAA Major", {{1}, {2}, {3}, {4}, {5}, {6}, {7}}));

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ScaleDictionary test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ScaleFormulaSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonScaleFormula.json";
   tml::ScaleFormula test_obj{"Locrian",
                              {{1},
                               {2, tml::Accidental::Flat},
                               {3, tml::Accidental::Flat},
                               {4},
                               {5, tml::Accidental::Flat},
                               {6, tml::Accidental::Flat},
                               {7, tml::Accidental::Flat}}};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ScaleFormula test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, SequenceSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonSequence.json";
   tml::Sequence test_obj;

   test_obj.AppendNote({{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_obj.AppendNote({{tml::PitchLetter::F, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_obj.AppendNote({{tml::PitchLetter::G, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_obj.AppendNote({{tml::PitchLetter::E, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Quarter, 0, 0},
                        {tml::DynamicMarking::p}});

   test_obj.AppendNote({{tml::PitchLetter::C, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Eighth, 0, 0},
                        {tml::DynamicMarking::m}});

   test_obj.AppendNote({{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                        {tml::NoteValue::Quarter, 1, 0},
                        {tml::DynamicMarking::m}});

   test_obj.PrependNote({{tml::PitchLetter::D, tml::Accidental::Natural, 3},
                         {tml::NoteValue::Eighth, 0, 0},
                         {tml::DynamicMarking::m}});

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Sequence test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, TimeSignatureSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonTimeSignature.json";
   tml::TimeSignature test_obj{7, 8};

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::TimeSignature test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, ToneRowSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonToneRow.json";
   tml::ToneRow test_obj;

   test_obj.Append(3);
   test_obj.Append(7);
   test_obj.Append(2);
   test_obj.Append(8);

   test_obj.Append(11);
   test_obj.Append(1);
   test_obj.Append(4);
   test_obj.Append(5);

   test_obj.Append(6);
   test_obj.Append(10);
   test_obj.Append(0);
   test_obj.Append(9);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::ToneRow test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}

TEST(TestJson, VoiceSerialization) {
   // Setup
   constexpr auto OutputFile = "output/TestJsonVoice.json";
   tml::Voice test_obj("End of the World", tml::Instrument::Glockenspiel);

   // Serialize object
   nlohmann::json json_data = test_obj;

   // Read serialized object
   tml::Voice test_obj_serialized;
   test_obj_serialized = json_data;

   // Compare our objects to ensure they're equivalent
   EXPECT_TRUE(test_obj == test_obj_serialized);

   // If enabled we will _actually_ output the object as a json file
   if (SaveJsonSamplesToFile) {
      std::ofstream output_json_file(OutputFile);
      output_json_file << std::setw(3) << json_data << std::endl;
   }
}
