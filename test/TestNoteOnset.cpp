#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestNoteOnset, TestNoteOnsetTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::NoteOnset test_note_onset;

   std::cout << "    Note Onset: " << test_note_onset << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::Duration duration;

   tml::NoteValue note_value = tml::NoteValue::Eighth;
   tml::Dotted dotted = tml::Dotted::Single;

   duration.SetNoteValue(note_value);
   duration.SetDotModifier(dotted);

   test_note_onset.SetBeat(3);
   test_note_onset.SetDurationOffset(duration);

   std::cout << "    Note Onset: " << test_note_onset << std::endl;

   test_note_onset.SetHasDurationOffset(false);

   std::cout << "    Note Onset: " << test_note_onset << std::endl;

   // Test all combinations
   std::cout << "Testing all combinations: " << std::endl;

   test_note_onset.SetBeat(1);

   auto note_values = {tml::NoteValue::Whole,
                       tml::NoteValue::Half,
                       tml::NoteValue::Quarter,
                       tml::NoteValue::Eighth,
                       tml::NoteValue::Sixteenth,
                       tml::NoteValue::ThirtySecond,
                       tml::NoteValue::SixtyFourth,
                       tml::NoteValue::HundredTwentyEighth,
                       tml::NoteValue::TwoHundredFiftySixth,
                       tml::NoteValue::DoubleWhole,
                       tml::NoteValue::Longa,
                       tml::NoteValue::Maxima};

   for (auto note_value_it : note_values) {
      for (unsigned int dot_it = 0; dot_it <= 5; dot_it++) {
         for (unsigned int tuplet_it = 0; tuplet_it <= 9; tuplet_it++) {
            duration.SetNoteValue(note_value_it);
            duration.SetDotModifier(dot_it);
            duration.SetTuplet(tuplet_it);
            test_note_onset.SetDurationOffset(duration);
            std::cout << "    Note Onset: " << test_note_onset << std::endl;
         }
      }
   }

   EXPECT_TRUE(false);
}
