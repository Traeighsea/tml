#include "tml.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestInterval, TestIntervalTodo) {
   // Test ostream operator and default constructor
   std::cout << "Testing Default Constructor: " << std::endl;

   tml::Interval test_interval = tml::Interval();

   std::cout << "    Interval: " << test_interval << std::endl;

   // Test setters
   std::cout << "Testing setters: " << std::endl;

   tml::NamedInterval n = tml::NamedInterval::PerfectFourth;

   test_interval.Set(n);

   std::cout << "    Interval: " << test_interval << std::endl;
   std::cout << "    Interval (distance): " << test_interval.GetDistance() << std::endl;

   test_interval = tml::Interval(4);

   std::cout << "    Interval: " << test_interval << std::endl;
   std::cout << "    Interval (distance): " << test_interval.GetDistance() << std::endl;

   test_interval = tml::Interval(3, 8);

   std::cout << "    Interval: " << test_interval << std::endl;
   std::cout << "    Interval (distance): " << test_interval.GetDistance() << std::endl;

   // Test Functionality
   std::cout << "Testing Functionality: " << std::endl;

   test_interval.Set(tml::NamedInterval::MinorThird);
   std::cout << "    Interval: " << test_interval << std::endl;

   test_interval.Invert();
   std::cout << "    Interval (inverted): " << test_interval << std::endl;

   test_interval.Invert();
   std::cout << "    Interval (inverted-inverted): " << test_interval << std::endl;

   test_interval.Set(tml::NamedInterval::MajorSeventh);
   std::cout << "    Interval: " << test_interval << std::endl;

   test_interval.Invert();
   std::cout << "    Interval (inverted): " << test_interval << std::endl;

   EXPECT_TRUE(false);
}
