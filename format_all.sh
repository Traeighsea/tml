#!/bin/bash

# Cd into the location of the shell script
cd "$(dirname "$0")"

pwd

find . -regex './src/.*\.\(cpp\|hpp\|cc\|cxx\)' -exec clang-format -i {} \;

find . -regex './test/.*\.\(cpp\|hpp\|cc\|cxx\)' -exec clang-format -i {} \;

find . -regex './include/.*\.\(cpp\|hpp\|cc\|cxx\)' -exec clang-format -i {} \;