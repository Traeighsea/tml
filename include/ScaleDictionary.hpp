#pragma once

#include "ScaleFormula.hpp"
#include <iosfwd>
#include <map>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class Scale;
class PitchName;
class Pitch;
class Note;
}

namespace tml {
/// @brief Container for registering and retrieving known scales.
/// @details Data is stored as key value pairs of strings and ScaleFormulas.
class ScaleDictionary {
 public:
   ScaleDictionary(bool register_common = true);
   ~ScaleDictionary() = default;
   ScaleDictionary(const ScaleDictionary &other) = default;
   ScaleDictionary(ScaleDictionary &&other) = default;
   ScaleDictionary &operator=(const ScaleDictionary &rhs) = default;
   ScaleDictionary &operator=(ScaleDictionary &&rhs) = default;

   bool operator==(const ScaleDictionary &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ScaleDictionary &val);
   std::string ToString() const;

   bool Register(const ScaleFormula &formula);
   bool Unregister(const std::string &name);
   void Clear();
   /// Clears the dictionary and resets based on the SCALE_DICTIONARY_SCALES_TO_REGISTER global
   void Reset();

   ScaleFormula GetScaleFormula(const std::string &name) const;
   /// There may be multiple keys with the same value, so return all
   std::vector<std::string> GetName(const ScaleFormula &formula) const;
   Scale GetScale(const Pitch &root, const std::string &name);
   Scale GetScale(const PitchName &root, const std::string &name);
   Scale GetScale(const Note &root, const std::string &name);

   std::vector<std::string> GetRegisteredNames() const;
   const std::map<std::string, ScaleFormula> &GetFullDictionary() const;

 private:
   // @TODO: Implement a bi map, and potentially divide up the data by size
   //    for lookup speed and efficiency
   std::map<std::string, ScaleFormula> dictionary_;

   /// Registers a collection of commonly used scales
   void RegisterCommon();
   /// Registers a collection of scales that would be typical in jazz and more modern harmony
   void RegisterModern();
   /// Registers a collection of scales that would rarely be used
   void RegisterExtended();
   /// Non 12-edo
   /// @TODO: This is just an example of other sets of supported scales
   void RegisterNonTwelveEdo();

   friend void to_json(nlohmann::json &j, const ScaleDictionary &val);
   friend void from_json(const nlohmann::json &j, ScaleDictionary &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ScaleDictionary &val);
/// json deserializer
void from_json(const nlohmann::json &j, ScaleDictionary &val);

}