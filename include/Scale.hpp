#pragma once

#include "Pitch.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
class PitchName;
class ScaleDegree;
}

namespace tml {
/// @brief A scale is a collection of subsequent notes in a series with a predetermined start and
/// end in a specific direction
/// @details @TODO(traeighsea)
/// @see [Scale](https://en.wikipedia.org/wiki/Scale_(music))
class Scale {
 public:
   Scale() = default;
   ~Scale() = default;
   Scale(const Scale &other) = default;
   Scale(Scale &&other) = default;
   Scale &operator=(const Scale &rhs) = default;
   Scale &operator=(Scale &&rhs) = default;

   bool operator==(const Scale &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Scale &val);
   std::string ToString() const;

   // Scale(ScaleName scale_name);

   std::string GetName() const;
   std::vector<Pitch> GetPitches() const;

   void SetName(const std::string &name);

   /* May put this in a ScaleBuilder class
   void SetFromScaleName(const PitchName& root, const ScaleName& scale_name);
   void SetFromScaleName(const Pitch& root, const ScaleName& scale_name);
   void SetFromScaleName(const Note& root, const ScaleName& scale_name);
   */

   void AddPitch(const Pitch &pitch);
   void RemovePitch(int index);
   void Clear();

   bool IsDiatonic() const;
   Pitch GetRoot() const;

   /// @note: Index will be %'d by the size, so should not go out of bounds
   PitchName GetPitchName(int index) const;

   /// @note: Index will be %'d by the size, so should not go out of bounds
   Pitch GetPitch(int index) const;
   Pitch GetPitch(const ScaleDegree &scale_degree) const;

   int GetSize() const;

 private:
   /// If it's a named scale, use this name
   std::string name_ = "";

   /// Structure holding sequence of pitches
   std::vector<Pitch> pitch_sequence_;

   /*
   /// Information about how the note correlates to the pitch sequence through a scale degree
   /// @note: should always be  the same size as pitch_sequence_
   std::vector<ScaleDegree> scale_degrees_;
   */
   friend void to_json(nlohmann::json &j, const Scale &val);
   friend void from_json(const nlohmann::json &j, Scale &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Scale &val);
/// json deserializer
void from_json(const nlohmann::json &j, Scale &val);

}
