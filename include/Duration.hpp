#pragma once

#include <iosfwd>
#include <iostream>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
// @TODO: Redesign without the extra typedefs

/// @brief NoteValues are relative values or durations as you would use in typical music notation.
/// @details Since the value is relative to the tempo, and the note does not have knowledge of the
/// tempo at this level, the noteValue needs to be calculated with the tempo to get the duration in
/// ms/s.
/// @see [Note Values](https://en.wikipedia.org/wiki/Note_value)
enum class NoteValue : int {
   Whole = 1,
   Half = 2,
   Quarter = 4,
   Eighth = 8,
   Sixteenth = 16,
   ThirtySecond = 32,
   SixtyFourth = 64,
   HundredTwentyEighth = 128,  // whyyy bro?
   TwoHundredFiftySixth = 256, // BRO STAHP

   // Have to get creative with these guys
   DoubleWhole = -2,
   Longa = -4,
   Maxima = -8 // did you leave the note on bro?
};

std::ostream &operator<<(std::ostream &out, const NoteValue &val);
std::string ToString(const NoteValue &val);

typedef unsigned int Tuplet;

enum class NamedTuplet : Tuplet {
   Duplet = 2,
   Triplet = 3,
   Quadruplet = 4,
   Quintuplet = 5,
   Sextuplet = 6,
   Septuplet = 7,
   Octuplet = 8
};

std::ostream &operator<<(std::ostream &out, const NamedTuplet &val);
std::string ToString(const NamedTuplet &val);

typedef unsigned int Dot;

enum class Dotted : Dot {
   None = 0,
   Single = 1,
   Double = 2,
   Triple = 3,
   Quadruple = 4
};
std::ostream &operator<<(std::ostream &out, const Dotted &val);
std::string ToString(const Dotted &val);

/// TODO:
class Duration {
 public:
   Duration() = default;
   ~Duration() = default;
   Duration(const Duration &other) = default;
   Duration(Duration &&other) = default;
   Duration &operator=(const Duration &rhs) = default;
   Duration &operator=(Duration &&rhs) = default;

   Duration(const NoteValue &note_value);
   Duration(const NoteValue &note_value, const Dot &dot_modifier);
   Duration(const NoteValue &note_value, const Dot &dot_modifier, const Tuplet &tuplet);

   bool operator==(const Duration &rhs) const;
   bool operator>(const Duration &rhs) const;
   bool operator<(const Duration &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Duration &val);
   std::string ToString() const;

   NoteValue GetNoteValue() const;
   Dot GetDotModifier() const;
   Tuplet GetTuplet() const;

   void SetNoteValue(const NoteValue &note_value);
   void SetDotModifier(const Dotted &dot_modifier);
   void SetDotModifier(const Dot &dot_modifier);
   void SetTuplet(const NamedTuplet &tuplet);
   void SetTuplet(const Tuplet &tuplet);

 private:
   /// The noteValue is using relative values as you would
   NoteValue note_value_ = NoteValue::Whole;
   /// The modifer is the number of dotted values added to the note. Realistically shouldn't really
   /// go above 4, but go crazy fam.
   ///   The algorithm for dotted notes is ((2^n - 1) / 2^n)
   Dot dot_modifier_ = 0;

   /// If you want to work with tuples (such as a triplet) set the tuplet to something other than 0
   /// This tuplet will modify the final duration with the ratio tuplet:noteValue or
   /// tuplet/noteValue
   ///   [Tuplet](https://en.wikipedia.org/wiki/Tuplet)
   Tuplet tuplet_ = 0;

   /// Could potentially use inheritance for these values to overwrite the functions for when we
   /// obtain the duration
   /*
   /// The duration will use the note value, also known as the relative value, unless otherwise
   noted. If false, the timeValue will be used for duration. bool usingRelativeValue_ = true;
   /// The timeValue will only be used if the usingRelativeValue is set. This will allow for fine
   tuned control. double timeValue_ = 0.0;
   */
   friend void to_json(nlohmann::json &j, const Duration &val);
   friend void from_json(const nlohmann::json &j, Duration &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Duration &val);
/// json deserializer
void from_json(const nlohmann::json &j, Duration &val);

}
