#pragma once

/// This works as a single header inclusion

// Configs
#include "Configs.hpp"

// Utility Functions
#include "MusicTheoryCommon.hpp"

// Note information data classes
#include "Duration.hpp"
#include "Dynamic.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchClass.hpp"
#include "PitchName.hpp"

// Music information data classes
#include "Instrument.hpp"
#include "KeySignature.hpp"
#include "NoteOnset.hpp"
#include "Tempo.hpp"
#include "TimeSignature.hpp"
#include "Voice.hpp"

// Note relationships
#include "Chord.hpp"
#include "ChordDegree.hpp"
#include "ChordFormula.hpp"
#include "CircleOfFifths.hpp"
#include "Interval.hpp"
#include "IntervalClass.hpp"
#include "PitchClassSet.hpp"
#include "Rhythm.hpp"
#include "Scale.hpp"
#include "ScaleDegree.hpp"
#include "ScaleFormula.hpp"
#include "ToneRow.hpp"

// Informational Dictionarys
#include "ChordDictionary.hpp"
#include "ScaleDictionary.hpp"

// Containers
#include "Composition.hpp"
#include "Measure.hpp"
#include "Motif.hpp"
#include "Part.hpp"
#include "Sequence.hpp"

// Extra utility
#include "GuitarFretboard.hpp"