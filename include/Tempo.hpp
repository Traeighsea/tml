#pragma once

#include <iosfwd>
#include <string>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
enum class Tempo : unsigned int {
   Larghissimo = 24,
   Adagissimo = 32,
   Grave = 40,
   Largo = 48,
   Lento = 56,
   Larghetto = 64,
   Adagio = 72,
   Adagietto = 80,
   Andante = 88,
   Andantino = 96,
   MarciaModerato = 104,
   AndanteModerato = 112,
   Moderato = 120,
   Allegretto = 128,
   AllegroModerato = 136,
   Allegro = 144,
   MoltoAllegro = 152,
   Vivace = 160,
   Vivacissimo = 168,
   Allegrissimo = 176,
   Presto = 184,
   Prestissimo = 192,
   Prestississimo = 200,
   Cookin = 260,
   AAAAAAAA = 300
};

// Stream operator for Tempo
std::ostream &operator<<(std::ostream &out, const Tempo &val);
std::string ToString(const Tempo &val);

}