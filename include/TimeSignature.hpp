#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
enum class NoteValue : int;
}

namespace tml {

/// @TODO(traeighsea) Revisit this class implementation for wonky values in the note value

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class TimeSignature {
 public:
   TimeSignature() = default;
   ~TimeSignature() = default;
   TimeSignature(const TimeSignature &other) = default;
   TimeSignature(TimeSignature &&other) = default;
   TimeSignature &operator=(const TimeSignature &rhs) = default;
   TimeSignature &operator=(TimeSignature &&rhs) = default;

   TimeSignature(unsigned int beats, NoteValue note_value);
   TimeSignature(unsigned int beats, unsigned int note_value);

   bool operator==(const TimeSignature &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const TimeSignature &val);
   std::string ToString() const;

   unsigned int GetBeats() const;
   unsigned int GetNoteVale() const;

   void SetBeats(unsigned int beats);
   void SetNoteValue(NoteValue note_value);
   void SetNoteValue(unsigned int note_value);

 private:
   unsigned int beats_ = 4;
   unsigned int note_value_ = 4;

   friend void to_json(nlohmann::json &j, const TimeSignature &val);
   friend void from_json(const nlohmann::json &j, TimeSignature &val);
};

/// json serializer
void to_json(nlohmann::json &j, const TimeSignature &val);
/// json deserializer
void from_json(const nlohmann::json &j, TimeSignature &val);

}