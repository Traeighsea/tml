#pragma once

#include "Duration.hpp"
#include "Dynamic.hpp"
#include "Pitch.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
enum class PitchLetter : int;
enum class Accidental : int;
class PitchName;
class PitchClass;
}

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Note {
 public:
   Note() = default;
   ~Note() = default;

   Note(const Note &other) = default;
   Note(Note &&other) = default;
   Note &operator=(const Note &rhs) = default;
   Note &operator=(Note &&rhs) = default;

   Note(const PitchName &other);
   Note(PitchName &&other);
   Note &operator=(const PitchName &rhs);
   Note &operator=(PitchName &&rhs);

   Note(const Pitch &other);
   Note(Pitch &&other);
   Note &operator=(const Pitch &rhs);
   Note &operator=(Pitch &&rhs);

   Note(const Pitch &pitch, const Duration &duration, const Dynamic &dynamic);

   bool operator==(const Note &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Note &val);
   std::string ToString() const;

   PitchClass GetPitchClass() const;

   Pitch GetPitch() const;
   Duration GetDuration() const;
   Dynamic GetDynamic() const;

   void SetPitch(const Pitch &pitch);
   void SetDuration(const Duration &duration);
   void SetDynamic(const Dynamic &dynamic);

   // Pitch functions
   PitchName GetPitchName() const;
   Octave GetOctave() const;

   void SetPitchName(const PitchName &pitch_name);
   void SetOctave(const Octave &octave);

   // PitchName functions
   PitchLetter GetPitchLetter() const;
   Accidental GetAccidental() const;

   void SetPitchLetter(const PitchLetter &letter);
   void SetAccidental(const Accidental &accidental);
   void AddAccidental(const Accidental &accidental);
   void Flatten();
   void Sharpen();

   // Duration functions
   NoteValue GetNoteValue() const;
   Dot GetDotModifier() const;
   Tuplet GetTuplet() const;

   void SetNoteValue(const NoteValue &note_value);
   void SetDotModifier(const Dotted &dot_modifier);
   void SetDotModifier(const Dot &dot_modifier);
   void SetTuplet(const NamedTuplet &tuplet);
   void SetTuplet(const Tuplet &tuplet);

   // Dynamic functions
   void SetDynamicMarking(const DynamicMarking &dynamic_marking);

   DynamicMarking GetDynamicMarking() const;

 private:
   /// The pitch will contain all of the frequency information, as well as the note name / theory
   ///   TODO: Should there be an std::optional here? Could make it more difficult to non C++ users.
   ///   Alternatively could have a bool for rest.
   Pitch pitch_;
   /// The duration will contain all of the note values. The Note Values can be converted to ms/s if
   /// given the tempo to calculate.
   Duration duration_;
   /// Dynamic will contain an enumerated list of durations using
   Dynamic dynamic_;
   /// TODO: Add articulation. The idea here is articulation can be things such as staccato, accent,
   /// crecendo, vibrato, etc. to a specific note. There can ideally be multiple different
   /// articulation modifiers on a single note so this could be a linked list, or it could be an
   /// abstract base class with a collection of modifiers, but that could get tricky to modify after
   /// the fact. Need to be modifiable after the fact.
   // Articulation articulation_;
   /// TODO: Decide if this is relevant at the note level
   // Timbre timbre_;

   friend void to_json(nlohmann::json &j, const Note &val);
   friend void from_json(const nlohmann::json &j, Note &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Note &val);
/// json deserializer
void from_json(const nlohmann::json &j, Note &val);

}
