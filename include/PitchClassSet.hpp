#pragma once

#include <array>
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class PitchClass;
class PitchName;
class Pitch;
class Note;
class Chord;
class Scale;
}

namespace tml {
/// @brief Object for pitch (class) set operations such that pitch is agnostic to note name. Useful
/// in twelve-tone matrices
/// @see [Pitch (Class) Set](https://en.wikipedia.org/wiki/Set_(music))
class PitchClassSet {
 public:
   PitchClassSet() = default;
   ~PitchClassSet() = default;
   PitchClassSet(const PitchClassSet &other) = default;
   PitchClassSet(PitchClassSet &&other) = default;
   PitchClassSet &operator=(const PitchClassSet &rhs) = default;
   PitchClassSet &operator=(PitchClassSet &&rhs) = default;

   bool operator==(const PitchClassSet &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const PitchClassSet &val);
   std::string ToString() const;

   bool &operator[](int pc);
   bool &operator[](unsigned pc);
   bool &operator[](const PitchClass &pitch_class);
   bool &operator[](const PitchName &pitch_name);
   bool &operator[](const Pitch &pitch);
   bool &operator[](const Note &note);

   const std::array<bool, 12> &GetFullPitchClassEnables() const;
   bool Get(int pc) const;

   void Enable(int pc);
   void Disable(int pc);
   void Toggle(int pc);

   void Set(unsigned int pc, bool val);
   void Set(int pc, bool val);
   void Set(const PitchClass &pitch_class, bool val);
   void Set(const PitchName &pitch_name, bool val);
   void Set(const Pitch &pitch, bool val);
   void Set(const Note &note, bool val);
   /// When setting from a container, pitch classes will first be reset
   void Set(const Chord &chord, bool val);
   /// When setting from a container, pitch classes will first be reset
   void Set(const Scale &scale, bool val);

   void Clear();

   // TODO: Figure out what transformations make sense
   /*
   void Transposition(...);
   void Inversion(...);
   void Complement(...);
   void Multiplication(...);
   void Retrograde(...);
   void Rotation(...);
   void CyclePermutation(...);
   void RetrogradeInversion(...);
   */

 private:
   /// Integer representing the pitch class as a value from 0-11
   ///   0 represents C
   std::array<bool, 12> pitch_class_enables_ = {false};

   friend void to_json(nlohmann::json &j, const PitchClassSet &val);
   friend void from_json(const nlohmann::json &j, PitchClassSet &val);
};

/// json serializer
void to_json(nlohmann::json &j, const PitchClassSet &val);
/// json deserializer
void from_json(const nlohmann::json &j, PitchClassSet &val);

}
