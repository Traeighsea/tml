#pragma once

#include "PitchName.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class Note;
}

namespace tml {
typedef int Octave;

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
/// @see [Pitch](https://en.wikipedia.org/wiki/Pitch_(music))
class Pitch {
 public:
   Pitch() = default;
   ~Pitch() = default;
   Pitch(const Pitch &other) = default;
   Pitch(Pitch &&other) = default;
   Pitch &operator=(const Pitch &rhs) = default;
   Pitch &operator=(Pitch &&rhs) = default;

   Pitch(const PitchName &other);
   Pitch(PitchName &&other);
   Pitch &operator=(const PitchName &rhs);
   Pitch &operator=(PitchName &&rhs);

   Pitch(const Note &other);
   Pitch(Note &&other);
   Pitch &operator=(const Note &rhs);
   Pitch &operator=(Note &&rhs);

   Pitch(PitchLetter letter, Accidental accidental, Octave octave);
   Pitch(PitchName pitch_name, Octave octave);

   bool operator==(const Pitch &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Pitch &val);
   std::string ToString() const;

   int ToInt() const;

   PitchClass GetPitchClass() const;

   PitchName GetPitchName() const;
   Octave GetOctave() const;

   void SetPitchName(const PitchName &pitch_name);
   void SetOctave(const Octave &octave);

   // PitchName functions
   PitchLetter GetPitchLetter() const;
   Accidental GetAccidental() const;

   void SetPitchLetter(const PitchLetter &letter);
   void SetAccidental(const Accidental &accidental);
   void AddAccidental(const Accidental &accidental);
   void Flatten();
   void Sharpen();

   /// TODO: Function to get scientific pitch notation

   /// TODO: Function to get Helmholtz pitch notation

 private:
   /// The letter name for purposes of keeping the music theory in tact (as opposed to the midi int
   /// or the pitch class, although pitch classes will be used for calculation most likely). This
   /// will make it easier to build melodic lines from if you already know the note name/letter to
   /// go from the pitch cycle.
   PitchName pitch_name_;
   /// Min of -1, max of 9
   Octave octave_ = 4;

   // Could potentially use inheritance for these values to overwrite the functions for when we
   // obtain the duration
   /*
   /// If you'd rather manipulate the MIDI notes yourself for full control, have fun
   bool usingNoteNumbers_ = false;
   /// Relative to a MIDI note number
   unsigned int noteNumber_ = 0;
   */

   friend void to_json(nlohmann::json &j, const Pitch &val);
   friend void from_json(const nlohmann::json &j, Pitch &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Pitch &val);
/// json deserializer
void from_json(const nlohmann::json &j, Pitch &val);

}
