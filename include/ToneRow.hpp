#pragma once

#include "PitchClass.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class ToneRow {
 public:
   ToneRow() = default;
   ~ToneRow() = default;
   ToneRow(const ToneRow &other) = default;
   ToneRow(ToneRow &&other) = default;
   ToneRow &operator=(const ToneRow &rhs) = default;
   ToneRow &operator=(ToneRow &&rhs) = default;

   bool operator==(const ToneRow &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ToneRow &val);
   std::string ToString() const;

   PitchClass &operator[](int index);
   PitchClass &operator[](unsigned index);

   bool HasUniqueValues() const;
   const std::vector<PitchClass> &GetPitchClasses() const;
   std::vector<PitchClass> &GetPitchClasses();

   PitchClass Get(int index) const;
   PitchClass Get(unsigned index) const;

   /// Returns true on success, will not add pitch_class on failure
   bool Append(const PitchClass &pitch_class);
   bool Prepend(const PitchClass &pitch_class);
   bool Insert(int index, const PitchClass &pitch_class);
   void Remove(int index);
   void Clear();

   int GetSize() const;

   // TODO: Figure out what transformations make sense
   /*
   void Transposition(...);
   void Inversion(...);
   void Complement(...);
   void Multiplication(...);
   void Retrograde(...);
   void Rotation(...);
   void CyclePermutation(...);
   void RetrogradeInversion(...);
   */

 private:
   std::vector<PitchClass> set_;

   /// While enabled, it will prevent you from adding a non unique value to the set
   ///   This inherently disallows more than 12 notes total
   bool unique_values_ = true;

   friend void to_json(nlohmann::json &j, const ToneRow &val);
   friend void from_json(const nlohmann::json &j, ToneRow &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ToneRow &val);
/// json deserializer
void from_json(const nlohmann::json &j, ToneRow &val);

}
