#pragma once

#include "ChordFormula.hpp"
#include <iosfwd>
#include <map>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class Chord;
class PitchName;
class Pitch;
class Note;
}

namespace tml {

/// @brief Container for registering and retrieving a generic formulas for chords.
/// @details Data is stored as key value pairs of a chord name string and a chord formula.
/// Technically the information is duplicated as the ChordFormula also contains the name, however no
/// guarantee that they do match up, though they conceptually should.
class ChordDictionary {
 public:
   ChordDictionary(bool register_common = true);
   ~ChordDictionary() = default;
   ChordDictionary(const ChordDictionary &other) = default;
   ChordDictionary(ChordDictionary &&other) = default;
   ChordDictionary &operator=(const ChordDictionary &rhs) = default;
   ChordDictionary &operator=(ChordDictionary &&rhs) = default;

   bool operator==(const ChordDictionary &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ChordDictionary &val);
   std::string ToString() const;

   bool Register(const ChordFormula &formula);
   bool Unregister(const std::string &name);
   void Clear();
   /// Clears the dictionary and resets to the default chords
   void Reset();

   ChordFormula GetChordFormula(const std::string &name) const;
   /// There may be multiple keys with the same value, so return all
   std::vector<std::string> GetName(const ChordFormula &formula) const;
   Chord GetChord(const Pitch &root, const std::string &name);
   Chord GetChord(const PitchName &root, const std::string &name);
   Chord GetChord(const Note &root, const std::string &name);

   std::vector<std::string> GetRegisteredNames() const;
   const std::map<std::string, ChordFormula> &GetFullDictionary() const;

 private:
   std::map<std::string, ChordFormula> dictionary_;

   /// Registers a collection of commonly used chords
   void RegisterCommon();
   friend void to_json(nlohmann::json &j, const ChordDictionary &val);
   friend void from_json(const nlohmann::json &j, ChordDictionary &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ChordDictionary &val);
/// json deserializer
void from_json(const nlohmann::json &j, ChordDictionary &val);

}