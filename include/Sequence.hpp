#pragma once

#include "Note.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {

/// @TODO: add iterator support

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Sequence {
 public:
   Sequence() = default;
   ~Sequence() = default;
   Sequence(const Sequence &other) = default;
   Sequence(Sequence &&other) = default;
   Sequence &operator=(const Sequence &rhs) = default;
   Sequence &operator=(Sequence &&rhs) = default;

   bool operator==(const Sequence &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Sequence &val);
   std::string ToString() const;

   std::string GetName() const;
   const std::vector<Note> &GetNotes() const;
   std::vector<Note> &GetNotes();

   void SetName(std::string name);

   void AppendNote(Note new_note);
   void PrependNote(Note new_note);
   // Negative indexing goes from the reverse order
   void Insert(int index, Note new_note, bool circular_buffer_indexing = true);
   void Remove(int index, bool circular_buffer_indexing = true);
   void Clear();
   Note GetNote(int index, bool circular_buffer_indexing = true) const;
   Note &GetNote(int index, bool circular_buffer_indexing = true);
   int GetSize() const;

 private:
   std::string name_ = "";
   std::vector<Note> sequence_;

   friend void to_json(nlohmann::json &j, const Sequence &val);
   friend void from_json(const nlohmann::json &j, Sequence &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Sequence &val);
/// json deserializer
void from_json(const nlohmann::json &j, Sequence &val);

}