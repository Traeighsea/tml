#pragma once

#include "PitchName.hpp"
#include <nlohmann/json_fwd.hpp>

namespace tml {
class Pitch;
class Scale;
}
#include <iosfwd>
#include <string>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
/// @see [Scale Degree](https://en.wikipedia.org/wiki/Degree_(music))
class ScaleDegree {
 public:
   ScaleDegree() = default;
   ~ScaleDegree() = default;
   ScaleDegree(const ScaleDegree &other) = default;
   ScaleDegree(ScaleDegree &&other) = default;
   ScaleDegree &operator=(const ScaleDegree &rhs) = default;
   ScaleDegree &operator=(ScaleDegree &&rhs) = default;

   ScaleDegree(int degree, Accidental accidental);
   ScaleDegree(int degree);

   bool operator==(const ScaleDegree &rhs) const;
   bool operator>(const ScaleDegree &rhs) const;
   bool operator<(const ScaleDegree &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ScaleDegree &val);
   std::string ToString() const;

   int ToInt() const;

   // HarmonicFunction ToHarmonicFunction() const;
   // PitchName ToPitchName(const Scale& scale) const;

   int GetDegree() const;
   Accidental GetAccidental() const;

   void SetDegree(int degree);
   void SetAccidental(const Accidental &accidental);
   void AddAccidental(const Accidental &accidental);
   void Flatten();
   void Sharpen();

   /// \brief Returns the pitch information given a scale and the internal scale degree
   PitchName GetPitchNameFromScale(Scale scale) const;

   /// \brief Returns the pitch information given a scale and the internal scale degree
   Pitch GetPitchFromScale(Scale scale) const;

 private:
   /// Confusing name but this converts the scale degree to the int used in pitch class calculations
   int DegreeToPitchClassInt() const;
   /// Note: Keeping as a signed int as it may be useful
   int degree_ = 1;
   Accidental accidental_ = Accidental::Natural;

   friend void to_json(nlohmann::json &j, const ScaleDegree &val);
   friend void from_json(const nlohmann::json &j, ScaleDegree &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ScaleDegree &val);
/// json deserializer
void from_json(const nlohmann::json &j, ScaleDegree &val);

}
