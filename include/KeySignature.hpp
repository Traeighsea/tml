#pragma once

#include "PitchName.hpp"
#include <array>
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

/*
namespace tml {
   class Scale;
}*/

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class KeySignature {
 public:
   KeySignature() = default;
   ~KeySignature() = default;
   KeySignature(const KeySignature &other) = default;
   KeySignature(KeySignature &&other) = default;
   KeySignature &operator=(const KeySignature &rhs) = default;
   KeySignature &operator=(KeySignature &&rhs) = default;

   KeySignature(PitchName root, bool is_major_key = true);
   KeySignature(int num_accidentals, bool is_major_key = true);

   bool operator==(const KeySignature &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const KeySignature &val);
   std::string ToString() const;

   void Set(PitchName root, bool is_major_key = true);
   /// Number of accidentals from -7 to 7 where negative numbers are flat keys and positive are
   /// sharp keys
   void Set(int num_accidentals, bool is_major_key = true);

   int GetNumAccidentals() const;
   bool IsMajor() const;
   const std::array<PitchName, 7> &GetNotes() const;

   PitchName KeyCenter();
   PitchName Root();
   PitchName MinorRoot();
   PitchName MajorRoot();
   std::array<PitchName, 7> PitchNames() const;

   /*
   Scale MajorScale();
   Scale MinorScale();
   Scale MelodicMinorScale();
   Scale IonianMode();
   Scale DorianMode();
   Scale PhrygianMode();
   Scale LydianMode();
   Scale MixolydianMode();
   Scale AeolianMode();
   Scale LocrianMode();
   */

 private:
   /// Number of accidentals in the key
   int accidentals_ = 0;

   /// Wether or not this key is major or minor
   bool major_ = true;

   /// Cached array of PitchNames
   std::array<PitchName, 7> notes_ = {PitchName(PitchLetter::C, Accidental::Natural),
                                      PitchName(PitchLetter::D, Accidental::Natural),
                                      PitchName(PitchLetter::E, Accidental::Natural),
                                      PitchName(PitchLetter::F, Accidental::Natural),
                                      PitchName(PitchLetter::G, Accidental::Natural),
                                      PitchName(PitchLetter::A, Accidental::Natural),
                                      PitchName(PitchLetter::B, Accidental::Natural)};

   friend void to_json(nlohmann::json &j, const KeySignature &val);
   friend void from_json(const nlohmann::json &j, KeySignature &val);
};

/// json serializer
void to_json(nlohmann::json &j, const KeySignature &val);
/// json deserializer
void from_json(const nlohmann::json &j, KeySignature &val);

}
