#pragma once

#include "PitchName.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
class Pitch;
}

namespace tml {

/// @brief  A container object that holds pitches, a root note, and allows naming of this
/// collection.
/// @details   You can have a root not in the collection of pitches contained in the pitches param.
/// This class does not do any analysis or naming automatically, you must update the name yourself.
/// When updating the name you do not have to specify the root, only the chord name excluding the
/// root. Pitches do not contain scale degree information.
///
class Chord {
 public:
   Chord() = default;
   ~Chord() = default;
   Chord(const Chord &other) = default;
   Chord(Chord &&other) = default;
   Chord &operator=(const Chord &rhs) = default;
   Chord &operator=(Chord &&rhs) = default;

   bool operator==(const Chord &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Chord &val);
   std::string ToString() const;

   std::string GetName() const;
   PitchName GetRoot() const;
   std::vector<Pitch> GetPitches() const;

   void SetName(const std::string &name);
   void SetRoot(const PitchName &pitch_name);
   void SetRoot(const Pitch &pitch);
   void SetRoot(const Note &note);

   void AddPitch(const Pitch &pitch);
   // void AddPitch(const ChordFactor &chord_factor);
   void RemovePitch(int index);
   void Clear();

   /// @note: Index will be %'d by the size, so should not go out of bounds
   PitchName GetPitchName(int index) const;
   /// @note: Index will be %'d by the size, so should not go out of bounds
   Pitch GetPitch(int index) const;
   // Pitch GetPitch(const ChordFactor &chord_factor) const;

   int GetSize() const;

 private:
   /// @TODO: Add a way to "calculate" name of chord
   /// Cached value of the name of the chord
   std::string name_ = "";

   /// The root of the chord. Note: Does not always have to contain the root in the pitches_
   PitchName root_;

   /// Structure to hold a collection of notes, sorted by their pitch
   std::vector<Pitch> pitches_;

   // @TODO: Will probably need more metadata to keep chord information accurate
   friend void to_json(nlohmann::json &j, const Chord &val);
   friend void from_json(const nlohmann::json &j, Chord &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Chord &val);
/// json deserializer
void from_json(const nlohmann::json &j, Chord &val);

}
