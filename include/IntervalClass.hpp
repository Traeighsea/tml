#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class PitchClass;
class PitchName;
class Pitch;
class Note;
class Interval;
}

namespace tml {
/// @brief Wrapper around an int for values from 0-6 with additional functions to convert to other
/// objects.
/// @details Useful for mathematical operations with intervals and pitch classes. The interval class
/// will always be condensed into it's smallest form.
/// @see [Interval Class](https://en.wikipedia.org/wiki/Interval_class)
class IntervalClass {
 public:
   IntervalClass() = default;
   ~IntervalClass() = default;
   IntervalClass(const IntervalClass &other) = default;
   IntervalClass(IntervalClass &&other) = default;
   IntervalClass &operator=(const IntervalClass &rhs) = default;
   IntervalClass &operator=(IntervalClass &&rhs) = default;

   IntervalClass(unsigned int ic);
   IntervalClass(int ic);
   IntervalClass(const Interval &interval);
   IntervalClass(const PitchClass &from, const PitchClass &to);
   IntervalClass(const PitchName &from, const PitchName &to);
   IntervalClass(const Pitch &from, const Pitch &to);
   IntervalClass(const Note &from, const Note &to);

   bool operator==(const IntervalClass &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const IntervalClass &val);
   std::string ToString() const;

   // Explicit method to cast to an int. Returns the pitch class int
   int ToInt() const;
   // Explicit method to cast to an unsigned int. Returns the pitch class unsigned int
   unsigned int ToUnsignedInt() const;

   unsigned int Get() const;

   void Set(unsigned int ic);
   void Set(int ic);
   void Set(const Interval &interval);
   void Set(const PitchClass &from, const PitchClass &to);
   void Set(const PitchName &from, const PitchName &to);
   void Set(const Pitch &from, const Pitch &to);
   void Set(const Note &from, const Note &to);

 private:
   /// Integer representing the integer class 0-6
   unsigned int ic_ = 0;

   friend void to_json(nlohmann::json &j, const IntervalClass &val);
   friend void from_json(const nlohmann::json &j, IntervalClass &val);
};

/// json serializer
void to_json(nlohmann::json &j, const IntervalClass &val);
/// json deserializer
void from_json(const nlohmann::json &j, IntervalClass &val);

}
