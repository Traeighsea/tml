#pragma once

#include "Duration.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
/// @TODO(traeighsea) Revisit this implementation

/// @brief This class describes the start of a note. The start is a combination of the beat plus a
/// duration offset.
/// @details For example a section in common time with the beat_ of 2 and a duration_offset_ of an
/// eighth would start on the and of 2: | 1 + 2 (+) 3 + 4 + |
/// Negative beats are supported for the purposes of Anacrusis (pickup notes), and 0 has no meaning
/// in this context.
class NoteOnset {
 public:
   NoteOnset() = default;
   ~NoteOnset() = default;
   NoteOnset(const NoteOnset &other) = default;
   NoteOnset(NoteOnset &&other) = default;
   NoteOnset &operator=(const NoteOnset &rhs) = default;
   NoteOnset &operator=(NoteOnset &&rhs) = default;

   NoteOnset(int beat);
   NoteOnset(int beat, Duration duration_offset);

   bool operator==(const NoteOnset &rhs) const;
   bool operator>(const NoteOnset &rhs) const;
   bool operator<(const NoteOnset &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const NoteOnset &val);
   std::string ToString() const;

   int GetBeat() const;
   bool HasDurationOfset() const;
   Duration GetDurationOffset() const;

   void SetBeat(int beat);
   void SetHasDurationOffset(bool has_duration_offset);
   /// Note: This also sets the has_duration_offset_ to true as it's implied you want to use this
   /// value
   void SetDurationOffset(Duration duration_offset);

 private:
   int beat_ = 1;
   // TODO: make this an std::optional
   bool has_duration_offset_ = false;
   Duration duration_offset_;

   friend void to_json(nlohmann::json &j, const NoteOnset &val);
   friend void from_json(const nlohmann::json &j, NoteOnset &val);
};

/// json serializer
void to_json(nlohmann::json &j, const NoteOnset &val);
/// json deserializer
void from_json(const nlohmann::json &j, NoteOnset &val);

}