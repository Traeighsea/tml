#pragma once

#include "Duration.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
/// @brief Container class for a collection of durations
/// @details @TODO(traeighsea)
class Rhythm {
 public:
   Rhythm() = default;
   ~Rhythm() = default;
   Rhythm(const Rhythm &other) = default;
   Rhythm(Rhythm &&other) = default;
   Rhythm &operator=(const Rhythm &rhs) = default;
   Rhythm &operator=(Rhythm &&rhs) = default;

   bool operator==(const Rhythm &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Rhythm &val);
   std::string ToString() const;

   std::string GetName() const;
   const std::vector<Duration> &GetDurations() const;
   std::vector<Duration> &GetDurations();

   void SetName(std::string name);

   void Append(Duration new_duration);
   void Prepend(Duration new_duration);
   // Negative indexing goes from the reverse order
   void Insert(int index, Duration new_duration, bool circular_buffer_indexing = true);
   void Remove(int index, bool circular_buffer_indexing = true);
   void Clear();
   Duration GetDuration(int index, bool circular_buffer_indexing = true) const;
   Duration &GetDuration(int index, bool circular_buffer_indexing = true);
   int GetSize() const;

 private:
   std::string name_ = "";
   std::vector<Duration> durations_;

   friend void to_json(nlohmann::json &j, const Rhythm &val);
   friend void from_json(const nlohmann::json &j, Rhythm &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Rhythm &val);
/// json deserializer
void from_json(const nlohmann::json &j, Rhythm &val);

}
