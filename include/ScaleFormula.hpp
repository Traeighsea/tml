#pragma once

#include "ScaleDegree.hpp"
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
class Interval;
class PitchName;
class Scale;
}

namespace tml {

/// @brief Builder class for scales composed of a formula for how to build a particular scale
/// @details @TODO(traeighsea)
class ScaleFormula {
 public:
   ScaleFormula() = default;
   ~ScaleFormula() = default;
   ScaleFormula(const ScaleFormula &other) = default;
   ScaleFormula(ScaleFormula &&other) = default;
   ScaleFormula &operator=(const ScaleFormula &rhs) = default;
   ScaleFormula &operator=(ScaleFormula &&rhs) = default;

   ScaleFormula(const std::string &name, const std::vector<ScaleDegree> &formula);

   bool operator==(const ScaleFormula &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ScaleFormula &val);
   std::string ToString() const;

   std::string GetName() const;
   std::vector<ScaleDegree> GetFormula();

   void SetName(const std::string &name);

   void AddScaleDegree(const ScaleDegree &scale_degree);
   void RemoveScaleDegree(int index);
   void Clear();

   int GetSize() const;

   std::vector<Interval> GetIntervalDistances();
   Scale ApplyScale(const PitchName &root);

 private:
   std::string name_;
   /// The ScaleDegree in this context correlates to how this scale relates to the major scale
   ///   The benefit of relating to the major scale is to allow us to spell correctly
   std::vector<ScaleDegree> formula_;

   /* This might not be needed considering you an calculate the invalic relationship from the
   ScaleDegrees
   /// Intervalic relationship between each consecutive note
   ///   The [0]th element is the distance between the root and the second, etc.
   ///   The [size-1] element is the distance from the last note to the root to wrap back around
   std::vector<Interval> interval_distances_;
   */
   friend void to_json(nlohmann::json &j, const ScaleFormula &val);
   friend void from_json(const nlohmann::json &j, ScaleFormula &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ScaleFormula &val);
/// json deserializer
void from_json(const nlohmann::json &j, ScaleFormula &val);

}
