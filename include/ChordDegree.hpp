#pragma once

#include "PitchName.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class Pitch;
class Note;
class Chord;
}

namespace tml {

/// @brief Representation of chord degrees as a degree and accidental.
/// @details Chord degrees can be positive and negative, however a chord degree of 0 does not have
/// meaning in this class. Chord degrees start at index 1, which would be the root.
class ChordDegree {
 public:
   ChordDegree() = default;
   ~ChordDegree() = default;
   ChordDegree(const ChordDegree &other) = default;
   ChordDegree(ChordDegree &&other) = default;
   ChordDegree &operator=(const ChordDegree &rhs) = default;
   ChordDegree &operator=(ChordDegree &&rhs) = default;

   ChordDegree(int degree, Accidental accidental);
   ChordDegree(int degree);

   bool operator==(const ChordDegree &rhs) const;
   bool operator>(const ChordDegree &rhs) const;
   bool operator<(const ChordDegree &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const ChordDegree &val);
   std::string ToString() const;

   /// \brief Explicit method to cast to an int. Returns the pitch class int
   int ToInt() const;
   // HarmonicFunction ToHarmonicFunction() const;
   // PitchClass ToPitchClass() const;

   int GetDegree() const;
   Accidental GetAccidental() const;

   void SetDegree(int degree);
   void SetAccidental(const Accidental &accidental);
   void AddAccidental(const Accidental &accidental);
   void Flatten();
   void Sharpen();

   /// \brief Returns the pitch information given a chord and the chord degree
   PitchName GetPitchNameFromChord(Chord chord) const;

   /// \brief Returns the pitch information given a chord and the chord degree
   Pitch GetPitchFromChord(Chord chord) const;

   /// \brief Returns the pitch information given a chord and the chord degree
   Note GetNoteFromChord(Chord chord) const;

 private:
   /// Confusing name but this converts the scale degree to the int used in pitch class calculations
   int DegreeToPitchClassInt() const;
   // Note: Keeping as a signed int as it may be useful
   int degree_ = 1;
   Accidental accidental_ = Accidental::Natural;
   friend void to_json(nlohmann::json &j, const ChordDegree &val);
   friend void from_json(const nlohmann::json &j, ChordDegree &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ChordDegree &val);
/// json deserializer
void from_json(const nlohmann::json &j, ChordDegree &val);

}
