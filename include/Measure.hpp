#pragma once

#include "Duration.hpp"
#include "KeySignature.hpp"
#include "Note.hpp"
#include "NoteOnset.hpp"
#include "TimeSignature.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <utility>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Measure {
 public:
   Measure() = default;
   ~Measure() = default;
   Measure(const Measure &other) = default;
   Measure(Measure &&other) = default;
   Measure &operator=(const Measure &rhs) = default;
   Measure &operator=(Measure &&rhs) = default;

   Measure(KeySignature key_signature, TimeSignature time_signature);

   bool operator==(const Measure &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Measure &val);
   std::string ToString() const;

   KeySignature GetKeySignature() const;
   TimeSignature GetTimeSignature() const;

   void SetKeySignature(KeySignature key_signature);
   void SetTimeSignature(TimeSignature time_signature);

   // TODO: Add a return code that avoids adding notes if it would go outside the bounds
   // of the measure
   void Add(NoteOnset start, Note new_note);
   void Remove(int index, bool circular_buffer_indexing = true);

   // TODO: potentially add prepend and append that would calculate where to add the note based on
   // the surrounding notes

   void Clear();
   const std::vector<std::pair<NoteOnset, Note>> &GetNotes() const;
   std::pair<NoteOnset, Note> GetNote(int index, bool circular_buffer_indexing = true) const;
   std::pair<NoteOnset, Note> &GetNote(int index, bool circular_buffer_indexing = true);
   int GetNumberOfNotes() const;

 private:
   KeySignature key_signature_;
   TimeSignature time_signature_;
   std::vector<std::pair<NoteOnset, Note>> notes_;

   friend void to_json(nlohmann::json &j, const Measure &val);
   friend void from_json(const nlohmann::json &j, Measure &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Measure &val);
/// json deserializer
void from_json(const nlohmann::json &j, Measure &val);

}