#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class Pitch;
class Note;
class PitchClass;
}

namespace tml {

/// @brief @TODO(traeighsea)
enum class PitchLetter : int {
   C = 0,
   D = 2,
   E = 4,
   F = 5,
   G = 7,
   A = 9,
   B = 11
};
// Stream operator for pitch letters
std::ostream &operator<<(std::ostream &out, const PitchLetter &val);
std::string ToString(const PitchLetter &val);

/// @brief @TODO(traeighsea)
enum class Accidental : int {
   DoubleFlat = -2,
   Flat = -1,
   Natural = 0,
   Sharp = 1,
   DoubleSharp = 2
};
// Stream operator for accidentals
std::ostream &operator<<(std::ostream &out, const Accidental &val);
std::string ToString(const Accidental &val);

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class PitchName {
 public:
   PitchName() = default;
   ~PitchName() = default;
   PitchName(const PitchName &other) = default;
   PitchName(PitchName &&other) = default;
   PitchName &operator=(const PitchName &rhs) = default;
   PitchName &operator=(PitchName &&rhs) = default;

   PitchName(const Pitch &other);
   PitchName(Pitch &&other);
   PitchName &operator=(const Pitch &rhs);
   PitchName &operator=(Pitch &&rhs);

   PitchName(const Note &other);
   PitchName(Note &&other);
   PitchName &operator=(const Note &rhs);
   PitchName &operator=(Note &&rhs);

   PitchName(PitchLetter letter, Accidental accidental);

   bool operator==(const PitchName &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const PitchName &val);
   std::string ToString() const;

   // Explicit method to cast to an int. Returns the pitch class int
   int ToInt() const;

   PitchClass GetPitchClass() const;

   PitchLetter GetPitchLetter() const;
   Accidental GetAccidental() const;

   void SetPitchLetter(const PitchLetter &letter);
   void SetAccidental(const Accidental &accidental);
   void AddAccidental(const Accidental &accidental);
   void Flatten();
   void Sharpen();

 protected:
   PitchLetter letter_ = PitchLetter::C;
   Accidental accidental_ = Accidental::Natural;

   friend void to_json(nlohmann::json &j, const PitchName &val);
   friend void from_json(const nlohmann::json &j, PitchName &val);
};

/// json serializer
void to_json(nlohmann::json &j, const PitchName &val);
/// json deserializer
void from_json(const nlohmann::json &j, PitchName &val);

}
