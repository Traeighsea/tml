#pragma once

#include "Measure.hpp"
#include "Voice.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Part {
 public:
   Part() = default;
   ~Part() = default;
   Part(const Part &other) = default;
   Part(Part &&other) = default;
   Part &operator=(const Part &rhs) = default;
   Part &operator=(Part &&rhs) = default;

   Part(std::string part_name, Instrument instrument, int num_measures);
   Part(Voice voice, int num_measures);

   bool operator==(const Part &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Part &val);
   std::string ToString() const;

   Measure &operator[](int index);
   Measure &operator[](unsigned index);

   Voice GetVoice() const;
   std::vector<Measure> GetMeasures() const;
   std::vector<Measure> &GetMeasures();

   void SetVoice(Voice voice);

   void Append(Measure new_measure);
   void Prepend(Measure new_measure);
   // Negative indexing goes from the reverse order
   void Insert(int index, Measure new_measure, bool circular_buffer_indexing = true);
   void Remove(int index, bool circular_buffer_indexing = true);
   void Clear();
   Measure GetMeasure(int index, bool circular_buffer_indexing = true) const;
   Measure &GetMeasure(int index, bool circular_buffer_indexing = true);
   int GetNumberOfMeasures() const;

   // Functions from Voice
   std::string GetName() const;
   Instrument GetInstrument() const;

   void SetName(std::string name);
   void SetInstrument(Instrument instrument);

 private:
   Voice voice_;
   std::vector<Measure> measures_;

   friend void to_json(nlohmann::json &j, const Part &val);
   friend void from_json(const nlohmann::json &j, Part &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Part &val);
/// json deserializer
void from_json(const nlohmann::json &j, Part &val);

}