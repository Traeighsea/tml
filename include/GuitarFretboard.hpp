#pragma once

#include "Pitch.hpp"
#include "PitchName.hpp"
#include "ScaleDegree.hpp"
#include <array>
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
class Scale;
class ScaleFormula;
}

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class GuitarFretboard {
 public:
   GuitarFretboard() = default;
   ~GuitarFretboard() = default;
   GuitarFretboard(const GuitarFretboard &other) = default;
   GuitarFretboard(GuitarFretboard &&other) = default;
   GuitarFretboard &operator=(const GuitarFretboard &rhs) = default;
   GuitarFretboard &operator=(GuitarFretboard &&rhs) = default;

   bool operator==(const GuitarFretboard &rhs) const;
   bool operator>(const GuitarFretboard &rhs) const;
   bool operator<(const GuitarFretboard &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const GuitarFretboard &val);
   std::string ToString() const;

   /// The String is 1 based indexing starting with the highest string to make it consistent with
   /// guitarist's use
   void SetStringTuning(unsigned int string, tml::Pitch);

   void SetRoot(tml::PitchName pitch_name);

   void SetPitchClassName(unsigned int pitch_class, tml::PitchName);
   // void SetPitchClassName(tml::PitchClass pitch_class, tml::PitchName);
   void SetPitchClassName(tml::PitchName);

   void EnablePitchClass(unsigned int pitch_class);
   // void EnablePitchClass(tml::PitchClass pitch_class);
   void DisablePitchClass(unsigned int pitch_class);
   // void DisablePitchClass(tml::PitchClass pitch_class);
   void TogglePitchClass(unsigned int pitch_class);
   // void TogglePitchClass(tml::PitchClass pitch_class);

   void EnableOutputScaleDegrees();
   void DisableOutputScaleDegrees();

   const std::vector<tml::Pitch> &GetTuning() const;
   tml::PitchName GetRoot() const;
   const std::array<tml::PitchName, 12> &GetPitchClassBuffer() const;
   const std::array<bool, 12> &GetPitchClassEnables() const;
   std::array<tml::ScaleDegree, 12> GetScaleDegreeBuffer() const;
   bool GetOutputScaleDegrees() const;

   void SetFromScale(tml::Scale scale);
   /// Temporarily set by pairing these two values together, should be able to calculate in either
   /// direction assuming the root of the scale is the first note, and the root is set prior to
   /// using only the scale formula
   void SetFromScaleAndScaleFormula(tml::Scale scale, tml::ScaleFormula scale_formula);

 private:
   /// Keeps the tuning (or starting note) of each string. Get the size to know the number of
   /// strings, should be able to handle more or less strings without issue
   std::vector<tml::Pitch> tuning_{tml::Pitch(tml::PitchLetter::E, tml::Accidental::Natural, 4),
                                   tml::Pitch(tml::PitchLetter::B, tml::Accidental::Natural, 3),
                                   tml::Pitch(tml::PitchLetter::G, tml::Accidental::Natural, 3),
                                   tml::Pitch(tml::PitchLetter::D, tml::Accidental::Natural, 3),
                                   tml::Pitch(tml::PitchLetter::A, tml::Accidental::Natural, 2),
                                   tml::Pitch(tml::PitchLetter::E, tml::Accidental::Natural, 2)};

   tml::PitchName root_ = {tml::PitchName(tml::PitchLetter::C, tml::Accidental::Natural)};

   /// ~~Circular buffer~~ Array of pitches indexed by the pitch class
   std::array<tml::PitchName, 12> pitch_class_buffer_{
         tml::PitchName{tml::PitchLetter::C, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::D, tml::Accidental::Flat},
         tml::PitchName{tml::PitchLetter::D, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::E, tml::Accidental::Flat},
         tml::PitchName{tml::PitchLetter::E, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::F, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::G, tml::Accidental::Flat},
         tml::PitchName{tml::PitchLetter::G, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::A, tml::Accidental::Flat},
         tml::PitchName{tml::PitchLetter::A, tml::Accidental::Natural},
         tml::PitchName{tml::PitchLetter::B, tml::Accidental::Flat},
         tml::PitchName{tml::PitchLetter::B, tml::Accidental::Natural}};

   /// ~~Circular buffer~~ Array of bools indexed by the pitch class for enabling and disabling
   /// pitches
   std::array<bool, 12> pitch_class_enables_{true,  false, true,  false, true,  true,
                                             false, true,  false, true,  false, true};

   /// ~~Circular buffer~~ Array of pitches indexed by the pitch class
   std::array<tml::ScaleDegree, 12> scale_degree_buffer_{
         tml::ScaleDegree{1, tml::Accidental::Natural},
         tml::ScaleDegree{2, tml::Accidental::Flat},
         tml::ScaleDegree{2, tml::Accidental::Natural},
         tml::ScaleDegree{3, tml::Accidental::Flat},
         tml::ScaleDegree{3, tml::Accidental::Natural},
         tml::ScaleDegree{4, tml::Accidental::Natural},
         tml::ScaleDegree{5, tml::Accidental::Flat},
         tml::ScaleDegree{5, tml::Accidental::Natural},
         tml::ScaleDegree{6, tml::Accidental::Flat},
         tml::ScaleDegree{6, tml::Accidental::Natural},
         tml::ScaleDegree{7, tml::Accidental::Flat},
         tml::ScaleDegree{7, tml::Accidental::Natural}};

   /// Enable this to output using scale degrees instead of pitches
   bool output_scale_degrees_ = false;

   friend void to_json(nlohmann::json &j, const GuitarFretboard &val);
   friend void from_json(const nlohmann::json &j, GuitarFretboard &val);
};

/// json serializer
void to_json(nlohmann::json &j, const GuitarFretboard &val);
/// json deserializer
void from_json(const nlohmann::json &j, GuitarFretboard &val);

}