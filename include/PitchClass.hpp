#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class PitchName;
class Pitch;
class Note;
}

namespace tml {

/// @brief Fundamentally this is a wrapper around an int for values from 0-11 with
///   additional functions to convert to other objects.
/// @details Pitch classes are really useful for genericized musical computations.
/// @see [Pitch Class](https://en.wikipedia.org/wiki/Pitch_class)
class PitchClass {
 public:
   PitchClass() = default;
   ~PitchClass() = default;
   PitchClass(const PitchClass &other) = default;
   PitchClass(PitchClass &&other) = default;
   PitchClass &operator=(const PitchClass &rhs) = default;
   PitchClass &operator=(PitchClass &&rhs) = default;

   bool operator==(const PitchClass &rhs) const;
   bool operator>(const PitchClass &rhs) const;
   bool operator<(const PitchClass &rhs) const;

   PitchClass(unsigned int pc);
   PitchClass(int pc);
   PitchClass(const PitchName &pitch_name);
   PitchClass(const Pitch &pitch);
   PitchClass(const Note &note);

   friend std::ostream &operator<<(std::ostream &out, const PitchClass &val);
   std::string ToString() const;

   // Explicit method to cast to an int. Returns the pitch class int
   int ToInt() const;
   // Explicit method to cast to an unsigned int. Returns the pitch class unsigned int
   unsigned int ToUnsignedInt() const;

   unsigned int Get() const;

   void Set(unsigned int pc);
   void Set(int pc);
   void Set(const PitchName &pitch_name);
   void Set(const Pitch &pitch);
   void Set(const Note &note);

 private:
   /// Integer representing the pitch class as a value from 0-11
   unsigned int pc_ = 0;

   friend void to_json(nlohmann::json &j, const PitchClass &val);
   friend void from_json(const nlohmann::json &j, PitchClass &val);
};

/// json serializer
void to_json(nlohmann::json &j, const PitchClass &val);
/// json deserializer
void from_json(const nlohmann::json &j, PitchClass &val);

}
