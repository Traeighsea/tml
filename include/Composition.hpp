#pragma once

#include "Part.hpp"
#include "Tempo.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Composition {
 public:
   Composition() = default;
   ~Composition() = default;
   Composition(const Composition &other) = default;
   Composition(Composition &&other) = default;
   Composition &operator=(const Composition &rhs) = default;
   Composition &operator=(Composition &&rhs) = default;

   Composition(std::string name, double bpm);

   bool operator==(const Composition &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Composition &val);
   std::string ToString() const;

   Part &operator[](int index);
   Part &operator[](unsigned index);

   std::string GetName() const;
   double GetBpm() const;

   void SetName(std::string name);
   void SetBpm(Tempo tempo);
   void SetBpm(double bpm);

   std::vector<Part> GetParts() const;
   std::vector<Part> &GetParts();

   int GetNumberOfParts() const;
   Part &GetPart(unsigned index);
   Part GetPart(unsigned index) const;
   /// Returning -1 is a failure to find the part
   int GetPartIndex(std::string part_name);

   /// Returns the index of the added part
   unsigned AddPart(std::string part_name, Instrument instrument, int num_measures);
   void InsertPart(unsigned index, std::string part_name, Instrument instrument, int num_measures);
   void RemovePart(unsigned index);
   void RemovePart(std::string part_name);

   // @TODO: Add Part functions
   // @TODO: Add [] operator

 private:
   std::string name_ = "";
   std::vector<Part> parts_;
   double bpm_ = static_cast<double>(Tempo::Moderato);

   friend void to_json(nlohmann::json &j, const Composition &val);
   friend void from_json(const nlohmann::json &j, Composition &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Composition &val);
/// json deserializer
void from_json(const nlohmann::json &j, Composition &val);

}