#pragma once

#include "PitchName.hpp"
#include <array>
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class CircleOfFifths {
 public:
   CircleOfFifths() = default;
   ~CircleOfFifths() = default;
   CircleOfFifths(const CircleOfFifths &other) = default;
   CircleOfFifths(CircleOfFifths &&other) = default;
   CircleOfFifths &operator=(const CircleOfFifths &rhs) = default;
   CircleOfFifths &operator=(CircleOfFifths &&rhs) = default;

   bool operator==(const CircleOfFifths &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const CircleOfFifths &val);
   std::string ToString() const;

   PitchName GetCurrentRoot() const;

   /// Returns the new pitch name
   const PitchName &CycleUp(int num = 1);
   const PitchName &UpAFifth();

   /// Returns the new pitch name
   const PitchName &CycleDown(int num = 1);
   const PitchName &DownAFourth();

   /// Resets to C major
   const PitchName &Reset();

   std::vector<PitchName> CalculateRange(const PitchName &start, int num = 1);

 private:
   PitchName current_ = {PitchLetter::C, Accidental::Natural};
   friend void to_json(nlohmann::json &j, const CircleOfFifths &val);
   friend void from_json(const nlohmann::json &j, CircleOfFifths &val);
};

/// json serializer
void to_json(nlohmann::json &j, const CircleOfFifths &val);
/// json deserializer
void from_json(const nlohmann::json &j, CircleOfFifths &val);

}
