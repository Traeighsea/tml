#pragma once

#include <array>
#include <math.h>
#include <vector>

/// This file contains a collection of common utility functions for music theory

/// Namespace Traeighsea's Music (Theory) Library
namespace tml {
/// n represents a midi note to obtain a frequency for
double GetFrequency(int midi_note);

/// We use a lot of circular buffers, so here we create a templated method to index into these
/// vectors/arrays
template <typename T> int CircularIndex(const std::vector<T> &container, int index) {
   return ((index % container.size()) + container.size()) % container.size();
}

/// We use a lot of circular buffers, so here we create a templated method to index into these
/// vectors/arrays
template <typename T, std::size_t N>
int CircularIndex(const std::array<T, N> &container, int index) {
   return ((index % container.size()) + container.size()) % container.size();
}

int CircularIndex(int size, int index);

}
