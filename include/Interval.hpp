#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {
class PitchName;
class Pitch;
class Note;
class IntervalClass;
}

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
enum class NamedInterval {
   PerfectUnison = 0,
   MinorSecond = 1,
   MajorSecond = 2,
   MinorThird = 3,
   MajorThird = 4,
   PerfectFourth = 5,
   AugmentedFourth = 6,
   DiminishedFifth = -6,
   PerfectFifth = 7,
   MinorSixth = 8,
   MajorSixth = 9,
   MinorSeventh = 10,
   MajorSeventh = 11,
   PerfectOctave = 12,

   // Other names for these intervals
   Semitone = 1,
   Tone = 2,
   HalfTone = 1,
   WholeTone = 2,
   HalfStep = 1,
   WholeStep = 2
};

/// Stream operator for NamedInterval
std::ostream &operator<<(std::ostream &out, const NamedInterval &val);
std::string ToString(const NamedInterval &val);

/// @brief @TODO(traeighsea)
enum class IntervalQuality {
   Minor,
   Major,
   Diminished,
   Augmented,
   Perfect
};

/// @brief @TODO(traeighsea)
enum Direction {
   Down = -1,
   None = 0,
   Up = 1
};

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Interval {
 public:
   Interval() = default;
   ~Interval() = default;
   Interval(const Interval &other) = default;
   Interval(Interval &&other) = default;
   Interval &operator=(const Interval &rhs) = default;
   Interval &operator=(Interval &&rhs) = default;

   bool operator==(const Interval &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Interval &val);
   std::string ToString() const;

   // Type Conversion
   Interval(int distance);
   Interval(int from, int to);
   Interval(const PitchName &from, const PitchName &to);
   Interval(const Pitch &from, const Pitch &to);
   Interval(const Note &from, const Note &to);

   IntervalClass GetIntervalClass() const;

   // Getters
   int GetDistance() const;
   Direction GetDirection() const;
   NamedInterval GetNamedInterval() const;
   IntervalQuality GetIntervalQuality() const;
   int GetIntervalNumber() const;
   std::string GetName() const;

   // Setters

   /// Most specific setter for most accurate retention of information
   void Set(IntervalQuality quality, int interval_number, Direction direction = Direction::Up);

   /// Most specific setter for most accurate retention of information
   void Set(NamedInterval named_interval, Direction direction = Direction::Up);

   /// Will attempt to update other values based on distance but will lose some "precision" of
   /// accurate information use negative or positive for direction
   void Set(int distance);

   // Functionality

   /// Inverts the distance, so perfect fifths would be perfect fourths
   void Invert();

 private:
   /// Distance of interval. Negative or positive indicates direction
   int distance_ = 0;

   /// Quality of the interval such as minor, major, perfect, diminished, augmented
   IntervalQuality quality_ = IntervalQuality::Perfect;

   /// Number that correlates more or less to the diatonic scale 1-8
   int interval_number_ = 1;

   /// Cached interval name, sets name whenever distance_ is modified
   std::string name_ = "";

   friend void to_json(nlohmann::json &j, const Interval &val);
   friend void from_json(const nlohmann::json &j, Interval &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Interval &val);
/// json deserializer
void from_json(const nlohmann::json &j, Interval &val);

}
