#pragma once

namespace tml {
namespace cfg {
// @TODO: remove temporary solution for configs
// @TODO: Add unicode symbols

// PitchName

/// Use unicode values for accidentals
extern bool USE_UNICODE_ACCIDENTALS;
/// Always print naturals
extern bool USE_NATURALS_ALWAYS;

// Dynamic

/// Use short names for dynamics
extern bool USE_SHORTENED_DYNAMIC_NAMES;
/// Always output the default dynamic value (Mezzo)
extern bool USE_OUTPUT_MEZZO;

// Intervals
/// Use for two character interval names
extern bool USE_SHORTENED_INTERVAL_NAMES;

// ScaleDictionary
/// Each bit registers a different set of scales
extern unsigned int SCALE_DICTIONARY_SCALES_TO_REGISTER;
}

extern const unsigned int SCALE_DICTIONARY_REGISTER_COMMON;
extern const unsigned int SCALE_DICTIONARY_REGISTER_MODERN;
extern const unsigned int SCALE_DICTIONARY_REGISTER_EXTENDED;
extern const unsigned int SCALE_DICTIONARY_REGISTER_NON_TWELVE_EDO;

}
