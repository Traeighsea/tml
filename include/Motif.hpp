#pragma once

#include "Note.hpp"
#include "NoteOnset.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <utility>
#include <vector>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Motif {
 public:
   Motif() = default;
   ~Motif() = default;
   Motif(const Motif &other) = default;
   Motif(Motif &&other) = default;
   Motif &operator=(const Motif &rhs) = default;
   Motif &operator=(Motif &&rhs) = default;

   bool operator==(const Motif &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Motif &val);
   std::string ToString() const;

   std::string GetName() const;
   const std::vector<std::pair<NoteOnset, Note>> &GetNotes() const;
   std::vector<std::pair<NoteOnset, Note>> &GetNotes();

   void SetName(std::string name);

   void Add(NoteOnset start, Note new_note);
   void Remove(int index, bool circular_buffer_indexing = true);
   void Clear();
   std::pair<NoteOnset, Note> GetNote(int index, bool circular_buffer_indexing = true) const;
   std::pair<NoteOnset, Note> &GetNote(int index, bool circular_buffer_indexing = true);
   int GetSize() const;

 private:
   std::string name_ = "";
   std::vector<std::pair<NoteOnset, Note>> notes_;

   friend void to_json(nlohmann::json &j, const Motif &val);
   friend void from_json(const nlohmann::json &j, Motif &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Motif &val);
/// json deserializer
void from_json(const nlohmann::json &j, Motif &val);

}