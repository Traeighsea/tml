#pragma once

#include "Instrument.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Voice {
 public:
   Voice() = default;
   ~Voice() = default;
   Voice(const Voice &other) = default;
   Voice(Voice &&other) = default;
   Voice &operator=(const Voice &rhs) = default;
   Voice &operator=(Voice &&rhs) = default;

   Voice(std::string part_name, Instrument instrument);

   bool operator==(const Voice &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Voice &val);
   std::string ToString() const;

   std::string GetName() const;
   Instrument GetInstrument() const;

   void SetName(std::string name);
   void SetInstrument(Instrument instrument);

 private:
   std::string name_ = "";
   Instrument instrument_ = Instrument::None;

   friend void to_json(nlohmann::json &j, const Voice &val);
   friend void from_json(const nlohmann::json &j, Voice &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Voice &val);
/// json deserializer
void from_json(const nlohmann::json &j, Voice &val);

}