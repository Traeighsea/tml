#pragma once

#include "ChordDegree.hpp"
#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>

namespace tml {
class Interval;
class PitchName;
class Chord;
}

namespace tml {

/// @brief Container class for chords composed of a genericised formula containing chord degrees for
/// how to build a particular chord
/// @details The formula assumes chord degrees are relative to the root and from a major key.
class ChordFormula {
 public:
   ChordFormula() = default;
   ~ChordFormula() = default;
   ChordFormula(const ChordFormula &other) = default;
   ChordFormula(ChordFormula &&other) = default;
   ChordFormula &operator=(const ChordFormula &rhs) = default;
   ChordFormula &operator=(ChordFormula &&rhs) = default;

   ChordFormula(const std::string &name, const std::vector<ChordDegree> &formula);

   bool operator==(const ChordFormula &rhs) const;

   void operator+=(const ChordDegree &rhs);
   void operator+=(const ChordFormula &rhs);
   void operator+=(const std::vector<ChordDegree> &rhs);

   void operator-=(const ChordDegree &rhs);
   void operator-=(const ChordFormula &rhs);
   void operator-=(const std::vector<ChordDegree> &rhs);

   friend std::ostream &operator<<(std::ostream &out, const ChordFormula &val);
   std::string ToString() const;

   std::string GetName() const;
   std::vector<ChordDegree> GetFormula() const;
   std::vector<ChordDegree> &GetFormula();

   int GetSize() const;

   std::vector<Interval> GetIntervalDistances();
   Chord ApplyChord(const PitchName &root);

   void SetName(const std::string &name);

   /// TODO: Currently when adding a chord degree we sort it, do we want to keep this behavior?
   void AddChordDegree(const ChordDegree &chord_degree);
   void RemoveChordDegree(int index);
   void RemoveChordDegree(const ChordDegree &chord_degree);
   void Clear();

 private:
   std::string name_;

   /// @brief The ChordDegree in this context correlates to how this chord relates to the major
   /// scale.
   /// @details The benefit of relating to the major scale is to allow us to spell correctly.
   std::vector<ChordDegree> formula_;
   friend void to_json(nlohmann::json &j, const ChordFormula &val);
   friend void from_json(const nlohmann::json &j, ChordFormula &val);
};

/// json serializer
void to_json(nlohmann::json &j, const ChordFormula &val);
/// json deserializer
void from_json(const nlohmann::json &j, ChordFormula &val);

}
