#pragma once

#include <iosfwd>
#include <nlohmann/json_fwd.hpp>
#include <string>

namespace tml {

/// @brief A representation of dynamic markings like you'd find in sheet music.
/// @details The numbers correlate to the midi velocity. Feel free to modify these values as midi
/// velocity can be finicky.
enum class DynamicMarking : unsigned int {
   fff = 127,
   ff = 111,
   f = 95,
   mf = 79,
   m = 71,
   mp = 63,
   p = 47,
   pp = 31,
   ppp = 15,

   none = 71, // Default

   Fortississimo = 127,
   Fortissimo = 111,
   Forte = 95,
   MezzoForte = 79,
   Mezzo = 71,
   MezzoPiano = 63,
   Piano = 47,
   Pianissimo = 31,
   Pianississimo = 15
};

std::ostream &operator<<(std::ostream &out, const DynamicMarking &val);
std::string ToString(const DynamicMarking &val);

/// @brief @TODO(traeighsea)
/// @details @TODO(traeighsea)
class Dynamic {
 public:
   Dynamic() = default;
   ~Dynamic() = default;
   Dynamic(const Dynamic &other) = default;
   Dynamic(Dynamic &&other) = default;
   Dynamic &operator=(const Dynamic &rhs) = default;
   Dynamic &operator=(Dynamic &&rhs) = default;

   Dynamic(const DynamicMarking &dynamic_marking);

   bool operator==(const Dynamic &rhs) const;

   friend std::ostream &operator<<(std::ostream &out, const Dynamic &val);
   std::string ToString() const;

   void SetDynamicMarking(const DynamicMarking &dynamic_marking);

   DynamicMarking GetDynamicMarking() const;

 private:
   /// The loudness of the sound. The dynamicMarking corelates to midi velocity
   DynamicMarking dynamic_marking_ = DynamicMarking::none;

   /// @TODO(traeighsea) Could potentially use inheritance for these values to overwrite the
   /// functions for when we obtain the velocity
   /*
   /// If you would like to have control of velocity through the number directly, enable this bool
   bool usingVelocity_ = false;
   /// Velocity is a number between 0 and 127 and correlates directly to the midi velocity
   unsigned int velocity_ = 71;
   */
   friend void to_json(nlohmann::json &j, const Dynamic &val);
   friend void from_json(const nlohmann::json &j, Dynamic &val);
};

/// json serializer
void to_json(nlohmann::json &j, const Dynamic &val);
/// json deserializer
void from_json(const nlohmann::json &j, Dynamic &val);

}
