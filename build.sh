#!/bin/bash

function usage {
   echo "Usage: ./build.sh <optional args>"
   echo 'Running without args will build a release build of the library. You can also clean build, debug, or choose a target'
   echo '   Options:'
   echo '          debug: Build with debug and tests enabled'
   echo '          clean: Clean out the build directory'
   echo '          target <target name>: Build a specific target in the test directory'
   exit 1
}

BUILD_TYPE="Release"
BUILD_UNIT_TESTS=false
BUILD_CLEAN=false
BUILD_TARGET_NAME="all"

# Handle arguments
if [[ $# > 0 ]]
then
   for ((i = 1 ; i < $#+1 ; i++)); do
      if [[ ${!i} = "debug" ]]
      then
         BUILD_TYPE="Debug"
         BUILD_UNIT_TESTS=true

      elif [[ ${!i} = "clean" ]]
      then
         BUILD_CLEAN=true

      elif [[ ${!i} = "target" ]]
      then
         if [[ $i+1 > $# ]]
         then
            usage
         else
            i=$(($i+1))
            BUILD_TARGET_NAME="${!i}"
         fi
      else
         usage
      fi
   done
fi

echo "Building: $BUILD_TYPE"
echo "Testing : $BUILD_UNIT_TESTS"
echo "Cleaning: $BUILD_CLEAN"
echo "Target  : $BUILD_TARGET_NAME"

# Clean First if enabled
if $BUILD_CLEAN
then
   echo "Cleaning..."
   cd "$(dirname "$0")"
   rm -rf build
   mkdir build
fi

# Cd into the location of the shell script
cd "$(dirname "$0")/build"
pwd

# Generate our project commands
cmake -G Ninja -DTML_BUILD_UNIT_TESTS=$BUILD_UNIT_TESTS -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..

# Build our target, default all
time ninja $BUILD_TARGET_NAME

# Temporary measure to move test data to the build directory, eventually this should be done with cmake I believe
if $BUILD_UNIT_TESTS
then
   cp -a ../test/data .
   mkdir output
fi

# For LSP support with clangd we need to make sure our indexes are continually kept up to date
cp ./compile_commands.json ../