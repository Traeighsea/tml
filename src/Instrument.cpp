#include "Instrument.hpp"
#include <ostream>
#include <string>

namespace tml {
std::ostream &operator<<(std::ostream &out, const Instrument &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const Instrument &val) {
   std::string out;
   switch (val) {
   // This switch case is brought to you in part by <3 ~ R e g e x ~ <3
   case Instrument::AcousticGrandPiano:
      out = "AcousticGrandPiano";
      break;
   case Instrument::BrightAcousticPiano:
      out = "BrightAcousticPiano";
      break;
   case Instrument::ElectricGrandPiano:
      out = "ElectricGrandPiano";
      break;
   case Instrument::HonkyTonkPiano:
      out = "HonkyTonkPiano";
      break;
   case Instrument::ElectricPiano1:
      out = "ElectricPiano1";
      break;
   case Instrument::ElectricPiano2:
      out = "ElectricPiano2";
      break;
   case Instrument::Harpsichord:
      out = "Harpsichord";
      break;
   case Instrument::Clavinet:
      out = "Clavinet";
      break;
   case Instrument::Celesta:
      out = "Celesta";
      break;
   case Instrument::Glockenspiel:
      out = "Glockenspiel";
      break;
   case Instrument::MusicBox:
      out = "MusicBox";
      break;
   case Instrument::Vibraphone:
      out = "Vibraphone";
      break;
   case Instrument::Marimba:
      out = "Marimba";
      break;
   case Instrument::Xylophone:
      out = "Xylophone";
      break;
   case Instrument::TubularBells:
      out = "TubularBells";
      break;
   case Instrument::Dulcimer:
      out = "Dulcimer";
      break;
   case Instrument::DrawbarOrgan:
      out = "DrawbarOrgan";
      break;
   case Instrument::PercussiveOrgan:
      out = "PercussiveOrgan";
      break;
   case Instrument::RockOrgan:
      out = "RockOrgan";
      break;
   case Instrument::ChurchOrgan:
      out = "ChurchOrgan";
      break;
   case Instrument::ReedOrgan:
      out = "ReedOrgan";
      break;
   case Instrument::Accordion:
      out = "Accordion";
      break;
   case Instrument::Harmonica:
      out = "Harmonica";
      break;
   case Instrument::TangoAccordion:
      out = "TangoAccordion";
      break;
   case Instrument::AcousticGuitarNylon:
      out = "AcousticGuitarNylon";
      break;
   case Instrument::AcousticGuitarSteel:
      out = "AcousticGuitarSteel";
      break;
   case Instrument::ElectricGuitarJazz:
      out = "ElectricGuitarJazz";
      break;
   case Instrument::ElectricGuitarClean:
      out = "ElectricGuitarClean";
      break;
   case Instrument::ElectricGuitarMuted:
      out = "ElectricGuitarMuted";
      break;
   case Instrument::OverdrivenGuitar:
      out = "OverdrivenGuitar";
      break;
   case Instrument::DistortionGuitar:
      out = "DistortionGuitar";
      break;
   case Instrument::Guitarharmonics:
      out = "Guitarharmonics";
      break;
   case Instrument::AcousticBass:
      out = "AcousticBass";
      break;
   case Instrument::ElectricBassFinger:
      out = "ElectricBassFinger";
      break;
   case Instrument::ElectricBassPick:
      out = "ElectricBassPick";
      break;
   case Instrument::FretlessBass:
      out = "FretlessBass";
      break;
   case Instrument::SlapBass1:
      out = "SlapBass1";
      break;
   case Instrument::SlapBass2:
      out = "SlapBass2";
      break;
   case Instrument::SynthBass1:
      out = "SynthBass1";
      break;
   case Instrument::SynthBass2:
      out = "SynthBass2";
      break;
   case Instrument::Violin:
      out = "Violin";
      break;
   case Instrument::Viola:
      out = "Viola";
      break;
   case Instrument::Cello:
      out = "Cello";
      break;
   case Instrument::Contrabass:
      out = "Contrabass";
      break;
   case Instrument::TremoloStrings:
      out = "TremoloStrings";
      break;
   case Instrument::PizzicatoStrings:
      out = "PizzicatoStrings";
      break;
   case Instrument::OrchestralHarp:
      out = "OrchestralHarp";
      break;
   case Instrument::Timpani:
      out = "Timpani";
      break;
   case Instrument::StringEnsemble1:
      out = "StringEnsemble1";
      break;
   case Instrument::StringEnsemble2:
      out = "StringEnsemble2";
      break;
   case Instrument::SynthStrings1:
      out = "SynthStrings1";
      break;
   case Instrument::SynthStrings2:
      out = "SynthStrings2";
      break;
   case Instrument::ChoirAahs:
      out = "ChoirAahs";
      break;
   case Instrument::VoiceOohs:
      out = "VoiceOohs";
      break;
   case Instrument::SynthVoice:
      out = "SynthVoice";
      break;
   case Instrument::OrchestraHit:
      out = "OrchestraHit";
      break;
   case Instrument::Trumpet:
      out = "Trumpet";
      break;
   case Instrument::Trombone:
      out = "Trombone";
      break;
   case Instrument::Tuba:
      out = "Tuba";
      break;
   case Instrument::MutedTrumpet:
      out = "MutedTrumpet";
      break;
   case Instrument::FrenchHorn:
      out = "FrenchHorn";
      break;
   case Instrument::BrassSection:
      out = "BrassSection";
      break;
   case Instrument::SynthBrass1:
      out = "SynthBrass1";
      break;
   case Instrument::SynthBrass2:
      out = "SynthBrass2";
      break;
   case Instrument::SopranoSax:
      out = "SopranoSax";
      break;
   case Instrument::AltoSax:
      out = "AltoSax";
      break;
   case Instrument::TenorSax:
      out = "TenorSax";
      break;
   case Instrument::BaritoneSax:
      out = "BaritoneSax";
      break;
   case Instrument::Oboe:
      out = "Oboe";
      break;
   case Instrument::EnglishHorn:
      out = "EnglishHorn";
      break;
   case Instrument::Bassoon:
      out = "Bassoon";
      break;
   case Instrument::Clarinet:
      out = "Clarinet";
      break;
   case Instrument::Piccolo:
      out = "Piccolo";
      break;
   case Instrument::Flute:
      out = "Flute";
      break;
   case Instrument::Recorder:
      out = "Recorder";
      break;
   case Instrument::PanFlute:
      out = "PanFlute";
      break;
   case Instrument::BlownBottle:
      out = "BlownBottle";
      break;
   case Instrument::Shakuhachi:
      out = "Shakuhachi";
      break;
   case Instrument::Whistle:
      out = "Whistle";
      break;
   case Instrument::Ocarina:
      out = "Ocarina";
      break;
   case Instrument::Lead1Square:
      out = "Lead1Square";
      break;
   case Instrument::Lead2Sawtooth:
      out = "Lead2Sawtooth";
      break;
   case Instrument::Lead3Calliope:
      out = "Lead3Calliope";
      break;
   case Instrument::Lead4Chiff:
      out = "Lead4Chiff";
      break;
   case Instrument::Lead5Charang:
      out = "Lead5Charang";
      break;
   case Instrument::Lead6Voice:
      out = "Lead6Voice";
      break;
   case Instrument::Lead7Fifths:
      out = "Lead7Fifths";
      break;
   case Instrument::Lead8BassAndLead:
      out = "Lead8BassAndLead";
      break;
   case Instrument::Pad1NewAge:
      out = "Pad1NewAge";
      break;
   case Instrument::Pad2Warm:
      out = "Pad2Warm";
      break;
   case Instrument::Pad3Polysynth:
      out = "Pad3Polysynth";
      break;
   case Instrument::Pad4Choir:
      out = "Pad4Choir";
      break;
   case Instrument::Pad5Bowed:
      out = "Pad5Bowed";
      break;
   case Instrument::Pad6Metallic:
      out = "Pad6Metallic";
      break;
   case Instrument::Pad7Halo:
      out = "Pad7Halo";
      break;
   case Instrument::Pad8Wweep:
      out = "Pad8Wweep";
      break;
   case Instrument::FX1Rain:
      out = "FX1Rain";
      break;
   case Instrument::FX2Soundtrack:
      out = "FX2Soundtrack";
      break;
   case Instrument::FX3Crystal:
      out = "FX3Crystal";
      break;
   case Instrument::FX4Atmosphere:
      out = "FX4Atmosphere";
      break;
   case Instrument::FX5Brightness:
      out = "FX5Brightness";
      break;
   case Instrument::FX6Goblins:
      out = "FX6Goblins";
      break;
   case Instrument::FX7Echoes:
      out = "FX7Echoes";
      break;
   case Instrument::FX8SciFi:
      out = "FX8SciFi";
      break;
   case Instrument::Sitar:
      out = "Sitar";
      break;
   case Instrument::Banjo:
      out = "Banjo";
      break;
   case Instrument::Shamisen:
      out = "Shamisen";
      break;
   case Instrument::Koto:
      out = "Koto";
      break;
   case Instrument::Kalimba:
      out = "Kalimba";
      break;
   case Instrument::Bagpipe:
      out = "Bagpipe";
      break;
   case Instrument::Fiddle:
      out = "Fiddle";
      break;
   case Instrument::Shanai:
      out = "Shanai";
      break;
   case Instrument::TinkleBell:
      out = "TinkleBell";
      break;
   case Instrument::Agogo:
      out = "Agogo";
      break;
   case Instrument::SteelDrums:
      out = "SteelDrums";
      break;
   case Instrument::Woodblock:
      out = "Woodblock";
      break;
   case Instrument::TaikoDrum:
      out = "TaikoDrum";
      break;
   case Instrument::MelodicTom:
      out = "MelodicTom";
      break;
   case Instrument::SynthDrum:
      out = "SynthDrum";
      break;
   case Instrument::ReverseCymbal:
      out = "ReverseCymbal";
      break;
   case Instrument::GuitarFretNoise:
      out = "GuitarFretNoise";
      break;
   case Instrument::BreathNoise:
      out = "BreathNoise";
      break;
   case Instrument::Seashore:
      out = "Seashore";
      break;
   case Instrument::BirdTweet:
      out = "BirdTweet";
      break;
   case Instrument::TelephoneRing:
      out = "TelephoneRing";
      break;
   case Instrument::Helicopter:
      out = "Helicopter";
      break;
   case Instrument::Applause:
      out = "Applause";
      break;
   case Instrument::Gunshot:
      out = "Gunshot";
      break;
   case Instrument::None:
      out = "None";
      break;
   default:
      break;
   }

   return out;
}

}