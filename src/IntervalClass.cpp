#include "IntervalClass.hpp"
#include "Interval.hpp"
#include "MusicTheoryCommon.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchClass.hpp"
#include "PitchName.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {

bool IntervalClass::operator==(const IntervalClass &rhs) const {
   return ic_ == rhs.Get();
}

std::ostream &operator<<(std::ostream &out, const IntervalClass &val) {
   out << val.ToString();
   return out;
}

std::string IntervalClass::ToString() const {
   std::string out;
   out = ic_;

   return out;
}

IntervalClass::IntervalClass(unsigned int ic) : ic_(CircularIndex(7, ic)) {
}

IntervalClass::IntervalClass(int ic) : ic_(CircularIndex(7, ic)) {
}

IntervalClass::IntervalClass(const Interval &interval) : ic_(interval.GetIntervalClass().Get()) {
}

IntervalClass::IntervalClass(const PitchClass &from, const PitchClass &to)
      : ic_(CircularIndex(12, from.Get() - to.Get())) {
}

IntervalClass::IntervalClass(const PitchName &from, const PitchName &to)
      : ic_(CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get())) {
}

IntervalClass::IntervalClass(const Pitch &from, const Pitch &to)
      : ic_(CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get())) {
}

IntervalClass::IntervalClass(const Note &from, const Note &to)
      : ic_(CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get())) {
}

int IntervalClass::ToInt() const {
   return (int)ic_;
}

unsigned int IntervalClass::ToUnsignedInt() const {
   return ic_;
}

unsigned int IntervalClass::Get() const {
   return ic_;
}

void IntervalClass::Set(unsigned int ic) {
   ic_ = CircularIndex(7, ic);
}

void IntervalClass::Set(int ic) {
   ic_ = CircularIndex(7, ic);
}

void IntervalClass::Set(const Interval &interval) {
   ic_ = interval.GetIntervalClass().Get();
}

void IntervalClass::Set(const PitchClass &from, const PitchClass &to) {
   // This will inherently never be more than 6
   ic_ = CircularIndex(12, from.Get() - to.Get());
}

void IntervalClass::Set(const PitchName &from, const PitchName &to) {
   ic_ = CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get());
}

void IntervalClass::Set(const Pitch &from, const Pitch &to) {
   ic_ = CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get());
}

void IntervalClass::Set(const Note &from, const Note &to) {
   ic_ = CircularIndex(12, from.GetPitchClass().Get() - to.GetPitchClass().Get());
}

void to_json(nlohmann::json &j, const IntervalClass &val) {
   j = nlohmann::json{{"ic", val.ic_}};
}

void from_json(const nlohmann::json &j, IntervalClass &val) {
   j.at("ic").get_to(val.ic_);
}

}
