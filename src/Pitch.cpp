#include "Pitch.hpp"
#include "Note.hpp"
#include "PitchClass.hpp"
#include "PitchName.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace tml {
Pitch::Pitch(const PitchName &other) : pitch_name_(other), octave_(4) {
}

Pitch::Pitch(PitchName &&other) : pitch_name_(other), octave_(4) {
}

Pitch &Pitch::operator=(const PitchName &rhs) {
   pitch_name_ = rhs;
   return *this;
}

Pitch &Pitch::operator=(PitchName &&rhs) {
   pitch_name_ = rhs;
   return *this;
}

Pitch::Pitch(const Note &other) : pitch_name_(other.GetPitchName()), octave_(other.GetOctave()) {
}

Pitch::Pitch(Note &&other) : pitch_name_(other.GetPitchName()), octave_(other.GetOctave()) {
}

Pitch &Pitch::operator=(const Note &rhs) {
   pitch_name_ = rhs;
   return *this;
}

Pitch &Pitch::operator=(Note &&rhs) {
   pitch_name_ = rhs;
   return *this;
}

Pitch::Pitch(PitchLetter letter, Accidental accidental, Octave octave)
      : pitch_name_(letter, accidental), octave_(octave) {
}

Pitch::Pitch(PitchName pitch_name, Octave octave) : pitch_name_(pitch_name), octave_(octave) {
}

bool Pitch::operator==(const Pitch &rhs) const {
   return pitch_name_ == rhs.GetPitchName() && octave_ == rhs.GetOctave();
}

std::ostream &operator<<(std::ostream &out, const Pitch &val) {
   out << val.ToString();
   return out;
}

std::string Pitch::ToString() const {
   std::string out;
   out = pitch_name_.ToString() + std::to_string(octave_);
   return out;
}

int Pitch::ToInt() const {
   return pitch_name_.ToInt() + octave_ * 12;
}

PitchClass Pitch::GetPitchClass() const {
   return pitch_name_.GetPitchClass();
}

PitchName Pitch::GetPitchName() const {
   return pitch_name_;
}

Octave Pitch::GetOctave() const {
   return octave_;
}

void Pitch::SetPitchName(const PitchName &pitch_name) {
   pitch_name_ = pitch_name;
}

void Pitch::SetOctave(const Octave &octave) {
   if (octave > 9) {
      octave_ = 9;
      std::cerr << "Error in Pitch::SetOctave(const Octave& o): Octave exceeds max value: 9.\n";
   } else if (octave < -1) {
      octave_ = -1;
      std::cerr << "Error in Pitch::SetOctave(const Octave& o): Octave exceeds min value: -1.\n";
   } else {
      octave_ = octave;
   }
}

PitchLetter Pitch::GetPitchLetter() const {
   return pitch_name_.GetPitchLetter();
}

Accidental Pitch::GetAccidental() const {
   return pitch_name_.GetAccidental();
}

void Pitch::SetPitchLetter(const PitchLetter &letter) {
   pitch_name_.SetPitchLetter(letter);
}

void Pitch::SetAccidental(const Accidental &accidental) {
   pitch_name_.SetAccidental(accidental);
}

void Pitch::AddAccidental(const Accidental &accidental) {
   pitch_name_.AddAccidental(accidental);
}

void Pitch::Flatten() {
   pitch_name_.Flatten();
}

void Pitch::Sharpen() {
   pitch_name_.Sharpen();
}

void to_json(nlohmann::json &j, const Pitch &val) {
   j = nlohmann::json{{"pitch_name", val.pitch_name_}, {"octave", val.octave_}};
}

void from_json(const nlohmann::json &j, Pitch &val) {
   j.at("pitch_name").get_to(val.pitch_name_);
   j.at("octave").get_to(val.octave_);
}

}
