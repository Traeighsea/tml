#include "Dynamic.hpp"
#include "Configs.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
Dynamic::Dynamic(const DynamicMarking &dynamic_marking) : dynamic_marking_(dynamic_marking) {
}

bool Dynamic::operator==(const Dynamic &rhs) const {
   return dynamic_marking_ == rhs.GetDynamicMarking();
}

std::ostream &operator<<(std::ostream &out, const DynamicMarking &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const DynamicMarking &val) {
   std::string out;
   if (cfg::USE_SHORTENED_DYNAMIC_NAMES) {
      switch (val) {
      case DynamicMarking::fff:
         out = "fff";
         break;
      case DynamicMarking::ff:
         out = "ff";
         break;
      case DynamicMarking::f:
         out = "f";
         break;
      case DynamicMarking::mf:
         out = "mf";
         break;
      case DynamicMarking::m:
         if (cfg::USE_OUTPUT_MEZZO)
            out = "m";
         break;
      case DynamicMarking::mp:
         out = "mp";
         break;
      case DynamicMarking::p:
         out = "p";
         break;
      case DynamicMarking::pp:
         out = "pp";
         break;
      case DynamicMarking::ppp:
         out = "ppp";
         break;
      default:
         break;
      }
   } else {
      switch (val) {
      case DynamicMarking::Fortississimo:
         out = "Fortississimo";
         break;
      case DynamicMarking::Fortissimo:
         out = "Fortissimo";
         break;
      case DynamicMarking::Forte:
         out = "Forte";
         break;
      case DynamicMarking::MezzoForte:
         out = "MezzoForte";
         break;
      case DynamicMarking::Mezzo:
         if (cfg::USE_OUTPUT_MEZZO)
            out = "Mezzo";
         break;
      case DynamicMarking::MezzoPiano:
         out = "MezzoPiano";
         break;
      case DynamicMarking::Piano:
         out = "Piano";
         break;
      case DynamicMarking::Pianissimo:
         out = "Pianissimo";
         break;
      case DynamicMarking::Pianississimo:
         out = "Pianississimo";
         break;
      default:
         break;
      }
   }

   return out;
}

std::ostream &operator<<(std::ostream &out, const Dynamic &val) {
   out << val.ToString();
   return out;
}

std::string Dynamic::ToString() const {
   std::string out;
   out = ::tml::ToString(dynamic_marking_);
   return out;
}

void Dynamic::SetDynamicMarking(const DynamicMarking &dynamic_marking) {
   dynamic_marking_ = dynamic_marking;
}

DynamicMarking Dynamic::GetDynamicMarking() const {
   return dynamic_marking_;
}

void to_json(nlohmann::json &j, const Dynamic &val) {
   j = nlohmann::json{{"dynamic_marking", val.dynamic_marking_}};
}

void from_json(const nlohmann::json &j, Dynamic &val) {
   j.at("dynamic_marking").get_to(val.dynamic_marking_);
}

}
