#include "Motif.hpp"
#include "MusicTheoryCommon.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
bool Motif::operator==(const Motif &rhs) const {
   return name_ == rhs.GetName() && notes_ == rhs.GetNotes();
}

std::ostream &operator<<(std::ostream &out, const Motif &val) {
   out << val.ToString();
   return out;
}

std::string Motif::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out += name_ + ": ";
   }

   out += "{";
   for (std::vector<std::pair<NoteOnset, Note>>::const_iterator it = notes_.begin();
        it != notes_.end(); it++) {
      out += "{" + it->first.ToString() + ": " + it->second.ToString() + "}";
      if (it + 1 != notes_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

std::string Motif::GetName() const {
   return name_;
}

const std::vector<std::pair<NoteOnset, Note>> &Motif::GetNotes() const {
   return notes_;
}

std::vector<std::pair<NoteOnset, Note>> &Motif::GetNotes() {
   return notes_;
}

void Motif::SetName(std::string name) {
   name_ = name;
}

void Motif::Add(NoteOnset start, Note new_note) {
   if (notes_.size() == 0) {
      notes_.emplace_back(std::make_pair(start, new_note));
   } else {
      std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

      while (start > it->first && it != notes_.end()) {
         it++;
      }
      if (it == notes_.end()) {
         if (start > it->first) {
            notes_.emplace_back(std::make_pair(start, new_note));
         } else {
            notes_.insert(it, std::make_pair(start, new_note));
         }
      } else {
         notes_.insert(it, std::make_pair(start, new_note));
      }
   }
}

void Motif::Remove(int index, bool circular_buffer_indexing) {
   if (notes_.empty()) {
      std::cerr
            << "Motif::Remove(int index, bool circular_buffer_indexing): Zero notes in container."
            << std::endl;
      return;
   }

   std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Motif::Remove(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   notes_.erase(it);
}

void Motif::Clear() {
   notes_.clear();
}

std::pair<NoteOnset, Note> Motif::GetNote(int index, bool circular_buffer_indexing) const {
   if (notes_.empty()) {
      std::cerr
            << "Motif::GetNote(int index, bool circular_buffer_indexing): Zero notes in container."
            << std::endl;
      return std::make_pair(NoteOnset(), Note());
   }

   std::vector<std::pair<NoteOnset, Note>>::const_iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Motif::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return std::make_pair(NoteOnset(), Note());
      } else {
         it += index;
      }
   }
   return *it;
}

std::pair<NoteOnset, Note> &Motif::GetNote(int index, bool circular_buffer_indexing) {
   if (notes_.empty()) {
      std::cerr
            << "Motif::GetNote(int index, bool circular_buffer_indexing): Zero notes in container."
            << std::endl;
      // This should throw
      return notes_[index];
   }

   std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Motif::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         // This should throw
         return notes_[index];
      } else {
         it += index;
      }
   }
   return *it;
}

int Motif::GetSize() const {
   return notes_.size();
}

void to_json(nlohmann::json &j, const Motif &val) {
   j = nlohmann::json{{"name", val.name_}, {"notes", val.notes_}};
}

void from_json(const nlohmann::json &j, Motif &val) {
   j.at("name").get_to(val.name_);
   j.at("notes").get_to(val.notes_);
}

}