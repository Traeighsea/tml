#include "ScaleFormula.hpp"
#include "Interval.hpp"
#include "KeySignature.hpp"
#include "PitchName.hpp"
#include "Scale.hpp"
#include "ScaleDegree.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
ScaleFormula::ScaleFormula(const std::string &name, const std::vector<ScaleDegree> &formula)
      : name_(name), formula_(formula) {
}

bool ScaleFormula::operator==(const ScaleFormula &rhs) const {
   if (formula_.size() != rhs.formula_.size())
      return false;

   for (int i = 0; i < formula_.size(); i++) {
      if (!(formula_[i] == rhs.formula_[i])) {
         return false;
      }
   }
   return true;
}

std::ostream &operator<<(std::ostream &out, const ScaleFormula &val) {
   out << val.ToString();
   return out;
}

std::string ScaleFormula::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out += name_ + ": ";
   }
   for (int i = 0; i < formula_.size(); i++) {
      out += formula_[i].ToString();
      if (i != formula_.size() - 1)
         out += " ";
   }
   return out;
}

std::string ScaleFormula::GetName() const {
   return name_;
}

std::vector<ScaleDegree> ScaleFormula::GetFormula() {
   return formula_;
}

void ScaleFormula::SetName(const std::string &name) {
   name_ = name;
}

void ScaleFormula::AddScaleDegree(const ScaleDegree &scale_degree) {
   auto pos = std::find_if(formula_.begin(), formula_.end(), [scale_degree](auto pos) {
      return pos < scale_degree;
   });
   formula_.insert(pos, scale_degree);
}

void ScaleFormula::RemoveScaleDegree(int index) {
   if (index > formula_.size() - 1 || index < 0) {
      std::cerr << "ScaleFormula::RemoveScaleDegree(int index): Out of bounds." << std::endl;
      return;
   }
   // TODO fix this travesty..
   std::vector<ScaleDegree>::const_iterator it = formula_.begin();
   it += index;
   formula_.erase(it);
}

void ScaleFormula::Clear() {
   name_.clear();
   formula_.clear();
}

int ScaleFormula::GetSize() const {
   return formula_.size();
}

// @TODO: Fix
std::vector<Interval> ScaleFormula::GetIntervalDistances() {
   std::vector<PitchName> scale;
   std::vector<Interval> interval_distances;

   // Start with the C Major scale for reference
   const std::vector<PitchLetter> c_major = {PitchLetter::C, PitchLetter::D, PitchLetter::E,
                                             PitchLetter::F, PitchLetter::G, PitchLetter::A,
                                             PitchLetter::B};

   // Apply the formula to the scale for reference
   for (int i = 0; i < formula_.size(); i++)
      scale.emplace_back(
            PitchName(c_major[formula_[i].GetDegree() - 1], formula_[i].GetAccidental()));

   // Calculate the intervals between the notes in the scale
   // @TODO: Update intervals to a new system that isn't as enum heavy, and sepearates number and
   // quality
   for (int i = 0; i < scale.size(); i++) {
      interval_distances.emplace_back(Interval(scale[i % scale.size()].ToInt() - scale[i].ToInt()));
   }

   return interval_distances;
}

Scale ScaleFormula::ApplyScale(const PitchName &root) {
   // Get the major scale for the root from the root Key
   KeySignature key;
   key.Set(root);
   std::array<PitchName, 7> major_scale = key.PitchNames();

   // Apply the formula to the scale for reference
   Scale scale;
   Pitch pitch;
   int o = 4; // Defaulting to 4
   for (int i = 0; i < formula_.size(); i++) {
      pitch = major_scale[(formula_[i].GetDegree() - 1) % 7];
      pitch.AddAccidental(formula_[i].GetAccidental());

      if (scale.GetSize() >= 1) {
         Pitch last = scale.GetPitch(scale.GetSize() - 1);
         if ((static_cast<int>(pitch.GetPitchLetter()) + (o * 12)) <
             (static_cast<int>(last.GetPitchLetter()) + (last.GetOctave() * 12)))
            o++;
      }

      pitch.SetOctave(o);

      scale.AddPitch(pitch);
   }

   scale.SetName(name_);
   return scale;
}

void to_json(nlohmann::json &j, const ScaleFormula &val) {
   j = nlohmann::json{{"name", val.name_}, {"formula", val.formula_}};
}

void from_json(const nlohmann::json &j, ScaleFormula &val) {
   j.at("name").get_to(val.name_);
   j.at("formula").get_to(val.formula_);
}

}