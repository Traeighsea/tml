#include "Tempo.hpp"
#include <ostream>
#include <string>

namespace tml {
std::ostream &operator<<(std::ostream &out, const Tempo &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const Tempo &val) {
   std::string out;
   switch (val) {
   case Tempo::Larghissimo:
      out = "Larghissimo";
      break;
   case Tempo::Adagissimo:
      out = "Adagissimo";
      break;
   case Tempo::Grave:
      out = "Grave";
      break;
   case Tempo::Largo:
      out = "Largo";
      break;
   case Tempo::Lento:
      out = "Lento";
      break;
   case Tempo::Larghetto:
      out = "Larghetto";
      break;
   case Tempo::Adagio:
      out = "Adagio";
      break;
   case Tempo::Adagietto:
      out = "Adagietto";
      break;
   case Tempo::Andante:
      out = "Andante";
      break;
   case Tempo::Andantino:
      out = "Andantino";
      break;
   case Tempo::MarciaModerato:
      out = "MarciaModerato";
      break;
   case Tempo::AndanteModerato:
      out = "AndanteModerato";
      break;
   case Tempo::Moderato:
      out = "Moderato";
      break;
   case Tempo::Allegretto:
      out = "Allegretto";
      break;
   case Tempo::AllegroModerato:
      out = "AllegroModerato";
      break;
   case Tempo::Allegro:
      out = "Allegro";
      break;
   case Tempo::MoltoAllegro:
      out = "MoltoAllegro";
      break;
   case Tempo::Vivace:
      out = "Vivace";
      break;
   case Tempo::Vivacissimo:
      out = "Vivacissimo";
      break;
   case Tempo::Allegrissimo:
      out = "Allegrissimo";
      break;
   case Tempo::Presto:
      out = "Presto";
      break;
   case Tempo::Prestissimo:
      out = "Prestissimo";
      break;
   case Tempo::Prestississimo:
      out = "Prestississimo";
      break;
   // This is a real tempo, I promise. You'll just have to believe me.
   case Tempo::AAAAAAAA:
      out = "AAAAAAAA";
      break;
   default:
      break;
   }
   return out;
}
}