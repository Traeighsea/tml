#include "Scale.hpp"
#include "Pitch.hpp"
#include "PitchName.hpp"
#include "ScaleDegree.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
bool Scale::operator==(const Scale &rhs) const {
   return name_ == rhs.GetName() && pitch_sequence_ == rhs.GetPitches();
}

std::ostream &operator<<(std::ostream &out, const Scale &val) {
   out << val.ToString();
   return out;
}

std::string Scale::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out += pitch_sequence_.front().ToString() + " " + name_ + ": ";
   }
   for (int i = 0; i < pitch_sequence_.size(); i++) {
      out += pitch_sequence_[i].ToString();
      if (i != pitch_sequence_.size() - 1)
         out += " ";
   }
   return out;
}

std::string Scale::GetName() const {
   return name_;
}

std::vector<Pitch> Scale::GetPitches() const {
   return pitch_sequence_;
}

void Scale::SetName(const std::string &name) {
   name_ = name;
}

/*
void Scale::SetFromScaleName(const PitchName& root, const ScaleName& scale_name) {

}

void Scale::SetFromScaleName(const Pitch& root, const ScaleName& scale_name) {

}

void Scale::SetFromScaleName(const Note& root, const ScaleName& scale_name) {

}
*/

bool Scale::IsDiatonic() const {
   if (pitch_sequence_.size() != 7) {
      return false;
   }

   // TODO: Update how intervals are handled here
   int diatonic_intervals[7] = {2, 2, 1, 2, 2, 2, 1};

   for (int i = 0; i < pitch_sequence_.size(); i++) {
      if (i == pitch_sequence_.size() - 1) {
         if (((pitch_sequence_[i].GetPitchName().ToInt() + diatonic_intervals[i]) % 12) !=
             pitch_sequence_[0].GetPitchName().ToInt()) {
            return false;
         }
      } else if (((pitch_sequence_[i].GetPitchName().ToInt() + diatonic_intervals[i]) % 12) !=
                 pitch_sequence_[i + 1].GetPitchName().ToInt()) {
         return false;
      }
   }
   return true;
}

Pitch Scale::GetRoot() const {
   return (!pitch_sequence_.empty()) ? pitch_sequence_.front() : Pitch();
}

PitchName Scale::GetPitchName(int index) const {
   if (pitch_sequence_.size() == 0) {
      std::cerr << "Scale::GetPitchName(int index): Out of bounds." << std::endl;
   }
   if (index < 0) {
      return pitch_sequence_[(index + pitch_sequence_.size()) % pitch_sequence_.size()]
            .GetPitchName();
   } else {
      return pitch_sequence_[index % pitch_sequence_.size()].GetPitchName();
   }
}

Pitch Scale::GetPitch(int index) const {
   if (pitch_sequence_.size() == 0) {
      std::cerr << "Scale::GetPitch(int index): Out of bounds." << std::endl;
   }
   if (index < 0) {
      return pitch_sequence_[(index + pitch_sequence_.size()) % pitch_sequence_.size()];
   } else {
      return pitch_sequence_[index % pitch_sequence_.size()];
   }
}

void Scale::AddPitch(const Pitch &pitch) {
   pitch_sequence_.push_back(pitch);
}

void Scale::RemovePitch(int index) {
   if (index > pitch_sequence_.size() - 1 || index < 0) {
      std::cerr << "Scale::RemovePitch(int index): Out of bounds." << std::endl;
      return;
   }
   // TODO fix this travesty..
   std::vector<Pitch>::const_iterator it = pitch_sequence_.begin();
   it += index;
   pitch_sequence_.erase(it);
}

void Scale::Clear() {
   name_.clear();
   pitch_sequence_.clear();
}

int Scale::GetSize() const {
   return pitch_sequence_.size();
}

void to_json(nlohmann::json &j, const Scale &val) {
   j = nlohmann::json{{"name", val.name_}, {"pitch_sequence", val.pitch_sequence_}};
}

void from_json(const nlohmann::json &j, Scale &val) {
   j.at("name").get_to(val.name_);
   j.at("pitch_sequence").get_to(val.pitch_sequence_);
}

}
