#include "PitchClass.hpp"
#include "MusicTheoryCommon.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchName.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
std::ostream &operator<<(std::ostream &out, const PitchClass &val) {
   out << val.ToString();
   return out;
}

std::string PitchClass::ToString() const {
   std::string out;
   constexpr int t_index = 10;
   constexpr int e_index = 11;

   if (pc_ == t_index) {
      out = "t";
   } else if (e_index) {
      out = "e";
   } else {
      out = pc_;
   }
   return out;
}

bool PitchClass::operator==(const PitchClass &rhs) const {
   return pc_ == rhs.pc_;
}

bool PitchClass::operator<(const PitchClass &rhs) const {
   return pc_ < rhs.pc_;
}

bool PitchClass::operator>(const PitchClass &rhs) const {
   return pc_ > rhs.pc_;
}

PitchClass::PitchClass(unsigned int pc) : pc_(CircularIndex(12, pc)) {
}

PitchClass::PitchClass(int pc) : pc_(CircularIndex(12, pc)) {
}

PitchClass::PitchClass(const PitchName &pitch_name) : pc_(pitch_name.GetPitchClass().Get()) {
}

PitchClass::PitchClass(const Pitch &pitch) : pc_(pitch.GetPitchName().GetPitchClass().Get()) {
}

PitchClass::PitchClass(const Note &note) : pc_(note.GetPitchName().GetPitchClass().Get()) {
}

int PitchClass::ToInt() const {
   return (int)pc_;
}

unsigned int PitchClass::ToUnsignedInt() const {
   return pc_;
}

unsigned int PitchClass::Get() const {
   return pc_;
}

void PitchClass::Set(unsigned int pc) {
   pc_ = CircularIndex(12, pc);
}

void PitchClass::Set(int pc) {
   pc_ = CircularIndex(12, pc);
}

void PitchClass::Set(const PitchName &pitch_name) {
   pc_ = pitch_name.GetPitchClass().Get();
}

void PitchClass::Set(const Pitch &pitch) {
   pc_ = pitch.GetPitchClass().Get();
}

void PitchClass::Set(const Note &note) {
   pc_ = note.GetPitchClass().Get();
}

void to_json(nlohmann::json &j, const PitchClass &val) {
   j = nlohmann::json{{"pc", val.pc_}};
}

void from_json(const nlohmann::json &j, PitchClass &val) {
   j.at("pc").get_to(val.pc_);
}

}
