#include "Voice.hpp"
#include <nlohmann/json.hpp>
#include <ostream>

namespace tml {
Voice::Voice(std::string part_name, Instrument instrument)
      : name_(part_name), instrument_(instrument) {
}

bool Voice::operator==(const Voice &rhs) const {
   return name_ == rhs.GetName() && instrument_ == rhs.GetInstrument();
}

std::ostream &operator<<(std::ostream &out, const Voice &val) {
   out << val.ToString();
   return out;
}

std::string Voice::ToString() const {
   std::string out;
   if (GetName() != "") {
      out = GetName() + ": " + ::tml::ToString(GetInstrument());
   } else {
      out = ::tml::ToString(GetInstrument());
   }
   return out;
}

std::string Voice::GetName() const {
   return name_;
}

Instrument Voice::GetInstrument() const {
   return instrument_;
}

void Voice::SetName(std::string name) {
   name_ = name;
}

void Voice::SetInstrument(Instrument instrument) {
   instrument_ = instrument;
}

void to_json(nlohmann::json &j, const Voice &val) {
   j = nlohmann::json{{"name", val.name_}, {"instrument", val.instrument_}};
}

void from_json(const nlohmann::json &j, Voice &val) {
   j.at("name").get_to(val.name_);
   j.at("instrument").get_to(val.instrument_);
}

}