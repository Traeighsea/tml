#include "NoteOnset.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
bool NoteOnset::operator==(const NoteOnset &rhs) const {
   if (beat_ != rhs.beat_) {
      return false;
   }

   if (has_duration_offset_ != rhs.has_duration_offset_) {
      return false;
   }

   if (!has_duration_offset_) {
      return true;
   } else {
      return duration_offset_ == rhs.duration_offset_;
   }
}

bool NoteOnset::operator>(const NoteOnset &rhs) const {
   if (beat_ < rhs.beat_) {
      return false;
   } else if (beat_ == rhs.beat_) {
      if (!has_duration_offset_) {
         return false;
      } else if (rhs.has_duration_offset_) {
         return duration_offset_ > rhs.duration_offset_;
      } else {
         return true;
      }
   } else {
      return true;
   }
}

bool NoteOnset::operator<(const NoteOnset &rhs) const {
   if (beat_ > rhs.beat_) {
      return false;
   } else if (beat_ == rhs.beat_) {
      if (has_duration_offset_) {
         return false;
      } else if (!rhs.has_duration_offset_) {
         return duration_offset_ < rhs.duration_offset_;
      } else {
         return true;
      }
   } else {
      return true;
   }
}

NoteOnset::NoteOnset(int beat) : beat_(beat) {
}
NoteOnset::NoteOnset(int beat, Duration duration_offset)
      : beat_(beat), duration_offset_(duration_offset), has_duration_offset_(true) {
}

/// TODO: Turn this into a fractional output ie: 1 + 1/4
std::ostream &operator<<(std::ostream &out, const NoteOnset &val) {
   out << val.ToString();
   return out;
}

std::string NoteOnset::ToString() const {
   std::string out;
   out = beat_;
   if (has_duration_offset_) {
      out += " + " + duration_offset_.ToString();
   }
   return out;
}

int NoteOnset::GetBeat() const {
   return beat_;
}

bool NoteOnset::HasDurationOfset() const {
   return has_duration_offset_;
}

Duration NoteOnset::GetDurationOffset() const {
   return duration_offset_;
}

void NoteOnset::SetBeat(int beat) {
   beat_ = beat;
}

void NoteOnset::SetHasDurationOffset(bool has_duration_offset) {
   has_duration_offset_ = has_duration_offset;
}

void NoteOnset::SetDurationOffset(Duration duration_offset) {
   has_duration_offset_ = true;
   duration_offset_ = duration_offset;
}

void to_json(nlohmann::json &j, const NoteOnset &val) {
   j = nlohmann::json{{"beat", val.beat_},
                      {"has_duration_offset", val.has_duration_offset_},
                      {"duration_offset", val.duration_offset_}};
}

void from_json(const nlohmann::json &j, NoteOnset &val) {
   j.at("beat").get_to(val.beat_);
   j.at("has_duration_offset").get_to(val.has_duration_offset_);
   j.at("duration_offset").get_to(val.duration_offset_);
}

}