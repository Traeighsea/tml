#include "PitchName.hpp"
#include "Configs.hpp"
#include "MusicTheoryCommon.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchClass.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
PitchName::PitchName(const Pitch &other)
      : letter_(other.GetPitchLetter()), accidental_(other.GetAccidental()) {
}

PitchName::PitchName(Pitch &&other)
      : letter_(other.GetPitchLetter()), accidental_(other.GetAccidental()) {
}

PitchName &PitchName::operator=(const Pitch &rhs) {
   letter_ = rhs.GetPitchLetter();
   accidental_ = rhs.GetAccidental();
   return *this;
}

PitchName &PitchName::operator=(Pitch &&rhs) {
   letter_ = rhs.GetPitchLetter();
   accidental_ = rhs.GetAccidental();
   return *this;
}

PitchName::PitchName(const Note &other)
      : letter_(other.GetPitchLetter()), accidental_(other.GetAccidental()) {
}

PitchName::PitchName(Note &&other)
      : letter_(other.GetPitchLetter()), accidental_(other.GetAccidental()) {
}

PitchName &PitchName::operator=(const Note &rhs) {
   letter_ = rhs.GetPitchLetter();
   accidental_ = rhs.GetAccidental();
   return *this;
}

PitchName &PitchName::operator=(Note &&rhs) {
   letter_ = rhs.GetPitchLetter();
   accidental_ = rhs.GetAccidental();
   return *this;
}

PitchName::PitchName(PitchLetter letter, Accidental accidental)
      : letter_(letter), accidental_(accidental) {
}

bool PitchName::operator==(const PitchName &rhs) const {
   return letter_ == rhs.GetPitchLetter() && accidental_ == rhs.GetAccidental();
}

std::ostream &operator<<(std::ostream &out, const PitchLetter &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const PitchLetter &val) {
   std::string out;
   switch (val) {
   case PitchLetter::C:
      out = "C";
      break;
   case PitchLetter::D:
      out = "D";
      break;
   case PitchLetter::E:
      out = "E";
      break;
   case PitchLetter::F:
      out = "F";
      break;
   case PitchLetter::G:
      out = "G";
      break;
   case PitchLetter::A:
      out = "A";
      break;
   case PitchLetter::B:
      out = "B";
      break;
   default:
      // TODO: Output Error or Warning
      break;
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const Accidental &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const Accidental &val) {
   std::string out;
   if (cfg::USE_UNICODE_ACCIDENTALS) {
      switch (val) {
      case Accidental::DoubleFlat:
         out = "𝄫";
         break;
      case Accidental::Flat:
         out = "♭";
         break;
      case Accidental::Natural:
         if (cfg::USE_NATURALS_ALWAYS)
            out = "♮";
         break;
      case Accidental::Sharp:
         out = "♯";
         break;
      case Accidental::DoubleSharp:
         out = "𝄪";
         break;
      default:
         if (static_cast<int>(val) > 0) {
            out = "(" + std::to_string(static_cast<int>(val)) + "x♯)";
         } else {
            out = "(" + std::to_string(static_cast<int>(val) * -1) + "x♭)";
         }
         break;
      }
   } else {
      switch (val) {
      case Accidental::DoubleFlat:
         out = "bb";
         break;
      case Accidental::Flat:
         out = "b";
         break;
      case Accidental::Natural:
         out = "";
         break;
      case Accidental::Sharp:
         out = "#";
         break;
      case Accidental::DoubleSharp:
         out = "##";
         break;
      default:
         if (static_cast<int>(val) > 0) {
            out = "(" + std::to_string(static_cast<int>(val)) + "x#)";
         } else {
            out = "(" + std::to_string(static_cast<int>(val) * -1) + "xb)";
         }
         break;
      }
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const PitchName &val) {
   return out << val.ToString();
}

std::string PitchName::ToString() const {
   return ::tml::ToString(letter_) + ::tml::ToString(accidental_);
}

int PitchName::ToInt() const {
   return CircularIndex(12, (static_cast<int>(letter_) + static_cast<int>(accidental_)));
}

PitchClass PitchName::GetPitchClass() const {
   return PitchClass(
         CircularIndex(12, (static_cast<int>(letter_) + static_cast<int>(accidental_))));
}

PitchLetter PitchName::GetPitchLetter() const {
   return letter_;
}

Accidental PitchName::GetAccidental() const {
   return accidental_;
}

void PitchName::SetPitchLetter(const PitchLetter &l) {
   letter_ = l;
}

void PitchName::SetAccidental(const Accidental &a) {
   accidental_ = a;
}

void PitchName::AddAccidental(const Accidental &accidental) {
   accidental_ =
         static_cast<Accidental>(static_cast<int>(accidental_) + static_cast<int>(accidental));
}

void PitchName::Flatten() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) - 1);
}

void PitchName::Sharpen() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) + 1);
}

void to_json(nlohmann::json &j, const PitchName &val) {
   j = nlohmann::json{{"letter", val.letter_}, {"accidental", val.accidental_}};
}

void from_json(const nlohmann::json &j, PitchName &val) {
   j.at("letter").get_to(val.letter_);
   j.at("accidental").get_to(val.accidental_);
}

}
