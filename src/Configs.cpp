#include "Configs.hpp"

namespace tml {

namespace cfg {

bool USE_UNICODE_ACCIDENTALS = true;
bool USE_NATURALS_ALWAYS = false;

bool USE_SHORTENED_DYNAMIC_NAMES = true;
bool USE_OUTPUT_MEZZO = false;

bool USE_SHORTENED_INTERVAL_NAMES = true;

unsigned int SCALE_DICTIONARY_SCALES_TO_REGISTER = 0xff;

} // namespace cfg

const unsigned int SCALE_DICTIONARY_REGISTER_COMMON = 0x1;
const unsigned int SCALE_DICTIONARY_REGISTER_MODERN = 0x2;
const unsigned int SCALE_DICTIONARY_REGISTER_EXTENDED = 0x4;
const unsigned int SCALE_DICTIONARY_REGISTER_NON_TWELVE_EDO = 0x8;

} // namespace tml