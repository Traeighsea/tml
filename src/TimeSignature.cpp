#include "TimeSignature.hpp"
#include "Duration.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace tml {
std::ostream &operator<<(std::ostream &out, const TimeSignature &val) {
   out << val.ToString();
   return out;
}

bool TimeSignature::operator==(const TimeSignature &rhs) const {
   return beats_ == rhs.GetBeats() && note_value_ == rhs.GetNoteVale();
}

std::string TimeSignature::ToString() const {
   std::string out;
   out = std::to_string(beats_) + " / " + std::to_string(note_value_);
   return out;
}

TimeSignature::TimeSignature(unsigned int beats, NoteValue note_value)
      : beats_(beats),
        note_value_(static_cast<int>(note_value) <= 0 ? 4 : static_cast<unsigned int>(note_value)) {
}
TimeSignature::TimeSignature(unsigned int beats, unsigned int note_value)
      : beats_(beats), note_value_(note_value) {
}

unsigned int TimeSignature::GetBeats() const {
   return beats_;
}

unsigned int TimeSignature::GetNoteVale() const {
   return note_value_;
}

void TimeSignature::SetBeats(unsigned int beats) {
   beats_ = beats;
}

void TimeSignature::SetNoteValue(NoteValue note_value) {
   if (static_cast<int>(note_value) <= 0) {
      std::cerr << "TimeSignature::SetNoteValue: NoteValue invalid in this context.\n";
   } else {
      note_value_ = static_cast<unsigned int>(note_value);
   }
}

void TimeSignature::SetNoteValue(unsigned int note_value) {
   note_value_ = note_value;
}

void to_json(nlohmann::json &j, const TimeSignature &val) {
   j = nlohmann::json{{"beats", val.beats_}, {"note_value", val.note_value_}};
}

void from_json(const nlohmann::json &j, TimeSignature &val) {
   j.at("beats").get_to(val.beats_);
   j.at("note_value").get_to(val.note_value_);
}

}