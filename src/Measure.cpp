#include "Measure.hpp"
#include "MusicTheoryCommon.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace tml {
std::ostream &operator<<(std::ostream &out, const Measure &val) {
   out << val.ToString();
   return out;
}

bool Measure::operator==(const Measure &rhs) const {
   return key_signature_ == rhs.GetKeySignature() && time_signature_ == rhs.GetTimeSignature() &&
          notes_ == rhs.GetNotes();
}

std::string Measure::ToString() const {
   std::string out;
   out = "{";

   for (std::vector<std::pair<NoteOnset, Note>>::const_iterator it = notes_.begin();
        it != notes_.end(); it++) {
      out += "{" + it->first.ToString() + ": " + it->second.ToString() + "}";
      if (it + 1 != notes_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

Measure::Measure(KeySignature key_signature, TimeSignature time_signature)
      : key_signature_(key_signature), time_signature_(time_signature) {
}

KeySignature Measure::GetKeySignature() const {
   return key_signature_;
}

TimeSignature Measure::GetTimeSignature() const {
   return time_signature_;
}

void Measure::SetKeySignature(KeySignature key_signature) {
   key_signature_ = key_signature;
}

void Measure::SetTimeSignature(TimeSignature time_signature) {
   time_signature_ = time_signature;
}

void Measure::Add(NoteOnset start, Note new_note) {
   if (notes_.size() == 0) {
      notes_.emplace_back(std::make_pair(start, new_note));
   } else {
      std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

      while (start > it->first && it != notes_.end()) {
         it++;
      }
      if (it == notes_.end()) {
         if (start > it->first) {
            notes_.emplace_back(std::make_pair(start, new_note));
         } else {
            notes_.insert(it, std::make_pair(start, new_note));
         }
      } else {
         notes_.insert(it, std::make_pair(start, new_note));
      }
   }
}

void Measure::Remove(int index, bool circular_buffer_indexing) {
   if (notes_.empty()) {
      std::cerr
            << "Measure::Remove(int index, bool circular_buffer_indexing): Zero notes in container."
            << std::endl;
      return;
   }

   std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Measure::Remove(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   notes_.erase(it);
}

void Measure::Clear() {
   notes_.clear();
}

const std::vector<std::pair<NoteOnset, Note>> &Measure::GetNotes() const {
   return notes_;
}

std::pair<NoteOnset, Note> Measure::GetNote(int index, bool circular_buffer_indexing) const {
   if (notes_.empty()) {
      std::cerr << "Measure::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "container."
                << std::endl;
      return std::make_pair(NoteOnset(), Note());
   }

   std::vector<std::pair<NoteOnset, Note>>::const_iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Measure::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return std::make_pair(NoteOnset(), Note());
      } else {
         it += index;
      }
   }
   return *it;
}

std::pair<NoteOnset, Note> &Measure::GetNote(int index, bool circular_buffer_indexing) {
   if (notes_.empty()) {
      std::cerr << "Measure::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "container."
                << std::endl;
      // This should throw
      return notes_[index];
   }

   std::vector<std::pair<NoteOnset, Note>>::iterator it = notes_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(notes_, index);
   } else {
      if (index >= notes_.size() || index < 0) {
         std::cerr << "Measure::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         // This should throw
         return notes_[index];
      } else {
         it += index;
      }
   }
   return *it;
}

int Measure::GetNumberOfNotes() const {
   return notes_.size();
}

void to_json(nlohmann::json &j, const Measure &val) {
   j = nlohmann::json{{"key_signature", val.key_signature_},
                      {"time_signature", val.time_signature_},
                      {"notes", val.notes_}};
}

void from_json(const nlohmann::json &j, Measure &val) {
   j.at("key_signature").get_to(val.key_signature_);
   j.at("time_signature").get_to(val.time_signature_);
   j.at("notes").get_to(val.notes_);
}

}