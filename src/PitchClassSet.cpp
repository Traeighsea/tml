#include "PitchClassSet.hpp"
#include "Chord.hpp"
#include "MusicTheoryCommon.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchClass.hpp"
#include "PitchName.hpp"
#include "Scale.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {

bool PitchClassSet::operator==(const PitchClassSet &rhs) const {
   return pitch_class_enables_ == rhs.GetFullPitchClassEnables();
}

std::ostream &operator<<(std::ostream &out, const PitchClassSet &val) {
   out << val.ToString();
   return out;
}

std::string PitchClassSet::ToString() const {
   std::string out;
   constexpr int size = 12;
   constexpr int t_index = 10;
   constexpr int e_index = 11;

   out = "{";
   for (int i = 0; i < size; i++) {
      if (i == t_index) {
         out += "t, ";
      } else if (i == e_index) {
         out += "e";
      } else {
         out += std::to_string(i) + ", ";
      }
   }
   out += "}";
   return out;
}

bool &PitchClassSet::operator[](int pc) {
   return pitch_class_enables_[pc];
}
bool &PitchClassSet::operator[](unsigned pc) {
   return pitch_class_enables_[pc];
}

bool &PitchClassSet::operator[](const PitchClass &pitch_class) {
   return pitch_class_enables_[pitch_class.Get()];
}

bool &PitchClassSet::operator[](const PitchName &pitch_name) {
   return pitch_class_enables_[pitch_name.GetPitchClass().Get()];
}

bool &PitchClassSet::operator[](const Pitch &pitch) {
   return pitch_class_enables_[pitch.GetPitchClass().Get()];
}

bool &PitchClassSet::operator[](const Note &note) {
   return pitch_class_enables_[note.GetPitchClass().Get()];
}

const std::array<bool, 12> &PitchClassSet::GetFullPitchClassEnables() const {
   return pitch_class_enables_;
}

bool PitchClassSet::Get(int index) const {
   return pitch_class_enables_[CircularIndex(pitch_class_enables_, index)];
}

void PitchClassSet::Enable(int pc) {
   pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)] = true;
}

void PitchClassSet::Disable(int pc) {
   pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)] = false;
}

void PitchClassSet::Toggle(int pc) {
   pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)] =
         !pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)];
}

void PitchClassSet::Set(unsigned int pc, bool val) {
   pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)] = val;
}

void PitchClassSet::Set(int pc, bool val) {
   pitch_class_enables_[CircularIndex(pitch_class_enables_, pc)] = val;
}

void PitchClassSet::Set(const PitchClass &pitch_class, bool val) {
   pitch_class_enables_[pitch_class.Get()] = val;
}

void PitchClassSet::Set(const PitchName &pitch_name, bool val) {
   pitch_class_enables_[pitch_name.GetPitchClass().Get()] = val;
}

void PitchClassSet::Set(const Pitch &pitch, bool val) {
   pitch_class_enables_[pitch.GetPitchClass().Get()] = val;
}

void PitchClassSet::Set(const Note &note, bool val) {
   pitch_class_enables_[note.GetPitchClass().Get()] = val;
}

void PitchClassSet::Set(const Chord &chord, bool val) {
   Clear();

   std::vector<Pitch> pitches = chord.GetPitches();
   for (const Pitch it : pitches) {
      Set(it, true);
   }
}

void PitchClassSet::Set(const Scale &scale, bool val) {
   Clear();

   std::vector<Pitch> pitches = scale.GetPitches();
   for (const Pitch it : pitches) {
      Set(it, true);
   }
}

void PitchClassSet::Clear() {
   pitch_class_enables_ = {false};
}

void to_json(nlohmann::json &j, const PitchClassSet &val) {
   j = nlohmann::json{{"pitch_class_enables", val.pitch_class_enables_}};
}

void from_json(const nlohmann::json &j, PitchClassSet &val) {
   j.at("pitch_class_enables").get_to(val.pitch_class_enables_);
}

}
