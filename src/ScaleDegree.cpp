#include "ScaleDegree.hpp"
#include "Pitch.hpp"
#include "Scale.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
ScaleDegree::ScaleDegree(int degree, Accidental accidental)
      : degree_(degree), accidental_(accidental) {
}

ScaleDegree::ScaleDegree(int degree) : degree_(degree), accidental_(Accidental::Natural) {
}

bool ScaleDegree::operator==(const ScaleDegree &rhs) const {
   return (degree_ == rhs.GetDegree()) &&
          (static_cast<int>(accidental_) == static_cast<int>(rhs.GetAccidental()));
}

bool ScaleDegree::operator>(const ScaleDegree &rhs) const {
   if (degree_ > rhs.GetDegree())
      return true;
   else if (degree_ == rhs.GetDegree())
      return static_cast<int>(accidental_) > static_cast<int>(rhs.GetAccidental());
   else
      return false;
}

bool ScaleDegree::operator<(const ScaleDegree &rhs) const {
   if (degree_ > rhs.GetDegree())
      return true;
   else if (degree_ == rhs.GetDegree())
      return static_cast<int>(accidental_) < static_cast<int>(rhs.GetAccidental());
   else
      return false;
}

std::ostream &operator<<(std::ostream &out, const ScaleDegree &val) {
   out << val.ToString();
   return out;
}

std::string ScaleDegree::ToString() const {
   std::string out;
   out = ::tml::ToString(accidental_) + std::to_string(degree_);
   return out;
}

int ScaleDegree::ToInt() const {
   return (static_cast<int>(DegreeToPitchClassInt()) + static_cast<int>(accidental_) + 12) % 12;
}

int ScaleDegree::GetDegree() const {
   return degree_;
}

Accidental ScaleDegree::GetAccidental() const {
   return accidental_;
}

void ScaleDegree::SetDegree(int degree) {
   degree_ = degree;
}

void ScaleDegree::SetAccidental(const Accidental &accidental) {
   accidental_ = accidental;
}

void ScaleDegree::AddAccidental(const Accidental &accidental) {
   accidental_ =
         static_cast<Accidental>(static_cast<int>(accidental_) + static_cast<int>(accidental));
}

void ScaleDegree::Flatten() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) - 1);
}

void ScaleDegree::Sharpen() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) + 1);
}

PitchName ScaleDegree::GetPitchNameFromScale(Scale scale) const {
   // Degrees are 1-based indexing, so adjust accordingly
   PitchName pitch_name = scale.GetPitchName(degree_ - 1);
   pitch_name.AddAccidental(accidental_);
   return pitch_name;
}

Pitch ScaleDegree::GetPitchFromScale(Scale scale) const {
   // Degrees are 1-based indexing, so adjust accordingly
   Pitch pitch = scale.GetPitch(degree_ - 1);
   pitch.AddAccidental(accidental_);
   return pitch;
}

int ScaleDegree::DegreeToPitchClassInt() const {
   int degree = 0;
   switch (((degree_ - 1) % 7) + 1) {
   case 1:
      degree = 0;
      break;
   case 2:
      degree = 2;
      break;
   case 3:
      degree = 4;
      break;
   case 4:
      degree = 5;
      break;
   case 5:
      degree = 7;
      break;
   case 6:
      degree = 9;
      break;
   case 7:
      degree = 11;
      break;
   }
   return degree;
}

void to_json(nlohmann::json &j, const ScaleDegree &val) {
   j = nlohmann::json{{"degree", val.degree_}, {"accidental", val.accidental_}};
}

void from_json(const nlohmann::json &j, ScaleDegree &val) {
   j.at("degree").get_to(val.degree_);
   j.at("accidental").get_to(val.accidental_);
}

}
