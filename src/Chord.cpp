#include "Chord.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
std::ostream &operator<<(std::ostream &out, const Chord &val) {
   out << val.ToString();
   return out;
}

bool Chord::operator==(const Chord &rhs) const {
   return name_ == rhs.GetName() && root_ == rhs.GetRoot() && pitches_ == rhs.GetPitches();
}

std::string Chord::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out = root_.ToString() + " " + name_ + ": ";
   }
   for (int i = 0; i < pitches_.size(); i++) {
      out += pitches_[i].ToString();
      if (i != pitches_.size() - 1) {
         out += " ";
      }
   }
   return out;
}

std::string Chord::GetName() const {
   return name_;
}

PitchName Chord::GetRoot() const {
   return root_;
}

std::vector<Pitch> Chord::GetPitches() const {
   return pitches_;
}

void Chord::SetRoot(const PitchName &pitch_name) {
   root_ = pitch_name;
}

void Chord::SetRoot(const Pitch &pitch) {
   root_ = pitch.GetPitchName();
}

void Chord::SetRoot(const Note &note) {
   root_ = note.GetPitchName();
}

void Chord::SetName(const std::string &name) {
   name_ = name;
}

PitchName Chord::GetPitchName(int index) const {
   if (pitches_.size() == 0) {
      std::cerr << "Chord::GetPitchName(int index): Out of bounds." << std::endl;
   }
   if (index < 0) {
      return pitches_[(index + pitches_.size()) % pitches_.size()].GetPitchName();
   } else {
      return pitches_[index % pitches_.size()].GetPitchName();
   }
}

Pitch Chord::GetPitch(int index) const {
   if (pitches_.size() == 0) {
      std::cerr << "Chord::GetPitch(int index): Out of bounds." << std::endl;
   }
   if (index < 0) {
      return pitches_[(index + pitches_.size()) % pitches_.size()];
   } else {
      return pitches_[index % pitches_.size()];
   }
}

void Chord::AddPitch(const Pitch &pitch) {
   auto pos = std::find_if(pitches_.begin(), pitches_.end(), [pitch](auto pos) {
      return pos.ToInt() > pitch.ToInt();
   });
   pitches_.insert(pos, pitch);
}

void Chord::RemovePitch(int index) {
   if (index > pitches_.size() - 1 || index < 0) {
      std::cerr << "Chord::RemovePitch(int index): Out of bounds." << std::endl;
      return;
   }
   // TODO fix this travesty..
   std::vector<Pitch>::const_iterator it = pitches_.begin();
   it += index;
   pitches_.erase(it);
}

void Chord::Clear() {
   name_.clear();
   pitches_.clear();
   root_ = PitchName();
}

int Chord::GetSize() const {
   return pitches_.size();
}

void to_json(nlohmann::json &j, const Chord &val) {
   j = nlohmann::json{{"name", val.name_}, {"root", val.root_}, {"pitches", val.pitches_}};
}

void from_json(const nlohmann::json &j, Chord &val) {
   j.at("name").get_to(val.name_);
   j.at("root").get_to(val.root_);
   j.at("pitches").get_to(val.pitches_);
}

}
