#include "ToneRow.hpp"
#include "MusicTheoryCommon.hpp"
#include <algorithm>
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace tml {

bool ToneRow::operator==(const ToneRow &rhs) const {
   return set_ == rhs.GetPitchClasses() && unique_values_ == rhs.HasUniqueValues();
}

std::ostream &operator<<(std::ostream &out, const ToneRow &val) {
   out << val.ToString();
   return out;
}

std::string ToneRow::ToString() const {
   std::string out;
   out = "{";

   for (std::vector<PitchClass>::const_iterator it = set_.begin(); it != set_.end(); it++) {
      out += "{" + it->ToString() + "}";
      if (it + 1 != set_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

PitchClass &ToneRow::operator[](int index) {
   if (index >= set_.size() || index < 0) {
      std::cerr << "ToneRow::operator[](int index): Index out of bounds." << std::endl;
   }

   return set_[index];
}

PitchClass &ToneRow::operator[](unsigned index) {
   if (index >= set_.size()) {
      std::cerr << "ToneRow::operator[](unsigned index): Index out of bounds." << std::endl;
   }

   return set_[index];
}

bool ToneRow::HasUniqueValues() const {
   return unique_values_;
}

const std::vector<PitchClass> &ToneRow::GetPitchClasses() const {
   return set_;
}

std::vector<PitchClass> &ToneRow::GetPitchClasses() {
   return set_;
}

PitchClass ToneRow::Get(int index) const {
   if (set_.empty()) {
      std::cerr << "ToneRow::GetPitchClass(int index): Zero items in container." << std::endl;
      return PitchClass{};
   }
   std::vector<PitchClass>::const_iterator it = set_.begin() + index;

   return *it;
}

PitchClass ToneRow::Get(unsigned index) const {
   if (set_.empty()) {
      std::cerr << "ToneRow::GetPitchClass(unsigned index): Zero items in container." << std::endl;
      return PitchClass{};
   }
   std::vector<PitchClass>::const_iterator it = set_.begin() + index;

   return *it;
}

bool ToneRow::Append(const PitchClass &pitch_class) {
   if (unique_values_) {
      if (std::find(set_.begin(), set_.end(), pitch_class) != set_.end()) {
         return false;
      }
   }
   set_.emplace_back(pitch_class);
   return true;
}

bool ToneRow::Prepend(const PitchClass &pitch_class) {
   if (unique_values_) {
      if (std::find(set_.begin(), set_.end(), pitch_class) != set_.end()) {
         return false;
      }
   }
   set_.insert(set_.begin(), pitch_class);
   return true;
}

bool ToneRow::Insert(int index, const PitchClass &pitch_class) {
   if (unique_values_) {
      if (std::find(set_.begin(), set_.end(), pitch_class) != set_.end()) {
         return false;
      }
   }
   std::vector<PitchClass>::iterator it = set_.begin() + index;

   set_.insert(it, pitch_class);
   return true;
}

void ToneRow::Remove(int index) {
   if (set_.empty()) {
      std::cerr << "ToneRow::Remove(int index): Zero notes in sequence." << std::endl;
      return;
   }
   std::vector<PitchClass>::iterator it = set_.begin() + index;

   set_.erase(it);
}

void ToneRow::Clear() {
   set_.clear();
}

int ToneRow::GetSize() const {
   return set_.size();
}

void to_json(nlohmann::json &j, const ToneRow &val) {
   j = nlohmann::json{{"set", val.set_}, {"unique_values", val.unique_values_}};
}

void from_json(const nlohmann::json &j, ToneRow &val) {
   j.at("set").get_to(val.set_);
   j.at("unique_values").get_to(val.unique_values_);
}

}