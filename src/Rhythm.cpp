#include "Rhythm.hpp"
#include "MusicTheoryCommon.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
bool Rhythm::operator==(const Rhythm &rhs) const {
   return name_ == rhs.GetName() && durations_ == rhs.GetDurations();
}

std::ostream &operator<<(std::ostream &out, const Rhythm &val) {
   out << val.ToString();
   return out;
}

std::string Rhythm::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out += name_ + ": ";
   }

   out += "{";

   for (std::vector<Duration>::const_iterator it = durations_.begin(); it != durations_.end();
        it++) {
      out += "{" + it->ToString() + "}";
      if (it + 1 != durations_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

std::string Rhythm::GetName() const {
   return name_;
}

const std::vector<Duration> &Rhythm::GetDurations() const {
   return durations_;
}

std::vector<Duration> &Rhythm::GetDurations() {
   return durations_;
}

void Rhythm::SetName(std::string name) {
   name_ = name;
}

void Rhythm::Append(Duration new_duration) {
   durations_.emplace_back(new_duration);
}

void Rhythm::Prepend(Duration new_duration) {
   durations_.insert(durations_.begin(), new_duration);
}

void Rhythm::Insert(int index, Duration new_duration, bool circular_buffer_indexing) {
   std::vector<Duration>::iterator it = durations_.begin();

   // Lol I kinda hate this
   if (circular_buffer_indexing) {
      it += CircularIndex(durations_, index);
   } else {
      if (index >= durations_.size() || index < 0) {
         std::cerr << "Rhythm::Insert(int index, Duration new_duration, bool "
                      "circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   durations_.insert(it, new_duration);
}

void Rhythm::Remove(int index, bool circular_buffer_indexing) {
   if (durations_.empty()) {
      std::cerr
            << "Rhythm::Remove(int index, bool circular_buffer_indexing): Zero notes in durations."
            << std::endl;
      return;
   }

   std::vector<Duration>::iterator it = durations_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(durations_, index);
   } else {
      if (index >= durations_.size() || index < 0) {
         std::cerr << "Rhythm::Remove(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   durations_.erase(it);
}

void Rhythm::Clear() {
   durations_.clear();
}

Duration Rhythm::GetDuration(int index, bool circular_buffer_indexing) const {
   if (durations_.empty()) {
      std::cerr << "Rhythm::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "durations."
                << std::endl;
      return Duration{};
   }

   std::vector<Duration>::const_iterator it = durations_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(durations_, index);
   } else {
      if (index >= durations_.size() || index < 0) {
         std::cerr << "Rhythm::GetDuration(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return Duration{};
      } else {
         it += index;
      }
   }
   return *it;
}

Duration &Rhythm::GetDuration(int index, bool circular_buffer_indexing) {
   if (durations_.empty()) {
      std::cerr << "Rhythm::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "durations."
                << std::endl;
      // This should throw
      return durations_[index];
   }

   std::vector<Duration>::iterator it = durations_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(durations_, index);
   } else {
      if (index >= durations_.size() || index < 0) {
         std::cerr << "Rhythm::GetDuration(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         // This should throw
         return durations_[index];
      } else {
         it += index;
      }
   }
   return *it;
}

int Rhythm::GetSize() const {
   return durations_.size();
}

void to_json(nlohmann::json &j, const Rhythm &val) {
   j = nlohmann::json{{"name", val.name_}, {"durations", val.durations_}};
}

void from_json(const nlohmann::json &j, Rhythm &val) {
   j.at("name").get_to(val.name_);
   j.at("durations").get_to(val.durations_);
}

}