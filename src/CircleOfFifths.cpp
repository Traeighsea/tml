#include "CircleOfFifths.hpp"
#include "PitchClass.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace {
std::vector<tml::PitchLetter> pitch_letters = {
      tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E, tml::PitchLetter::F,
      tml::PitchLetter::G, tml::PitchLetter::A, tml::PitchLetter::B};
}

namespace tml {

bool CircleOfFifths::operator==(const CircleOfFifths &rhs) const {
   return current_ == rhs.GetCurrentRoot();
}

std::ostream &operator<<(std::ostream &out, const CircleOfFifths &val) {
   out << val.ToString();
   return out;
}

std::string CircleOfFifths::ToString() const {
   std::string out;
   out = current_.ToString();
   return out;
}

PitchName CircleOfFifths::GetCurrentRoot() const {
   return current_;
}

const PitchName &CircleOfFifths::CycleUp(int num) {
   for (int i = 0; i < num; i++) {
      return UpAFifth();
   }
   return current_;
}

const PitchName &CircleOfFifths::UpAFifth() {
   // This is the distance from the root to the fifth using pitch class
   const PitchClass fifth = {7};
   PitchClass new_pc = current_.GetPitchClass().Get() + fifth.Get();

   // Find the note in the sequence of note letters
   int start = 0;
   for (int i = 0; i < 7; i++) {
      if (pitch_letters[i] == current_.GetPitchLetter()) {
         start = i;
         i = 7; // set i to end the loop
      }
   }

   // Count up 5 note names to get the note letter
   start = (start + 5) % 7;
   current_.SetPitchLetter(pitch_letters[start]);

   // Resolve the accidental by resetting to a natural and determining the difference in the pitch
   // class
   current_.SetAccidental(Accidental::Natural);
   current_.SetAccidental(static_cast<Accidental>(
         (((new_pc.Get() - current_.GetPitchClass().Get()) % 12) + 12) % 12));

   return current_;
}

const PitchName &CircleOfFifths::CycleDown(int num) {
   for (int i = 0; i < num; i++) {
      return DownAFourth();
   }
   return current_;
}

const PitchName &CircleOfFifths::DownAFourth() {
   // This is the distance from the root to the fifth using pitch class
   const PitchClass fourth = {7};
   PitchClass new_pc = current_.GetPitchClass().Get() + fourth.Get();

   // Find the note in the sequence of note letters
   int start = 0;
   for (int i = 0; i < 7; i++) {
      if (pitch_letters[i] == current_.GetPitchLetter()) {
         start = i;
         i = 7; // set i to end the loop
      }
   }

   // Count down 4 note names to get the note letter
   start = (((start - 4) % 7) + 7) % 7;
   current_.SetPitchLetter(pitch_letters[start]);

   // Resolve the accidental by resetting to a natural and determining the difference in the pitch
   // class
   current_.SetAccidental(Accidental::Natural);
   current_.SetAccidental(static_cast<Accidental>(
         (((new_pc.Get() - current_.GetPitchClass().Get()) % 12) + 12) % 12));

   return current_;
}

const PitchName &CircleOfFifths::Reset() {
   current_ = {PitchLetter::C, Accidental::Natural};
   return current_;
}

std::vector<PitchName> CircleOfFifths::CalculateRange(const PitchName &start, int num) {
   std::vector<PitchName> sequence{};

   while (num > 0 || num < 0) {
      if (num > 0) {
         sequence.emplace_back(UpAFifth());
         num--;
      } else {
         sequence.emplace_back(DownAFourth());
         num++;
      }
   }

   return sequence;
}

void to_json(nlohmann::json &j, const CircleOfFifths &val) {
   j = nlohmann::json{{"current", val.current_}};
}

void from_json(const nlohmann::json &j, CircleOfFifths &val) {
   j.at("current").get_to(val.current_);
}

}
