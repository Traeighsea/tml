#include "Note.hpp"
#include "PitchClass.hpp"
#include "PitchName.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
Note::Note(const Pitch &other) : pitch_(other), duration_(), dynamic_() {
}

Note::Note(Pitch &&other) : pitch_(other), duration_(), dynamic_() {
}

Note &Note::operator=(const Pitch &rhs) {
   pitch_ = rhs;
   return *this;
}

Note &Note::operator=(Pitch &&rhs) {
   pitch_ = rhs;
   return *this;
}

Note::Note(const Pitch &pitch, const Duration &duration, const Dynamic &dynamic)
      : pitch_{pitch}, duration_{duration}, dynamic_{dynamic} {
}

Note::Note(const PitchName &other) : pitch_(other), duration_(), dynamic_() {
}

Note::Note(PitchName &&other) : pitch_(other), duration_(), dynamic_() {
}

Note &Note::operator=(const PitchName &rhs) {
   pitch_ = rhs;
   return *this;
}

Note &Note::operator=(PitchName &&rhs) {
   pitch_ = rhs;
   return *this;
}

bool Note::operator==(const Note &rhs) const {
   return pitch_ == rhs.GetPitch() && duration_ == rhs.GetDuration() &&
          dynamic_ == rhs.GetDynamic();
}

std::ostream &operator<<(std::ostream &out, const Note &val) {
   out << val.ToString();
   return out;
}

std::string Note::ToString() const {
   std::string out;
   out = pitch_.ToString() + ", " + duration_.ToString();

   if (GetDynamicMarking() != DynamicMarking::none) {
      out += ", " + dynamic_.ToString();
   }

   return out;
}

PitchClass Note::GetPitchClass() const {
   return pitch_.GetPitchClass();
}

void Note::SetPitch(const Pitch &pitch) {
   pitch_ = pitch;
}

void Note::SetDuration(const Duration &duration) {
   duration_ = duration;
}

void Note::SetDynamic(const Dynamic &dynamic) {
   dynamic_ = dynamic;
}

Pitch Note::GetPitch() const {
   return pitch_;
}

Duration Note::GetDuration() const {
   return duration_;
}

Dynamic Note::GetDynamic() const {
   return dynamic_;
}

PitchName Note::GetPitchName() const {
   return pitch_.GetPitchName();
}

Octave Note::GetOctave() const {
   return pitch_.GetOctave();
}

void Note::SetPitchName(const PitchName &pitch_name) {
   pitch_.SetPitchName(pitch_name);
}

void Note::SetOctave(const Octave &octave) {
   pitch_.SetOctave(octave);
}

PitchLetter Note::GetPitchLetter() const {
   return pitch_.GetPitchLetter();
}

Accidental Note::GetAccidental() const {
   return pitch_.GetAccidental();
}

void Note::SetPitchLetter(const PitchLetter &letter) {
   pitch_.SetPitchLetter(letter);
}

void Note::SetAccidental(const Accidental &accidental) {
   pitch_.SetAccidental(accidental);
}

void Note::AddAccidental(const Accidental &accidental) {
   pitch_.AddAccidental(accidental);
}

void Note::Flatten() {
   pitch_.Flatten();
}

void Note::Sharpen() {
   pitch_.Sharpen();
}

NoteValue Note::GetNoteValue() const {
   return duration_.GetNoteValue();
}
Dot Note::GetDotModifier() const {
   return duration_.GetDotModifier();
}

Tuplet Note::GetTuplet() const {
   return duration_.GetTuplet();
}

void Note::SetNoteValue(const NoteValue &note_value) {
   duration_.SetNoteValue(note_value);
}

void Note::SetDotModifier(const Dotted &dot_modifier) {
   duration_.SetDotModifier(dot_modifier);
}

void Note::SetDotModifier(const Dot &dot_modifier) {
   duration_.SetDotModifier(dot_modifier);
}

void Note::SetTuplet(const NamedTuplet &tuplet) {
   duration_.SetTuplet(tuplet);
}

void Note::SetTuplet(const Tuplet &tuplet) {
   duration_.SetTuplet(tuplet);
}

void Note::SetDynamicMarking(const DynamicMarking &dynamic_marking) {
   dynamic_.SetDynamicMarking(dynamic_marking);
}

DynamicMarking Note::GetDynamicMarking() const {
   return dynamic_.GetDynamicMarking();
}

void to_json(nlohmann::json &j, const Note &val) {
   j = nlohmann::json{
         {"pitch", val.pitch_}, {"duration", val.duration_}, {"dynamic", val.dynamic_}};
}

void from_json(const nlohmann::json &j, Note &val) {
   j.at("pitch").get_to(val.pitch_);
   j.at("duration").get_to(val.duration_);
   j.at("dynamic").get_to(val.dynamic_);
}

}
