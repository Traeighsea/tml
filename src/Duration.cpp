#include "Duration.hpp"
#include <nlohmann/json.hpp>

namespace tml {

Duration::Duration(const NoteValue &note_value) : note_value_{note_value} {
}

Duration::Duration(const NoteValue &note_value, const Dot &dot_modifier)
      : note_value_{note_value}, dot_modifier_{dot_modifier} {
}

Duration::Duration(const NoteValue &note_value, const Dot &dot_modifier, const Tuplet &tuplet)
      : note_value_{note_value}, dot_modifier_{dot_modifier}, tuplet_{tuplet} {
}

std::ostream &operator<<(std::ostream &out, const NoteValue &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const NoteValue &val) {
   std::string out;
   switch (val) {
   case NoteValue::Whole:
      out = "Whole";
      break;
   case NoteValue::Half:
      out = "Half";
      break;
   case NoteValue::Quarter:
      out = "Quarter";
      break;
   case NoteValue::Eighth:
      out = "Eighth";
      break;
   case NoteValue::Sixteenth:
      out = "Sixteenth";
      break;
   case NoteValue::ThirtySecond:
      out = "ThirtySecond";
      break;
   case NoteValue::SixtyFourth:
      out = "SixtyFourth";
      break;
   case NoteValue::HundredTwentyEighth:
      out = "HundredTwentyEighth";
      break;
   case NoteValue::TwoHundredFiftySixth:
      out = "TwoHundredFiftySixth";
      break;
   case NoteValue::DoubleWhole:
      out = "DoubleWhole";
      break;
   case NoteValue::Longa:
      out = "Longa";
      break;
   case NoteValue::Maxima:
      out = "Maxima";
      break;
   default:
      break;
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const NamedTuplet &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const NamedTuplet &val) {
   std::string out;
   switch (val) {
   case NamedTuplet::Duplet:
      out += " Duplet";
      break;
   case NamedTuplet::Triplet:
      out += " Triplet";
      break;
   case NamedTuplet::Quadruplet:
      out += " Quadruplet";
      break;
   case NamedTuplet::Quintuplet:
      out += " Quintuplet";
      break;
   case NamedTuplet::Sextuplet:
      out += " Sextuplet";
      break;
   case NamedTuplet::Octuplet:
      out += " Octuplet";
      break;
   default:
      out += " " + std::to_string(static_cast<int>(val)) + "-tuplet";
      break;
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const Dotted &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const Dotted &val) {
   std::string out;
   switch (val) {
   case Dotted::Single:
      out = "Dotted ";
      break;
   case Dotted::Double:
      out = "Double Dotted ";
      break;
   case Dotted::Triple:
      out = "Triple Dotted ";
      break;
   case Dotted::Quadruple:
      out = "Quadruple Dotted ";
      break;
   case Dotted::None:
      break;
   default:
      out = std::to_string(static_cast<int>(val)) + "-dotted ";
      break;
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const Duration &val) {
   out << val.ToString();
   return out;
}

std::string Duration::ToString() const {
   std::string out;
   out += ::tml::ToString(static_cast<Dotted>(dot_modifier_));
   out += ::tml::ToString(note_value_);
   out += ::tml::ToString(static_cast<NamedTuplet>(tuplet_));
   return out;
}

bool Duration::operator==(const Duration &rhs) const {
   return note_value_ == rhs.GetNoteValue() && dot_modifier_ == rhs.GetDotModifier() &&
          tuplet_ == rhs.GetTuplet();
}

bool Duration::operator>(const Duration &rhs) const {
   // @TODO:
   return false;
}

bool Duration::operator<(const Duration &rhs) const {
   // @TODO:
   return false;
}

NoteValue Duration::GetNoteValue() const {
   return note_value_;
}

Dot Duration::GetDotModifier() const {
   return dot_modifier_;
}

Tuplet Duration::GetTuplet() const {
   return tuplet_;
}

void Duration::SetNoteValue(const NoteValue &note_value) {
   note_value_ = note_value;
}

void Duration::SetDotModifier(const Dotted &dot_modifier) {
   dot_modifier_ = static_cast<Dot>(dot_modifier);
}

void Duration::SetDotModifier(const Dot &dot_modifier) {
   dot_modifier_ = dot_modifier;
}

void Duration::SetTuplet(const NamedTuplet &tuplet) {
   tuplet_ = static_cast<Tuplet>(tuplet);
}

void Duration::SetTuplet(const Tuplet &tuplet) {
   tuplet_ = tuplet;
}

void to_json(nlohmann::json &j, const Duration &val) {
   j = nlohmann::json{
         {"note_value", val.note_value_}, {"dot", val.dot_modifier_}, {"tuplet", val.tuplet_}};
}

void from_json(const nlohmann::json &j, Duration &val) {
   j.at("note_value").get_to(val.note_value_);
   j.at("dot").get_to(val.dot_modifier_);
   j.at("tuplet").get_to(val.tuplet_);
}

}
