#include "ChordFormula.hpp"
#include "Chord.hpp"
#include "ChordDegree.hpp"
#include "Interval.hpp"
#include "KeySignature.hpp"
#include "Pitch.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <optional>

namespace tml {
ChordFormula::ChordFormula(const std::string &name, const std::vector<ChordDegree> &formula)
      : name_(name), formula_(formula) {
}

bool ChordFormula::operator==(const ChordFormula &rhs) const {
   if (formula_.size() != rhs.formula_.size())
      return false;

   for (int i = 0; i < formula_.size(); i++) {
      if (!(formula_[i] == rhs.formula_[i])) {
         return false;
      }
   }
   return true;
}

std::ostream &operator<<(std::ostream &out, const ChordFormula &val) {
   out << val.ToString();
   return out;
}

std::string ChordFormula::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out = name_ + ": ";
   }
   for (int i = 0; i < formula_.size(); i++) {
      out += formula_[i].ToString();
      if (i != formula_.size() - 1) {
         out += " ";
      }
   }
   return out;
}

void ChordFormula::operator+=(const ChordDegree &rhs) {
   AddChordDegree(rhs);
}

void ChordFormula::operator+=(const ChordFormula &rhs) {
   for (ChordDegree degree : rhs.GetFormula()) {
      AddChordDegree(degree);
   }
}

void ChordFormula::operator+=(const std::vector<ChordDegree> &rhs) {
   for (ChordDegree degree : rhs) {
      AddChordDegree(degree);
   }
}

void ChordFormula::operator-=(const ChordDegree &rhs) {
   RemoveChordDegree(rhs);
}

void ChordFormula::operator-=(const ChordFormula &rhs) {
   for (ChordDegree degree : rhs.GetFormula()) {
      RemoveChordDegree(degree);
   }
}

void ChordFormula::operator-=(const std::vector<ChordDegree> &rhs) {
   for (ChordDegree degree : rhs) {
      RemoveChordDegree(degree);
   }
}

std::string ChordFormula::GetName() const {
   return name_;
}

std::vector<ChordDegree> ChordFormula::GetFormula() const {
   return formula_;
}

std::vector<ChordDegree> &ChordFormula::GetFormula() {
   return formula_;
}

void ChordFormula::SetName(const std::string &name) {
   name_ = name;
}

void ChordFormula::AddChordDegree(const ChordDegree &chord_degree) {
   auto pos = std::find_if(formula_.begin(), formula_.end(), [chord_degree](auto pos) {
      return pos < chord_degree;
   });
   formula_.insert(pos, chord_degree);
}

void ChordFormula::RemoveChordDegree(int index) {
   if (index > formula_.size() - 1 || index < 0) {
      std::cerr << "ChordFormula::RemoveChordDegree(int index): Out of bounds." << std::endl;
      return;
   }
   // TODO fix this travesty..
   std::vector<ChordDegree>::const_iterator it = formula_.begin();
   it += index;
   formula_.erase(it);
}

void ChordFormula::RemoveChordDegree(const ChordDegree &chord_degree) {
   auto pos = std::find_if(formula_.begin(), formula_.end(), [chord_degree](auto pos) {
      return pos == chord_degree;
   });
   formula_.erase(pos);
}

void ChordFormula::Clear() {
   name_.clear();
   formula_.clear();
}

int ChordFormula::GetSize() const {
   return formula_.size();
}

// @TODO: Fix
std::vector<Interval> ChordFormula::GetIntervalDistances() {
   std::vector<PitchName> chord;
   std::vector<Interval> interval_distances;

   // Start with the C Major chord for reference
   const std::vector<PitchLetter> c_major = {PitchLetter::C, PitchLetter::D, PitchLetter::E,
                                             PitchLetter::F, PitchLetter::G, PitchLetter::A,
                                             PitchLetter::B};

   // Apply the formula to the chord for reference
   for (int i = 0; i < formula_.size(); i++)
      chord.emplace_back(
            PitchName(c_major[formula_[i].GetDegree() - 1], formula_[i].GetAccidental()));

   // Calculate the intervals between the notes in the chord
   // @TODO: Update intervals to a new system that isn't as enum heavy, and sepearates number and
   // quality
   for (int i = 0; i < chord.size(); i++) {
      interval_distances.emplace_back(Interval(chord[i % chord.size()].ToInt() - chord[i].ToInt()));
   }

   return interval_distances;
}

Chord ChordFormula::ApplyChord(const PitchName &root) {
   // Get the major chord for the root from the root Key
   KeySignature key;
   key.Set(root);
   std::array<PitchName, 7> major_scale = key.PitchNames();

   // Apply the formula to the chord for reference
   Chord chord;
   Pitch pitch;
   std::optional<Pitch> prev_pitch{};
   int o = 4; // Defaulting to 4
   for (int i = 0; i < formula_.size(); i++) {
      pitch = major_scale[(formula_[i].GetDegree() - 1) % 7];
      pitch.AddAccidental(formula_[i].GetAccidental());

      if (prev_pitch) {
         if ((static_cast<int>(pitch.ToInt())) < (static_cast<int>(prev_pitch->ToInt())))
            o++;
      }
      pitch.SetOctave(o);

      chord.AddPitch(pitch);

      prev_pitch = pitch;
   }

   chord.SetRoot(root);
   chord.SetName(name_);
   return chord;
}

void to_json(nlohmann::json &j, const ChordFormula &val) {
   j = nlohmann::json{{"name", val.name_}, {"formula", val.formula_}};
}

void from_json(const nlohmann::json &j, ChordFormula &val) {
   j.at("name").get_to(val.name_);
   j.at("formula").get_to(val.formula_);
}

}