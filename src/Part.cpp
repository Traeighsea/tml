#include "Part.hpp"
#include "MusicTheoryCommon.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace tml {
Part::Part(std::string part_name, Instrument instrument, int num_measures)
      : voice_(part_name, instrument), measures_() {
   for (int i = 0; i < num_measures; i++)
      measures_.emplace_back(Measure());
}

Part::Part(Voice voice, int num_measures) : voice_(voice), measures_() {
   for (int i = 0; i < num_measures; i++)
      measures_.emplace_back(Measure());
}

bool Part::operator==(const Part &rhs) const {
   return voice_ == rhs.GetVoice() && measures_ == rhs.GetMeasures();
}

std::ostream &operator<<(std::ostream &out, const Part &val) {
   out << val.ToString();
   return out;
}

std::string Part::ToString() const {
   std::string out;
   out = "{";
   if (!GetName().empty()) {
      out += GetName() + " (" + ::tml::ToString(GetInstrument()) + "): ";
   }
   for (std::vector<Measure>::const_iterator it = measures_.begin(); it != measures_.end(); it++) {
      out += "|" + it->ToString() + "|";
      if (it + 1 != measures_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

Measure &Part::operator[](int index) {
   if (index >= measures_.size() || index < 0) {
      std::cerr << "Measure::operator[](int index): Index out of bounds." << std::endl;
   }

   return measures_[index];
}
Measure &Part::operator[](unsigned index) {
   if (index >= measures_.size()) {
      std::cerr << "Measure::operator[](unsigned index): Index out of bounds." << std::endl;
   }

   return measures_[index];
}

Voice Part::GetVoice() const {
   return voice_;
}

std::vector<Measure> Part::GetMeasures() const {
   return measures_;
}

std::vector<Measure> &Part::GetMeasures() {
   return measures_;
}

void Part::SetVoice(Voice voice) {
   voice_ = voice;
}

void Part::Append(Measure new_measure) {
   measures_.emplace_back(new_measure);
}

void Part::Prepend(Measure new_measure) {
   measures_.insert(measures_.begin(), new_measure);
}

void Part::Insert(int index, Measure new_measure, bool circular_buffer_indexing) {
   std::vector<Measure>::iterator it = measures_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(measures_, index);
   } else {
      if (index >= measures_.size() || index < 0) {
         std::cerr << "Part::Insert(int index, Note new_note, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   measures_.insert(it, new_measure);
}

void Part::Remove(int index, bool circular_buffer_indexing) {
   if (measures_.empty()) {
      std::cerr
            << "Part::Remove(int index, bool circular_buffer_indexing): Zero notes in container."
            << std::endl;
      return;
   }

   std::vector<Measure>::iterator it = measures_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(measures_, index);
   } else {
      if (index >= measures_.size() || index < 0) {
         std::cerr << "Part::Remove(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   measures_.erase(it);
}

void Part::Clear() {
   measures_.clear();
}

Measure Part::GetMeasure(int index, bool circular_buffer_indexing) const {
   if (measures_.empty()) {
      std::cerr << "Part::GetMeasure(int index, bool circular_buffer_indexing): Zero measures in "
                   "container."
                << std::endl;
      return Measure{};
   }

   std::vector<Measure>::const_iterator it = measures_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(measures_, index);
   } else {
      if (index >= measures_.size() || index < 0) {
         std::cerr << "Part::GetMeasure(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return Measure{};
      } else {
         it += index;
      }
   }
   return *it;
}

Measure &Part::GetMeasure(int index, bool circular_buffer_indexing) {
   if (measures_.empty()) {
      std::cerr << "Part::GetMeasure(int index, bool circular_buffer_indexing): Zero measures in "
                   "container."
                << std::endl;
      // This should throw
      return measures_[index];
   }

   std::vector<Measure>::iterator it = measures_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(measures_, index);
   } else {
      if (index >= measures_.size() || index < 0) {
         std::cerr << "Part::GetMeasure(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         // This should throw
         return measures_[index];
      } else {
         it += index;
      }
   }
   return *it;
}

int Part::GetNumberOfMeasures() const {
   return measures_.size();
}

std::string Part::GetName() const {
   return voice_.GetName();
}

Instrument Part::GetInstrument() const {
   return voice_.GetInstrument();
}

void Part::SetName(std::string name) {
   voice_.SetName(name);
}

void Part::SetInstrument(Instrument instrument) {
   voice_.SetInstrument(instrument);
}

void to_json(nlohmann::json &j, const Part &val) {
   j = nlohmann::json{{"voice", val.voice_}, {"measures", val.measures_}};
}

void from_json(const nlohmann::json &j, Part &val) {
   j.at("voice").get_to(val.voice_);
   j.at("measures").get_to(val.measures_);
}

}