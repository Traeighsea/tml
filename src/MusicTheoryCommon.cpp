#include "MusicTheoryCommon.hpp"

namespace tml {

double GetFrequency(int midi_note) {
   return (double)(440.0 * pow(2.0, ((double)(midi_note - 69) / 12.0)));
}

int CircularIndex(int size, int index) {
   return ((index % size) + size) % size;
}

}