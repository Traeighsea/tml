#include "GuitarFretboard.hpp"
#include "Scale.hpp"
#include "ScaleFormula.hpp"
#include <nlohmann/json.hpp>

#include <iomanip>
#include <ostream>
#include <sstream>
#include <string>

namespace tml {

bool GuitarFretboard::operator==(const GuitarFretboard &rhs) const {
   return tuning_ == rhs.GetTuning() && root_ == GetRoot() &&
          pitch_class_buffer_ == rhs.GetPitchClassBuffer() &&
          pitch_class_enables_ == rhs.GetPitchClassEnables() &&
          scale_degree_buffer_ == rhs.GetScaleDegreeBuffer() &&
          output_scale_degrees_ == rhs.GetOutputScaleDegrees();
}

std::ostream &operator<<(std::ostream &out, const GuitarFretboard &val) {
   out << val.ToString();
   return out;
}

std::string GuitarFretboard::ToString() const {
   std::stringstream out;
   const int frets = 24;

   // Output the fret numbers
   out << "      ";
   for (int i = 1; i < frets + 1; i++) {
      out << std::setw(6) << std::setfill(' ') << std::left << i;
   }
   out << "\n";

   if (output_scale_degrees_) {
      // Iterate through each string
      for (unsigned int string{0}; string < tuning_.size(); string++) {
         int pitch_index = tuning_[string].GetPitchName().ToInt();

         // Basically we need to get the pitch name, then rotate it such that the root is the index,
         // so we store the offset here to know how far off it is from 0
         int scale_degree_offset = GetRoot().ToInt();

         // Open string
         if (pitch_class_enables_[pitch_index]) {
            // std::setw() and std::setfill() do not seem to work with unicode characters
            if (scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12]
                      .GetAccidental() != Accidental::Natural) {
               out << scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12];
            } else {
               out << " " << scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12];
            }
         } else {
            out << "  ";
         }
         out << "||";
         pitch_index = (pitch_index + 1) % 12;

         // Rest of the string
         for (int i = 0; i < frets; i++) {
            std::stringstream temp_stream;
            if (pitch_class_enables_[pitch_index]) {
               // std::setw() and std::setfill() do not seem to work with unicode characters
               if (scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12]
                         .GetAccidental() != Accidental::Natural) {
                  temp_stream
                        << scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12];
               } else {
                  temp_stream
                        << "-"
                        << scale_degree_buffer_[(pitch_index - scale_degree_offset + 12) % 12];
               }
            } else {
               temp_stream << "--";
            }
            std::string temp;
            temp_stream >> temp;

            out << "-" << temp << "--|";
            pitch_index = (pitch_index + 1) % 12;
         }
         out << "|\n";
      }
   } else {
      // Iterate through each string
      for (unsigned int string{0}; string < tuning_.size(); string++) {
         int index = tuning_[string].GetPitchName().ToInt();

         // Open string
         if (pitch_class_enables_[index]) {
            // std::setw() and std::setfill() do not seem to work with unicode characters
            if (pitch_class_buffer_[index].GetAccidental() != Accidental::Natural) {
               out << pitch_class_buffer_[index];
            } else {
               out << pitch_class_buffer_[index] << " ";
            }
         } else {
            out << "  ";
         }
         out << "||";
         index = (index + 1) % 12;

         // Rest of the string
         for (int i = 0; i < frets; i++) {
            std::stringstream temp_stream;
            if (pitch_class_enables_[index]) {
               // std::setw() and std::setfill() do not seem to work with unicode characters
               if (pitch_class_buffer_[index].GetAccidental() != Accidental::Natural) {
                  temp_stream << pitch_class_buffer_[index];
               } else {
                  temp_stream << pitch_class_buffer_[index] << "-";
               }
            } else {
               temp_stream << "--";
            }
            std::string temp;
            temp_stream >> temp;

            out << "--" << temp << "-|";
            index = (index + 1) % 12;
         }
         out << "|\n";
      }
   }

   return out.str();
}

/// The String is 1 based indexing starting with the highest string to make it consistent with
/// guitarist's use
void GuitarFretboard::SetStringTuning(unsigned int string, tml::Pitch pitch) {
   string -= 1;
   tuning_[string] = pitch;
}

void GuitarFretboard::SetRoot(tml::PitchName pitch_name) {
   root_ = pitch_name;
}

void GuitarFretboard::SetPitchClassName(unsigned int pitch_class, tml::PitchName pitch_name) {
   pitch_class_buffer_[pitch_class] = pitch_name;
}

// void GuitarFretboard::SetPitchClassName(tml::PitchClass pitch_class, tml::PitchName) {
//
//}

void GuitarFretboard::SetPitchClassName(tml::PitchName pitch_name) {
   pitch_class_buffer_[pitch_name.ToInt()] = pitch_name;
}

void GuitarFretboard::EnablePitchClass(unsigned int pitch_class) {
   pitch_class_enables_[pitch_class] = true;
}

// void EnablePitchClass(tml::PitchClass pitch_class);
void GuitarFretboard::DisablePitchClass(unsigned int pitch_class) {
   pitch_class_enables_[pitch_class] = false;
}

// void DisablePitchClass(tml::PitchClass pitch_class);
void GuitarFretboard::TogglePitchClass(unsigned int pitch_class) {
   pitch_class_enables_[pitch_class] = !pitch_class_enables_[pitch_class];
}

// void TogglePitchClass(tml::PitchClass pitch_class);

void GuitarFretboard::EnableOutputScaleDegrees() {
   output_scale_degrees_ = true;
}

void GuitarFretboard::DisableOutputScaleDegrees() {
   output_scale_degrees_ = false;
}

const std::vector<tml::Pitch> &GuitarFretboard::GetTuning() const {
   return tuning_;
}

tml::PitchName GuitarFretboard::GetRoot() const {
   return root_;
}

const std::array<tml::PitchName, 12> &GuitarFretboard::GetPitchClassBuffer() const {
   return pitch_class_buffer_;
}

const std::array<bool, 12> &GuitarFretboard::GetPitchClassEnables() const {
   return pitch_class_enables_;
}

std::array<tml::ScaleDegree, 12> GuitarFretboard::GetScaleDegreeBuffer() const {
   return scale_degree_buffer_;
}

bool GuitarFretboard::GetOutputScaleDegrees() const {
   return output_scale_degrees_;
}

void GuitarFretboard::SetFromScale(tml::Scale scale) {
   for (int i = 0; i < pitch_class_enables_.size(); i++) {
      pitch_class_enables_[i] = false;
   }
   root_ = scale.GetRoot();

   auto pitches = scale.GetPitches();
   for (int i = 0; i < pitches.size(); i++) {
      pitch_class_enables_[pitches[i].GetPitchName().ToInt()] = true;
      pitch_class_buffer_[pitches[i].GetPitchName().ToInt()] = pitches[i].GetPitchName();
   }
}

void GuitarFretboard::SetFromScaleAndScaleFormula(tml::Scale scale,
                                                  tml::ScaleFormula scale_formula) {
   for (int i = 0; i < pitch_class_enables_.size(); i++) {
      pitch_class_enables_[i] = false;
   }
   root_ = scale.GetRoot();

   unsigned int scale_formula_root = root_.ToInt();
   auto scale_degrees = scale_formula.GetFormula();
   auto pitches = scale.GetPitches();
   for (int i = 0; i < pitches.size(); i++) {
      pitch_class_enables_[pitches[i].GetPitchName().ToInt()] = true;
      pitch_class_buffer_[pitches[i].GetPitchName().ToInt()] = pitches[i].GetPitchName();
      scale_degree_buffer_[(scale_degrees[i].ToInt() - root_.ToInt() + 12) % 12] = scale_degrees[i];
   }
}

void to_json(nlohmann::json &j, const GuitarFretboard &val) {
   j = nlohmann::json{{"tuning", val.tuning_},
                      {"root", val.root_},
                      {"pitch_class_buffer", val.pitch_class_buffer_},
                      {"pitch_class_enables", val.pitch_class_enables_},
                      {"scale_degree_buffer", val.scale_degree_buffer_},
                      {"output_scale_degrees", val.output_scale_degrees_}};
}

void from_json(const nlohmann::json &j, GuitarFretboard &val) {
   j.at("tuning").get_to(val.tuning_);
   j.at("root").get_to(val.root_);
   j.at("pitch_class_buffer").get_to(val.pitch_class_buffer_);
   j.at("pitch_class_enables").get_to(val.pitch_class_enables_);
   j.at("scale_degree_buffer").get_to(val.scale_degree_buffer_);
   j.at("output_scale_degrees").get_to(val.output_scale_degrees_);
}

}