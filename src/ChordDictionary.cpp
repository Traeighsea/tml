#include "ChordDictionary.hpp"
#include "Chord.hpp"
#include "ChordFormula.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchName.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
ChordDictionary::ChordDictionary(bool register_common) : dictionary_() {
   if (register_common) {
      RegisterCommon();
   }
}

bool ChordDictionary::operator==(const ChordDictionary &rhs) const {
   return dictionary_ == rhs.GetFullDictionary();
}

std::ostream &operator<<(std::ostream &out, const ChordDictionary &val) {
   out << val.ToString();
   return out;
}

std::string ChordDictionary::ToString() const {
   std::string out;
   out = "ChordDictionary number of elements: " + std::to_string(dictionary_.size()) + "\n";

   for (auto &[key, value] : dictionary_) {
      out += "{ " + value.ToString() + " }\n";
   }
   return out;
}

bool ChordDictionary::Register(const ChordFormula &formula) {
   if (formula.GetName().empty())
      return false;

   if (dictionary_.count(formula.GetName()))
      return false;

   dictionary_.emplace(std::make_pair(formula.GetName(), formula));
   return true;
}

bool ChordDictionary::Unregister(const std::string &name) {
   if (!dictionary_.count(name))
      return false;

   dictionary_.erase(name);
   return true;
}

void ChordDictionary::Clear() {
   dictionary_.clear();
}

void ChordDictionary::Reset() {
   dictionary_.clear();
   RegisterCommon();
}

ChordFormula ChordDictionary::GetChordFormula(const std::string &name) const {
   return dictionary_.at(name);
}

std::vector<std::string> ChordDictionary::GetName(const ChordFormula &formula) const {
   std::vector<std::string> strs;

   for (auto [key, value] : dictionary_) {
      if (value == formula)
         strs.push_back(key);
   }

   return strs;
}

Chord ChordDictionary::GetChord(const Pitch &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ChordDictionary::GetChord(): Error, Key not found.\n";
      return Chord();
   }

   Chord chord = (dictionary_.at(name)).ApplyChord(root);
   chord.SetName(name);
   return chord;
}

Chord ChordDictionary::GetChord(const PitchName &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ChordDictionary::GetChord(): Error, Key not found.\n";
      return Chord();
   }

   Chord chord = (dictionary_.at(name)).ApplyChord(root);
   chord.SetName(name);
   return chord;
}

Chord ChordDictionary::GetChord(const Note &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ChordDictionary::GetChord(): Error, Key not found.\n";
      return Chord();
   }

   Chord chord = (dictionary_.at(name)).ApplyChord(root);
   chord.SetName(name);
   return chord;
}

std::vector<std::string> ChordDictionary::GetRegisteredNames() const {
   std::vector<std::string> chord_list;

   for (auto [key, value] : dictionary_) {
      chord_list.push_back(key);
   }

   return chord_list;
}

void ChordDictionary::RegisterCommon() {
   // TODO: Redo this entire thing .-.

   // Major
   Register(ChordFormula("Major", {{1}, {3}, {5}}));
   Register(ChordFormula("Major 7", {{1}, {3}, {5}, {7}}));
   Register(ChordFormula("Major 9", {{1}, {3}, {5}, {7}, {9}}));
   Register(ChordFormula("Major 11", {{1}, {3}, {5}, {7}, {9}, {11}}));
   Register(ChordFormula("Major 13", {{1}, {3}, {5}, {7}, {9}, {11}, {13}}));
   Register(ChordFormula("Major 6", {{1}, {3}, {5}, {6}}));
   Register(ChordFormula("Major 6/9", {{1}, {3}, {5}, {6}, {9}}));
   Register(ChordFormula("Lydian", {{1}, {3}, {5}, {7}, {11, Accidental::Sharp}}));
   Register(ChordFormula("Major 7b13", {{1}, {3}, {5}, {7}, {9}, {11}, {13, Accidental::Sharp}}));

   // Minor
   Register(ChordFormula("Minor", {{1}, {3, Accidental::Flat}, {5}}));
   Register(ChordFormula("Minor 7", {{1}, {3, Accidental::Flat}, {5}, {7, Accidental::Flat}}));
   Register(ChordFormula("Minor 6", {{1}, {3, Accidental::Flat}, {5}, {6}}));
   Register(ChordFormula("Minor 9", {{1}, {3, Accidental::Flat}, {5}, {7, Accidental::Flat}, {9}}));
   Register(ChordFormula("Minor 11",
                         {{1}, {3, Accidental::Flat}, {5}, {7, Accidental::Flat}, {9}, {11}}));
   Register(ChordFormula(
         "Minor 13", {{1}, {3, Accidental::Flat}, {5}, {7, Accidental::Flat}, {9}, {11}, {13}}));
   Register(ChordFormula("Minor Major 7", {{1}, {3, Accidental::Flat}, {5}, {7}}));
   Register(ChordFormula("Minor Major 13", {{1}, {3, Accidental::Flat}, {5}, {7}, {9}, {13}}));

   // Diminished
   Register(ChordFormula("Diminished", {{1}, {3, Accidental::Flat}, {5, Accidental::Flat}}));
   Register(ChordFormula(
         "Full Diminished 7",
         {{1}, {3, Accidental::Flat}, {5, Accidental::Flat}, {7, Accidental::DoubleFlat}}));
   Register(
         ChordFormula("Half Diminished 7",
                      {{1}, {3, Accidental::Flat}, {5, Accidental::Flat}, {7, Accidental::Flat}}));

   // Dominant
   Register(ChordFormula("Dominant 7", {{1}, {3}, {5}, {7, Accidental::Flat}}));
   Register(ChordFormula("Dominant 9", {{1}, {3}, {5}, {7, Accidental::Flat}, {9}}));
   Register(ChordFormula("Dominant 11", {{1}, {3}, {5}, {7, Accidental::Flat}, {9}, {11}}));
   Register(ChordFormula("Dominant 13", {{1}, {3}, {5}, {7, Accidental::Flat}, {9}, {11}, {13}}));
   Register(
         ChordFormula("Lydian Dominant 7",
                      {{1}, {3}, {5}, {7, Accidental::Flat}, {9}, {11, Accidental::Sharp}, {13}}));

   // Augmented
   Register(ChordFormula("Augmented", {{1}, {3}, {5, Accidental::Sharp}}));
   Register(ChordFormula("Augmented 7", {{1}, {3}, {5, Accidental::Sharp}, {7, Accidental::Flat}}));
   Register(ChordFormula("Augmented Major 7", {{1}, {3}, {5, Accidental::Sharp}, {7}}));

   // Suspended
   Register(ChordFormula("Suspended 2nd", {{1}, {2}, {5}}));
   Register(ChordFormula("Suspended 4th", {{1}, {4}, {5}}));
   Register(ChordFormula("7 Suspended 4th", {{1}, {4}, {5}, {7, Accidental::Flat}}));
   Register(ChordFormula("9 Suspended", {{1}, {4}, {5}, {7, Accidental::Flat}, {9}}));
   Register(ChordFormula("11 Suspended", {{1}, {4}, {5}, {7, Accidental::Flat}, {9}, {11}}));
   Register(ChordFormula("13 Suspended", {{1}, {4}, {5}, {7, Accidental::Flat}, {9}, {13}}));
   Register(ChordFormula("Suspended 4th b9", {{1}, {4}, {5}, {9, Accidental::Flat}}));

   // Altered
   Register(ChordFormula("Dominant b9",
                         {{1}, {3}, {5}, {7, Accidental::Flat}, {9, Accidental::Sharp}}));
   Register(ChordFormula("Dominant #9",
                         {{1}, {3}, {5}, {7, Accidental::Flat}, {9, Accidental::Flat}}));
   Register(ChordFormula("Altered", {{1}, {3}, {7, Accidental::Flat}, {9, Accidental::Flat}}));

   // Additions
   Register(ChordFormula("Major Add9", {{1}, {3}, {5}, {9}}));
   Register(ChordFormula("Minor Add9", {{1}, {3, Accidental::Flat}, {5}, {9}}));

   // Alterations
   Register(ChordFormula("Major 7b5", {{1}, {3}, {5, Accidental::Flat}, {7}}));
   Register(ChordFormula(
         "Minor 7b5", {{1}, {3, Accidental::Flat}, {5, Accidental::Flat}, {7, Accidental::Flat}}));
   Register(ChordFormula(
         "Minor 7#5", {{1}, {3, Accidental::Flat}, {5, Accidental::Sharp}, {7, Accidental::Flat}}));
   Register(ChordFormula("Dominant #9",
                         {{1}, {3}, {5}, {7, Accidental::Flat}, {9, Accidental::Sharp}}));
   Register(ChordFormula("Suspended b9th",
                         {{1}, {4}, {5}, {7, Accidental::Flat}, {9, Accidental::Flat}}));
   Register(ChordFormula("Dominant Altered", {{1},
                                              {3},
                                              {5},
                                              {7, Accidental::Flat},
                                              {9, Accidental::Flat},
                                              {9, Accidental::Sharp},
                                              {11, Accidental::Sharp},
                                              {13, Accidental::Flat}}));

   // Misc
   Register(ChordFormula("Fifth", {{1}, {5}}));
}

const std::map<std::string, ChordFormula> &ChordDictionary::GetFullDictionary() const {
   return dictionary_;
}

void to_json(nlohmann::json &j, const ChordDictionary &val) {
   j = nlohmann::json{{"dictionary", val.dictionary_}};
}

void from_json(const nlohmann::json &j, ChordDictionary &val) {
   j.at("dictionary").get_to(val.dictionary_);
}

}
