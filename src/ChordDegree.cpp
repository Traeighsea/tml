#include "ChordDegree.hpp"
#include "Chord.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "Scale.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
ChordDegree::ChordDegree(int degree, Accidental accidental)
      : degree_(degree), accidental_(accidental) {
}

ChordDegree::ChordDegree(int degree) : degree_(degree), accidental_(Accidental::Natural) {
}

bool ChordDegree::operator==(const ChordDegree &rhs) const {
   return (degree_ == rhs.GetDegree()) &&
          (static_cast<int>(accidental_) == static_cast<int>(rhs.GetAccidental()));
}

bool ChordDegree::operator>(const ChordDegree &rhs) const {
   if (degree_ > rhs.GetDegree())
      return true;
   else if (degree_ == rhs.GetDegree())
      return static_cast<int>(accidental_) > static_cast<int>(rhs.GetAccidental());
   else
      return false;
}

bool ChordDegree::operator<(const ChordDegree &rhs) const {
   if (degree_ < rhs.GetDegree())
      return true;
   else if (degree_ == rhs.GetDegree())
      return static_cast<int>(accidental_) < static_cast<int>(rhs.GetAccidental());
   else
      return false;
}

std::ostream &operator<<(std::ostream &out, const ChordDegree &val) {
   out << val.ToString();
   return out;
}

std::string ChordDegree::ToString() const {
   std::string out;
   out = ::tml::ToString(accidental_) + std::to_string(degree_);
   return out;
}

int ChordDegree::GetDegree() const {
   return degree_;
}

Accidental ChordDegree::GetAccidental() const {
   return accidental_;
}

void ChordDegree::SetDegree(int degree) {
   degree_ = degree;
}

void ChordDegree::SetAccidental(const Accidental &accidental) {
   accidental_ = accidental;
}

void ChordDegree::AddAccidental(const Accidental &accidental) {
   accidental_ =
         static_cast<Accidental>(static_cast<int>(accidental_) + static_cast<int>(accidental));
}

void ChordDegree::Flatten() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) - 1);
}

void ChordDegree::Sharpen() {
   accidental_ = static_cast<Accidental>(static_cast<int>(accidental_) + 1);
}

PitchName ChordDegree::GetPitchNameFromChord(Chord chord) const {
   // Degrees are 1-based indexing, so adjust accordingly
   PitchName pitch_name = chord.GetPitchName(degree_ - 1);
   pitch_name.AddAccidental(accidental_);
   return pitch_name;
}

Pitch ChordDegree::GetPitchFromChord(Chord chord) const {
   // Degrees are 1-based indexing, so adjust accordingly
   Pitch pitch = chord.GetPitch(degree_ - 1);
   pitch.AddAccidental(accidental_);
   return pitch;
}

Note ChordDegree::GetNoteFromChord(Chord chord) const {
   // Degrees are 1-based indexing, so adjust accordingly
   Note note = chord.GetPitch(degree_ - 1);
   note.AddAccidental(accidental_);
   return note;
}

int ChordDegree::DegreeToPitchClassInt() const {
   int degree = 0;
   switch (((degree_ - 1) % 7) + 1) {
   case 1:
      degree = 0;
      break;
   case 2:
      degree = 2;
      break;
   case 3:
      degree = 4;
      break;
   case 4:
      degree = 5;
      break;
   case 5:
      degree = 7;
      break;
   case 6:
      degree = 9;
      break;
   case 7:
      degree = 11;
      break;
   }
   return degree;
}

void to_json(nlohmann::json &j, const ChordDegree &val) {
   j = nlohmann::json{{"degree", val.degree_}, {"accidental", val.accidental_}};
}

void from_json(const nlohmann::json &j, ChordDegree &val) {
   j.at("degree").get_to(val.degree_);
   j.at("accidental").get_to(val.accidental_);
}

}
