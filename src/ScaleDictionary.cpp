#include "ScaleDictionary.hpp"
#include "Configs.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchName.hpp"
#include "Scale.hpp"
#include "ScaleFormula.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
ScaleDictionary::ScaleDictionary(bool register_common) : dictionary_() {
   // if (SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_COMMON)
   RegisterCommon();
   // if (SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_MODERN)
   RegisterModern();
   // if (SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_EXTENDED)
   RegisterExtended();
   // if (SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_NON_TWELVE_EDO)
   RegisterNonTwelveEdo();
}

bool ScaleDictionary::operator==(const ScaleDictionary &rhs) const {
   return dictionary_ == rhs.GetFullDictionary();
}

std::ostream &operator<<(std::ostream &out, const ScaleDictionary &val) {
   out << val.ToString();
   return out;
}

std::string ScaleDictionary::ToString() const {
   std::string out;
   out = "ScaleDictionary number of elements: " + std::to_string(dictionary_.size()) + "\n";

   for (auto &[key, value] : dictionary_) {
      out += "{ " + value.ToString() + " }\n";
   }

   return out;
}

bool ScaleDictionary::Register(const ScaleFormula &formula) {
   if (formula.GetName().empty())
      return false;

   if (dictionary_.count(formula.GetName()))
      return false;

   dictionary_.emplace(std::make_pair(formula.GetName(), formula));
   return true;
}

bool ScaleDictionary::Unregister(const std::string &name) {
   if (!dictionary_.count(name))
      return false;

   dictionary_.erase(name);
   return true;
}

void ScaleDictionary::Clear() {
   dictionary_.clear();
}

void ScaleDictionary::Reset() {
   dictionary_.clear();

   if (cfg::SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_COMMON)
      RegisterCommon();
   if (cfg::SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_MODERN)
      RegisterModern();
   if (cfg::SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_EXTENDED)
      RegisterExtended();
   if (cfg::SCALE_DICTIONARY_SCALES_TO_REGISTER & SCALE_DICTIONARY_REGISTER_NON_TWELVE_EDO)
      RegisterNonTwelveEdo();
}

ScaleFormula ScaleDictionary::GetScaleFormula(const std::string &name) const {
   return dictionary_.at(name);
}

std::vector<std::string> ScaleDictionary::GetName(const ScaleFormula &formula) const {
   std::vector<std::string> strs;

   for (auto [key, value] : dictionary_) {
      if (value == formula)
         strs.push_back(key);
   }

   return strs;
}

Scale ScaleDictionary::GetScale(const Pitch &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ScaleDictionary::GetScale(): Error, Key not found.\n";
      return Scale();
   }

   Scale scale = (dictionary_.at(name)).ApplyScale(root);
   scale.SetName(name);
   return scale;
}

Scale ScaleDictionary::GetScale(const PitchName &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ScaleDictionary::GetScale(): Error, Key not found.\n";
      return Scale();
   }

   Scale scale = (dictionary_.at(name)).ApplyScale(root);
   scale.SetName(name);
   return scale;
}

Scale ScaleDictionary::GetScale(const Note &root, const std::string &name) {
   if (!dictionary_.count(name)) {
      std::cerr << "ScaleDictionary::GetScale(): Error, Key not found.\n";
      return Scale();
   }

   Scale scale = (dictionary_.at(name)).ApplyScale(root);
   scale.SetName(name);
   return scale;
}

std::vector<std::string> ScaleDictionary::GetRegisteredNames() const {
   std::vector<std::string> scale_list;

   for (auto [key, value] : dictionary_) {
      scale_list.push_back(key);
   }

   return scale_list;
}

const std::map<std::string, ScaleFormula> &ScaleDictionary::GetFullDictionary() const {
   return dictionary_;
}

void ScaleDictionary::RegisterCommon() {
   // Classical Scales

   Register(ScaleFormula("Major", {{1}, {2}, {3}, {4}, {5}, {6}, {7}}));
   Register(ScaleFormula("Minor", {{1},
                                   {2},
                                   {3, Accidental::Flat},
                                   {4},
                                   {5},
                                   {6, Accidental::Flat},
                                   {7, Accidental::Flat}}));
   Register(ScaleFormula("Harmonic Minor",
                         {{1}, {2}, {3, Accidental::Flat}, {4}, {5}, {6, Accidental::Flat}, {7}}));
   Register(ScaleFormula("Melodic Minor", {{1}, {2}, {3, Accidental::Flat}, {4}, {5}, {6}, {7}}));

   Register(ScaleFormula("Pentatonic", {{1}, {2}, {3}, {5}, {6}}));
   Register(ScaleFormula("Minor Pentatonic",
                         {{1}, {3, Accidental::Flat}, {4}, {5}, {7, Accidental::Flat}}));

   // Modes

   Register(ScaleFormula("Ionian", {{1}, {2}, {3}, {4}, {5}, {6}, {7}}));
   Register(ScaleFormula("Dorian",
                         {{1}, {2}, {3, Accidental::Flat}, {4}, {5}, {6}, {7, Accidental::Flat}}));
   Register(ScaleFormula("Phrygian", {{1},
                                      {2, Accidental::Flat},
                                      {3, Accidental::Flat},
                                      {4},
                                      {5},
                                      {6, Accidental::Flat},
                                      {7, Accidental::Flat}}));
   Register(ScaleFormula("Lydian", {{1}, {2}, {3}, {4, Accidental::Sharp}, {5}, {6}, {7}}));
   Register(ScaleFormula("Mixolydian", {{1}, {2}, {3}, {4}, {5}, {6}, {7, Accidental::Flat}}));
   Register(ScaleFormula("Aeolian", {{1},
                                     {2},
                                     {3, Accidental::Flat},
                                     {4},
                                     {5},
                                     {6, Accidental::Flat},
                                     {7, Accidental::Flat}}));
   Register(ScaleFormula("Locrian", {{1},
                                     {2, Accidental::Flat},
                                     {3, Accidental::Flat},
                                     {4},
                                     {5, Accidental::Flat},
                                     {6, Accidental::Flat},
                                     {7, Accidental::Flat}}));

   // Hybrid Modes

   Register(ScaleFormula("Locrian #2", {{1},
                                        {2},
                                        {3, Accidental::Flat},
                                        {4},
                                        {5, Accidental::Flat},
                                        {6, Accidental::Flat},
                                        {7, Accidental::Flat}}));
   Register(ScaleFormula("Lydian-Dominant",
                         {{1}, {2}, {3}, {4, Accidental::Sharp}, {5}, {6}, {7, Accidental::Flat}}));
   Register(ScaleFormula("Phrygian-Dorian", {{1},
                                             {2, Accidental::Flat},
                                             {3, Accidental::Flat},
                                             {4},
                                             {5},
                                             {6},
                                             {7, Accidental::Flat}}));
   Register(
         ScaleFormula("Lydian-Augmented",
                      {{1}, {2}, {3}, {4, Accidental::Sharp}, {5, Accidental::Sharp}, {6}, {7}}));
   Register(ScaleFormula("Mixolydian-b6",
                         {{1}, {2}, {3}, {4}, {5}, {6, Accidental::Flat}, {7, Accidental::Flat}}));

   // Jazz Scales

   Register(ScaleFormula(
         "Blues",
         {{1}, {3, Accidental::Flat}, {4}, {5, Accidental::Flat}, {5}, {7, Accidental::Flat}}));
   Register(ScaleFormula("Bebop Dominant",
                         {{1}, {2}, {3}, {4}, {5}, {6}, {7}, {7, Accidental::Flat}}));
   Register(
         ScaleFormula("Bebop Major", {{1}, {2}, {3}, {4}, {5}, {5, Accidental::Sharp}, {6}, {7}}));
   Register(ScaleFormula(
         "Whole Tone",
         {{1}, {2}, {3}, {4, Accidental::Sharp}, {5, Accidental::Sharp}, {6, Accidental::Sharp}}));
   Register(ScaleFormula("Half Diminished", {{1},
                                             {2},
                                             {3, Accidental::Flat},
                                             {4},
                                             {5, Accidental::Flat},
                                             {6, Accidental::Flat},
                                             {7, Accidental::Flat}}));
   Register(ScaleFormula("Octatonic (Half-Whole)", {{1},
                                                    {2, Accidental::Flat},
                                                    {3, Accidental::Flat},
                                                    {3},
                                                    {4, Accidental::Sharp},
                                                    {5},
                                                    {6},
                                                    {7, Accidental::Flat}}));
   Register(ScaleFormula("Octatonic (Whole-Half)", {{1},
                                                    {2},
                                                    {3, Accidental::Flat},
                                                    {4},
                                                    {4, Accidental::Sharp},
                                                    {5, Accidental::Sharp},
                                                    {6},
                                                    {7}}));
   Register(ScaleFormula("Diminished-Whole Tone", {{1},
                                                   {2, Accidental::Flat},
                                                   {3, Accidental::Flat},
                                                   {4, Accidental::Flat},
                                                   {5, Accidental::Flat},
                                                   {6, Accidental::Flat},
                                                   {7, Accidental::Flat}}));
   Register(ScaleFormula("Phrygian Dominant", {{1},
                                               {2, Accidental::Flat},
                                               {3},
                                               {4},
                                               {5},
                                               {6, Accidental::Flat},
                                               {7, Accidental::Flat}}));
   Register(ScaleFormula("Augmented",
                         {{1}, {3, Accidental::Flat}, {3}, {5}, {5, Accidental::Sharp}, {7}}));
}

void ScaleDictionary::RegisterModern() {
}

void ScaleDictionary::RegisterExtended() {
}

void ScaleDictionary::RegisterNonTwelveEdo() {
}

void to_json(nlohmann::json &j, const ScaleDictionary &val) {
   j = nlohmann::json{{"dictionary", val.dictionary_}};
}

void from_json(const nlohmann::json &j, ScaleDictionary &val) {
   j.at("dictionary").get_to(val.dictionary_);
}

}