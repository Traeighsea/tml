#include "KeySignature.hpp"
#include <nlohmann/json.hpp>
#include <ostream>
#include <string>

namespace tml {
int accidentals_ = 0;

/// Wether or not this key is major or minor
bool major_ = true;

bool KeySignature::operator==(const KeySignature &rhs) const {
   return accidentals_ == rhs.GetNumAccidentals() && major_ == rhs.IsMajor() &&
          notes_ == rhs.GetNotes();
}

std::ostream &operator<<(std::ostream &out, const KeySignature &val) {
   out << val.ToString();
   return out;
}

std::string KeySignature::ToString() const {
   std::string out;
   if (major_) {
      out = notes_[0].ToString() + " Major: ";
   } else {
      out = notes_[5].ToString() + " Minor: ";
   }

   for (int i = 0; i < 7; i++) {
      if (major_) {
         out += notes_[i].ToString() + ((i != 6) ? " " : "");
      } else {
         out += notes_[(i + 5) % 7].ToString() + ((i != 6) ? " " : "");
      }
   }

   return out;
}

KeySignature::KeySignature(PitchName root, bool is_major_key) {
   Set(root, is_major_key);
}

KeySignature::KeySignature(int num_accidentals, bool is_major_key) {
   Set(num_accidentals, is_major_key);
}

void KeySignature::Set(PitchName root, bool is_major_key) {
   major_ = is_major_key;

   // First init the array to C Major
   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};
   int i = 0;
   for (auto it_letter : pitch_letters) {
      notes_[i].SetPitchLetter(it_letter);
      notes_[i].SetAccidental(Accidental::Natural);
      i++;
   }

   // Determine where the passed in root is and set the accidental
   int root_index = 0;
   for (i = 0; i < 7; i++) {
      if (notes_[i].GetPitchLetter() == root.GetPitchLetter()) {
         root_index = i;
         notes_[i].SetAccidental(root.GetAccidental());
         i = 7; // set i to end the loop
      }
   }

   // Start with transposition so the (major) root is in position [0]
   if (major_) {
      if (root_index != 0) {
         std::array<PitchName, 7> notes;
         for (i = 0; i < 7; i++)
            notes[i] = notes_[(i + root_index) % 7];
         notes_ = notes;
      }
   } else {
      if (root_index != 5) {
         std::array<PitchName, 7> notes;
         for (i = 0; i < 7; i++)
            notes[i] = notes_[(i + root_index + 2) % 7];
         notes_ = notes;
      }
   }

   // Now make sure the distance from the root fits the distance of a diatonic scale, adjust
   // accidental accordingly
   int diatonic_intervals[7] = {2, 2, 1, 2, 2, 2, 1};
   for (i = 0; i < 6; i++) {
      if (major_) {
         // Add sharps or flats
         if (notes_[i].ToInt() > notes_[(i + 1) % 7].ToInt()) {
            notes_[(i + 1) % 7].SetAccidental(static_cast<Accidental>(
                  ((notes_[i].ToInt() + diatonic_intervals[i]) - (notes_[i + 1].ToInt() + 12)) %
                  12));
         } else {
            notes_[(i + 1) % 7].SetAccidental(static_cast<Accidental>(
                  ((notes_[i].ToInt() + diatonic_intervals[i]) - notes_[(i + 1) % 7].ToInt()) %
                  12));
         }
      } else {
         // Add sharps or flats
         if (notes_[(i + 5) % 7].ToInt() > notes_[(i + 1 + 5) % 7].ToInt()) {
            notes_[(i + 1 + 5) % 7].SetAccidental(static_cast<Accidental>(
                  ((notes_[(i + 5) % 7].ToInt() + diatonic_intervals[(i + 5) % 7]) -
                   (notes_[(i + 1 + 5) % 7].ToInt() + 12)) %
                  12));
         } else {
            notes_[(i + 1 + 5) % 7].SetAccidental(static_cast<Accidental>(
                  ((notes_[(i + 5) % 7].ToInt() + diatonic_intervals[(i + 5) % 7]) -
                   notes_[(i + 1 + 5) % 7].ToInt()) %
                  12));
         }
      }
   }
}

/// Number of accidentals from -7 to 7 where negative numbers are flat keys and positive are sharp
/// keys
void KeySignature::Set(int num_accidentals, bool is_major_key) {
   major_ = is_major_key;

   // First init the array to C Major
   auto pitch_letters = {tml::PitchLetter::C, tml::PitchLetter::D, tml::PitchLetter::E,
                         tml::PitchLetter::F, tml::PitchLetter::G, tml::PitchLetter::A,
                         tml::PitchLetter::B};
   int i;

   i = 0;
   for (auto it_letter : pitch_letters) {
      notes_[i].SetPitchLetter(it_letter);
      notes_[i].SetAccidental(Accidental::Natural);
      i++;
   }
   int root = 0; // C is the root currently at the 0th position

   // Next calculate where the accidentals need to go
   if (num_accidentals > 0) { // if sharp key
      while (num_accidentals > 0) {
         // Sharp the 4th
         notes_[(root + 3) % 7].SetAccidental(static_cast<Accidental>(
               static_cast<int>(notes_[(root + 3) % 7].GetAccidental()) + 1));
         // Set the new root to the 5th
         root = (root + 4) % 7;

         num_accidentals--;
      }
   } else { // else flat key
      while (num_accidentals < 0) {
         // Flat the 7th
         notes_[(root + 6) % 7].SetAccidental(static_cast<Accidental>(
               static_cast<int>(notes_[(root + 6) % 7].GetAccidental()) - 1));
         // Set the root to the 4th
         root = (root + 3) % 7;

         num_accidentals++;
      }
   }

   // Finally transpose so the root is in position [0]
   if (root != 0) {
      std::array<PitchName, 7> notes;
      for (int i = 0; i < 7; i++)
         notes[i] = notes_[(i + root) % 7];
      notes_ = notes;
   }
}

int KeySignature::GetNumAccidentals() const {
   return accidentals_;
}
bool KeySignature::IsMajor() const {
   return major_;
}

const std::array<PitchName, 7> &KeySignature::GetNotes() const {
   return notes_;
}

PitchName KeySignature::KeyCenter() {
   return notes_[0];
}

PitchName KeySignature::Root() {
   if (major_)
      return notes_[0];
   else
      return notes_[5];
}

PitchName KeySignature::MinorRoot() {
   return notes_[5];
}

PitchName KeySignature::MajorRoot() {
   return notes_[0];
}

std::array<PitchName, 7> KeySignature::PitchNames() const {
   if (major_) {
      return notes_;
   } else {
      std::array<PitchName, 7> notes;
      for (int i = 0; i < 7; i++)
         notes[i] = notes_[((i + 5) % 7)];
      return notes;
   }
}

/*
Scale KeySignature::MajorScale()
{
   return Scale();
}

Scale KeySignature::MinorScale()
{
   return Scale();
}
*/

void to_json(nlohmann::json &j, const KeySignature &val) {
   j = nlohmann::json{
         {"accidentals", val.accidentals_}, {"major", val.major_}, {"notes", val.notes_}};
}

void from_json(const nlohmann::json &j, KeySignature &val) {
   j.at("accidentals").get_to(val.accidentals_);
   j.at("major").get_to(val.major_);
   j.at("notes").get_to(val.notes_);
}
}