#include "Interval.hpp"
#include "Configs.hpp"
#include "IntervalClass.hpp"
#include "Note.hpp"
#include "Pitch.hpp"
#include "PitchName.hpp"
#include <nlohmann/json.hpp>

#include <cmath>
#include <iostream>
#include <sstream>

namespace tml {

bool Interval::operator==(const Interval &rhs) const {
   return distance_ == rhs.GetDistance() && quality_ == rhs.GetIntervalQuality() &&
          interval_number_ == rhs.GetIntervalNumber() && name_ == rhs.GetName();
}

std::ostream &operator<<(std::ostream &out, const NamedInterval &val) {
   out << ToString(val);
   return out;
}

std::string ToString(const NamedInterval &val) {
   std::string out;
   if (cfg::USE_SHORTENED_INTERVAL_NAMES) {
      switch (val) {
      case (NamedInterval::PerfectUnison):
         out = "P1";
         break;
      case (NamedInterval::MinorSecond):
         out = "m2";
         break;
      case (NamedInterval::MajorSecond):
         out = "M2";
         break;
      case (NamedInterval::MinorThird):
         out = "m3";
         break;
      case (NamedInterval::MajorThird):
         out = "M3";
         break;
      case (NamedInterval::PerfectFourth):
         out = "P4";
         break;
      case (NamedInterval::AugmentedFourth):
         out = "A4";
         break;
      case (NamedInterval::DiminishedFifth):
         out = "D5";
         break;
      case (NamedInterval::PerfectFifth):
         out = "P5";
         break;
      case (NamedInterval::MinorSixth):
         out = "m6";
         break;
      case (NamedInterval::MajorSixth):
         out = "M6";
         break;
      case (NamedInterval::MinorSeventh):
         out = "m7";
         break;
      case (NamedInterval::MajorSeventh):
         out = "M7";
         break;
      case (NamedInterval::PerfectOctave):
         out = "P8";
         break;
      default:
         out = std::to_string(static_cast<int>(val)) + " S";
         break;
      }
   } else {
      switch (val) {
      case (NamedInterval::PerfectUnison):
         out = "Perfect Unison";
         break;
      case (NamedInterval::MinorSecond):
         out = "Minor Second";
         break;
      case (NamedInterval::MajorSecond):
         out = "Major Second";
         break;
      case (NamedInterval::MinorThird):
         out = "Minor Third";
         break;
      case (NamedInterval::MajorThird):
         out = "Major Third";
         break;
      case (NamedInterval::PerfectFourth):
         out = "Perfect Fourth";
         break;
      case (NamedInterval::AugmentedFourth):
         out = "Augmented Fourth";
         break;
      case (NamedInterval::DiminishedFifth):
         out = "Diminished Fifth";
         break;
      case (NamedInterval::PerfectFifth):
         out = "Perfect Fifth";
         break;
      case (NamedInterval::MinorSixth):
         out = "Minor Sixth";
         break;
      case (NamedInterval::MajorSixth):
         out = "Major Sixth";
         break;
      case (NamedInterval::MinorSeventh):
         out = "Minor Seventh";
         break;
      case (NamedInterval::MajorSeventh):
         out = "Major Seventh";
         break;
      case (NamedInterval::PerfectOctave):
         out = "Perfect Octave";
         break;
      default:
         out = std::to_string(static_cast<int>(val)) + " Semitones";
         break;
      }
   }
   return out;
}

// @TODO: Fix all Type Conversions to have accurate and correct way to determine the interval
Interval::Interval(int distance)
      : distance_(distance), quality_(IntervalQuality::Perfect), interval_number_(1), name_("") {
}

Interval::Interval(int from, int to)
      : distance_(to - from), quality_(IntervalQuality::Perfect), interval_number_(1), name_("") {
}

Interval::Interval(const PitchName &from, const PitchName &to)
      : distance_(to.ToInt() - from.ToInt()), quality_(IntervalQuality::Perfect),
        interval_number_(1), name_("") {
}

Interval::Interval(const Pitch &from, const Pitch &to)
      : distance_(to.ToInt() - from.ToInt()), quality_(IntervalQuality::Perfect),
        interval_number_(1), name_("") {
}

Interval::Interval(const Note &from, const Note &to)
      : distance_(to.GetPitch().ToInt() - from.GetPitch().ToInt()),
        quality_(IntervalQuality::Perfect), interval_number_(1), name_("") {
}

IntervalClass Interval::GetIntervalClass() const {
   IntervalClass ic;
   // TODO: Surely this can be solved more elegantly
   if (interval_number_ == 1 || interval_number_ == 8) {
      ic = 0;
   } else if ((quality_ == IntervalQuality::Minor && interval_number_ == 2) ||
              (quality_ == IntervalQuality::Major && interval_number_ == 7)) {
      ic = 1;
   } else if ((quality_ == IntervalQuality::Major && interval_number_ == 2) ||
              (quality_ == IntervalQuality::Minor && interval_number_ == 7)) {
      ic = 2;

   } else if ((quality_ == IntervalQuality::Minor && interval_number_ == 3) ||
              (quality_ == IntervalQuality::Major && interval_number_ == 6)) {
      ic = 3;
   } else if ((quality_ == IntervalQuality::Major && interval_number_ == 3) ||
              (quality_ == IntervalQuality::Minor && interval_number_ == 6)) {
      ic = 4;
   } else if ((quality_ == IntervalQuality::Minor && interval_number_ == 4) ||
              (quality_ == IntervalQuality::Major && interval_number_ == 5)) {
      ic = 5;
   } else if ((quality_ == IntervalQuality::Augmented && interval_number_ == 4) ||
              (quality_ == IntervalQuality::Diminished && interval_number_ == 5)) {
      ic = 6;
   } else {
      std::cerr << "" << std::endl;
   }
   return ic;
}

std::ostream &operator<<(std::ostream &out, const Interval &val) {
   out << val.ToString();
   return out;
}

std::string Interval::ToString() const {
   std::string out;
   if (distance_ > 0) {
      if (cfg::USE_SHORTENED_INTERVAL_NAMES)
         out = "+";
      else
         out = "Up a ";
   } else if (distance_ < 0) {
      if (cfg::USE_SHORTENED_INTERVAL_NAMES)
         out = "-";
      else
         out = "Down a ";
   }
   out += name_;
   return out;
}

int Interval::GetDistance() const {
   return distance_;
}

Direction Interval::GetDirection() const {
   if (distance_ > 0)
      return Direction::Up;
   else if (distance_ < 0)
      return Direction::Down;
   else
      return Direction::None;
}

NamedInterval Interval::GetNamedInterval() const {
   NamedInterval named_interval = NamedInterval::PerfectUnison;

   switch (interval_number_) {
   case (1):
      if (quality_ == IntervalQuality::Perfect)
         named_interval = NamedInterval::PerfectUnison;
      break;
   case (2):
      if (quality_ == IntervalQuality::Minor)
         named_interval = NamedInterval::MinorSecond;
      else if (quality_ == IntervalQuality::Major)
         named_interval = NamedInterval::MajorSecond;
      break;
   case (3):
      if (quality_ == IntervalQuality::Minor)
         named_interval = NamedInterval::MinorThird;
      else if (quality_ == IntervalQuality::Major)
         named_interval = NamedInterval::MajorThird;
   case (4):
      if (quality_ == IntervalQuality::Augmented)
         named_interval = NamedInterval::AugmentedFourth;
      else if (quality_ == IntervalQuality::Perfect)
         named_interval = NamedInterval::PerfectFourth;
      break;
   case (5):
      if (quality_ == IntervalQuality::Diminished)
         named_interval = NamedInterval::DiminishedFifth;
      else if (quality_ == IntervalQuality::Perfect)
         named_interval = NamedInterval::PerfectFifth;
      break;
   case (6):
      if (quality_ == IntervalQuality::Minor)
         named_interval = NamedInterval::MinorSixth;
      else if (quality_ == IntervalQuality::Major)
         named_interval = NamedInterval::MajorSixth;
      break;
   case (7):
      if (quality_ == IntervalQuality::Minor)
         named_interval = NamedInterval::MinorSeventh;
      else if (quality_ == IntervalQuality::Major)
         named_interval = NamedInterval::MajorSeventh;
      break;
   case (8):
      if (quality_ == IntervalQuality::Perfect)
         named_interval = NamedInterval::PerfectOctave;
      break;
   default:
      break;
   }

   return named_interval;
}

IntervalQuality Interval::GetIntervalQuality() const {
   return quality_;
}

int Interval::GetIntervalNumber() const {
   return interval_number_;
}

std::string Interval::GetName() const {
   return name_;
}

// @TODO Need to update this method for better error handling of invalid data .. and there's
// probably a better way to handle this in general
void Interval::Set(IntervalQuality quality, int interval_number, Direction direction) {
   if (interval_number > 8 || interval_number < 1) {
      std::cerr << "Invalid Interval Number." << std::endl;
      return;
   }
   std::stringstream str;
   switch (interval_number) {
   case (1):
      if (quality == IntervalQuality::Perfect) {
         distance_ = static_cast<int>(NamedInterval::PerfectUnison);
         str << NamedInterval::PerfectUnison;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (2):
      if (quality == IntervalQuality::Minor) {
         distance_ = static_cast<int>(NamedInterval::MinorSecond);
         str << NamedInterval::MinorSecond;
      } else if (quality == IntervalQuality::Major) {
         distance_ = static_cast<int>(NamedInterval::MajorSecond);
         str << NamedInterval::MajorSecond;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (3):
      if (quality == IntervalQuality::Minor) {
         distance_ = static_cast<int>(NamedInterval::MinorThird);
         str << NamedInterval::MinorThird;
      } else if (quality == IntervalQuality::Major) {
         distance_ = static_cast<int>(NamedInterval::MajorThird);
         str << NamedInterval::MajorThird;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (4):
      if (quality == IntervalQuality::Augmented) {
         distance_ = static_cast<int>(NamedInterval::AugmentedFourth);
         str << NamedInterval::AugmentedFourth;
      } else if (quality == IntervalQuality::Perfect) {
         distance_ = static_cast<int>(NamedInterval::PerfectFourth);
         str << NamedInterval::PerfectFourth;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (5):
      if (quality == IntervalQuality::Diminished) {
         distance_ = std::abs(static_cast<int>(NamedInterval::DiminishedFifth));
         str << NamedInterval::DiminishedFifth;
      } else if (quality == IntervalQuality::Perfect) {
         distance_ = static_cast<int>(NamedInterval::PerfectFifth);
         str << NamedInterval::PerfectFifth;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (6):
      if (quality == IntervalQuality::Minor) {
         distance_ = static_cast<int>(NamedInterval::MinorSixth);
         str << NamedInterval::MinorSixth;
      } else if (quality == IntervalQuality::Major) {
         distance_ = static_cast<int>(NamedInterval::MajorSixth);
         str << NamedInterval::MajorSixth;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (7):
      if (quality == IntervalQuality::Minor) {
         distance_ = static_cast<int>(NamedInterval::MinorSeventh);
         str << NamedInterval::MinorSeventh;
      } else if (quality == IntervalQuality::Major) {
         distance_ = static_cast<int>(NamedInterval::MajorSeventh);
         str << NamedInterval::MajorSeventh;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   case (8):
      if (quality == IntervalQuality::Perfect) {
         distance_ = static_cast<int>(NamedInterval::PerfectOctave);
         str << NamedInterval::PerfectOctave;
      } else {
         std::cerr << "Invalid Interval." << std::endl;
         return;
      }
      break;
   default:
      std::cerr << "Invalid Interval." << std::endl;
      return;
   }
   quality_ = quality;
   interval_number_ = interval_number;
   if (direction == Direction::Down)
      distance_ = -distance_;
   str >> name_;
}

void Interval::Set(NamedInterval named_interval, Direction direction) {
   // Special case that needs to differentiate between DiminishedFifth and AugmentedFourth
   if (named_interval == NamedInterval::DiminishedFifth)
      distance_ = std::abs(static_cast<int>(named_interval));
   else
      distance_ = static_cast<int>(named_interval);

   switch (named_interval) {
   case (NamedInterval::PerfectUnison):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 1;
      break;
   case (NamedInterval::MinorSecond):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 2;
      break;
   case (NamedInterval::MajorSecond):
      quality_ = IntervalQuality::Major;
      interval_number_ = 2;
      break;
   case (NamedInterval::MinorThird):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 3;
      break;
   case (NamedInterval::MajorThird):
      quality_ = IntervalQuality::Major;
      interval_number_ = 3;
      break;
   case (NamedInterval::PerfectFourth):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 4;
      break;
   case (NamedInterval::AugmentedFourth):
      quality_ = IntervalQuality::Augmented;
      interval_number_ = 4;
      break;
   case (NamedInterval::DiminishedFifth):
      quality_ = IntervalQuality::Diminished;
      interval_number_ = 5;
      break;
   case (NamedInterval::PerfectFifth):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 5;
      break;
   case (NamedInterval::MinorSixth):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 6;
      break;
   case (NamedInterval::MajorSixth):
      quality_ = IntervalQuality::Major;
      interval_number_ = 6;
      break;
   case (NamedInterval::MinorSeventh):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 7;
      break;
   case (NamedInterval::MajorSeventh):
      quality_ = IntervalQuality::Major;
      interval_number_ = 7;
      break;
   case (NamedInterval::PerfectOctave):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 8;
      break;
   default:
      std::cerr << "Invalid Interval." << std::endl;
      break;
   }
   if (direction == Direction::Down)
      distance_ = -distance_;
   std::stringstream str;
   str << named_interval;
   str >> name_;
}

void Interval::Set(int distance) {
   distance_ = distance;

   switch (static_cast<NamedInterval>(std::abs(distance_))) {
   case (NamedInterval::PerfectUnison):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 1;
      break;
   case (NamedInterval::MinorSecond):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 2;
      break;
   case (NamedInterval::MajorSecond):
      quality_ = IntervalQuality::Major;
      interval_number_ = 2;
      break;
   case (NamedInterval::MinorThird):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 3;
      break;
   case (NamedInterval::MajorThird):
      quality_ = IntervalQuality::Major;
      interval_number_ = 3;
      break;
   case (NamedInterval::PerfectFourth):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 4;
      break;
   case (NamedInterval::AugmentedFourth):
      quality_ = IntervalQuality::Augmented;
      interval_number_ = 4;
      break;
   // Can't actually get this value unfortunately due to not enough information on weather the
   // interval is a fourth or fifth
   case (NamedInterval::DiminishedFifth):
      quality_ = IntervalQuality::Diminished;
      interval_number_ = 5;
      break;
   case (NamedInterval::PerfectFifth):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 5;
      break;
   case (NamedInterval::MinorSixth):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 6;
      break;
   case (NamedInterval::MajorSixth):
      quality_ = IntervalQuality::Major;
      interval_number_ = 6;
      break;
   case (NamedInterval::MinorSeventh):
      quality_ = IntervalQuality::Minor;
      interval_number_ = 7;
      break;
   case (NamedInterval::MajorSeventh):
      quality_ = IntervalQuality::Major;
      interval_number_ = 7;
      break;
   case (NamedInterval::PerfectOctave):
      quality_ = IntervalQuality::Perfect;
      interval_number_ = 8;
      break;
   default:
      std::cerr << "Invalid Interval." << std::endl;
      break;
   }
   std::stringstream str;
   str << static_cast<NamedInterval>(distance_);
   str >> name_;
}

void Interval::Invert() {
   // Rules pulled from wikipedia for reference
   // [Interval](https://en.wikipedia.org/wiki/Interval_(music)#Interval)
   // 1. The interval number and the number of its inversion always add up to nine (4 + 5 = 9, in
   // the example just given).
   // 2. The inversion of a major interval is a minor interval, and vice versa; the inversion of a
   // perfect interval is also perfect; the inversion of an augmented interval is a diminished
   // interval, and vice versa; the inversion of a doubly augmented interval is a doubly diminished
   // interval, and vice versa.
   int new_interval_number = 9 - interval_number_;
   IntervalQuality new_quality = IntervalQuality::Perfect;
   switch (quality_) {
   case (IntervalQuality::Minor):
      new_quality = IntervalQuality::Major;
      break;
   case (IntervalQuality::Major):
      new_quality = IntervalQuality::Minor;
      break;
   case (IntervalQuality::Diminished):
      new_quality = IntervalQuality::Augmented;
      break;
   case (IntervalQuality::Augmented):
      new_quality = IntervalQuality::Diminished;
      break;
   case (IntervalQuality::Perfect):
   default:
      std::cerr << "Invalid IntervalQuality" << std::endl;
      break;
   }
   Set(new_quality, new_interval_number, ((distance_ > 0) ? Direction::Down : Direction::Up));
}

void to_json(nlohmann::json &j, const Interval &val) {
   j = nlohmann::json{{"distance", val.distance_},
                      {"quality", val.quality_},
                      {"interval_number", val.interval_number_},
                      {"name", val.name_}};
}

void from_json(const nlohmann::json &j, Interval &val) {
   j.at("distance").get_to(val.distance_);
   j.at("quality").get_to(val.quality_);
   j.at("interval_number").get_to(val.interval_number_);
   j.at("name").get_to(val.name_);
}

}
