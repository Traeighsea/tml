#include "Composition.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {

bool Composition::operator==(const Composition &rhs) const {
   return name_ == rhs.GetName() && parts_ == rhs.GetParts() && bpm_ == rhs.GetBpm();
}

std::ostream &operator<<(std::ostream &out, const Composition &val) {
   out << val.ToString();
   return out;
}

std::string Composition::ToString() const {
   std::string out;
   out = "{";

   for (std::vector<Part>::const_iterator it = parts_.begin(); it != parts_.end(); it++) {
      out += "||:" + it->ToString() + ":||";
      if (it + 1 != parts_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

Part &Composition::operator[](int index) {
   if (index >= parts_.size() || index < 0) {
      std::cerr << "Composition::operator[](int index): Index out of bounds." << std::endl;
   }

   return parts_[index];
}

Part &Composition::operator[](unsigned index) {
   if (index >= parts_.size()) {
      std::cerr << "Composition::operator[](unsigned index): Index out of bounds." << std::endl;
   }

   return parts_[index];
}

Composition::Composition(std::string name, double bpm) : name_(name), bpm_(bpm) {
}

std::string Composition::GetName() const {
   return name_;
}

double Composition::GetBpm() const {
   return bpm_;
}

void Composition::SetName(std::string name) {
   name_ = name;
}

void Composition::SetBpm(Tempo tempo) {
   bpm_ = static_cast<double>(tempo);
}

void Composition::SetBpm(double bpm) {
   bpm_ = bpm;
}

std::vector<Part> Composition::GetParts() const {
   return parts_;
}

std::vector<Part> &Composition::GetParts() {
   return parts_;
}

int Composition::GetNumberOfParts() const {
   return parts_.size();
}

Part &Composition::GetPart(unsigned index) {
   if (index >= parts_.size()) {
      std::cerr << "Composition::GetPart(unsigned index): Index out of bounds." << std::endl;
   }

   return parts_[index];
}

Part Composition::GetPart(unsigned index) const {
   if (index >= parts_.size()) {
      std::cerr << "Composition::GetPart(unsigned index): Index out of bounds." << std::endl;
      return Part();
   }

   return parts_[index];
}

int Composition::GetPartIndex(std::string part_name) {
   int index = 0;
   bool found = false;
   {
      std::vector<Part>::const_iterator it = parts_.begin();
      while (it != parts_.end() && !found) {
         if (it->GetName() == part_name) {
            found = true;
         } else {
            index++;
            it++;
         }
      }
   }
   return found ? index : -1;
}

unsigned Composition::AddPart(std::string part_name, Instrument instrument, int num_measures) {
   parts_.emplace_back(Part(part_name, instrument, num_measures));
   return parts_.size() - 1;
}

void Composition::InsertPart(unsigned index, std::string part_name, Instrument instrument,
                             int num_measures) {
   std::vector<Part>::iterator it = parts_.begin();
   it += index;
   parts_.insert(it, Part(part_name, instrument, num_measures));
}

void Composition::RemovePart(unsigned index) {
   if (parts_.empty()) {
      std::cerr << "Composition::RemovePart(int index): Zero parts in container." << std::endl;
      return;
   } else if (index >= parts_.size() || index < 0) {
      std::cerr << "Composition::RemovePart(int index): "
                   "Index out of bounds."
                << std::endl;
      return;
   }
   std::vector<Part>::iterator it = parts_.begin();
   it += index;
   parts_.erase(it);
}

void Composition::RemovePart(std::string part_name) {
   bool found = false;

   std::vector<Part>::const_iterator it = parts_.begin();

   while (it != parts_.end() && !found) {
      if (it->GetName() == part_name) {
         found = true;
      } else {
         it++;
      }
   }
   if (found)
      parts_.erase(it);
}

void to_json(nlohmann::json &j, const Composition &val) {
   j = nlohmann::json{{"name", val.name_}, {"parts", val.parts_}, {"bpm", val.bpm_}};
}

void from_json(const nlohmann::json &j, Composition &val) {
   j.at("name").get_to(val.name_);
   j.at("parts").get_to(val.parts_);
   j.at("bpm").get_to(val.bpm_);
}

}