#include "Sequence.hpp"
#include "MusicTheoryCommon.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

namespace tml {
bool Sequence::operator==(const Sequence &rhs) const {
   return name_ == rhs.GetName() && sequence_ == rhs.GetNotes();
}

std::ostream &operator<<(std::ostream &out, const Sequence &val) {
   out << val.ToString();
   return out;
}

std::string Sequence::ToString() const {
   std::string out;
   if (!name_.empty()) {
      out = name_ + ": ";
   }

   out += "{";
   for (std::vector<Note>::const_iterator it = sequence_.begin(); it != sequence_.end(); it++) {
      out += "{" + it->ToString() + "}";
      if (it + 1 != sequence_.end()) {
         out += ", ";
      }
   }
   out += "}";
   return out;
}

std::string Sequence::GetName() const {
   return name_;
}

const std::vector<Note> &Sequence::GetNotes() const {
   return sequence_;
}

std::vector<Note> &Sequence::GetNotes() {
   return sequence_;
}

void Sequence::SetName(std::string name) {
   name_ = name;
}

void Sequence::AppendNote(Note new_note) {
   sequence_.emplace_back(new_note);
}

void Sequence::PrependNote(Note new_note) {
   sequence_.insert(sequence_.begin(), new_note);
}

void Sequence::Insert(int index, Note new_note, bool circular_buffer_indexing) {
   std::vector<Note>::iterator it = sequence_.begin();

   // Lol I kinda hate this
   if (circular_buffer_indexing) {
      it += CircularIndex(sequence_, index);
   } else {
      if (index >= sequence_.size() || index < 0) {
         std::cerr << "Sequence::Insert(int index, Note new_note, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   sequence_.insert(it, new_note);
}

void Sequence::Remove(int index, bool circular_buffer_indexing) {
   if (sequence_.empty()) {
      std::cerr
            << "Sequence::Remove(int index, bool circular_buffer_indexing): Zero notes in sequence."
            << std::endl;
      return;
   }

   std::vector<Note>::iterator it = sequence_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(sequence_, index);
   } else {
      if (index >= sequence_.size() || index < 0) {
         std::cerr << "Sequence::Remove(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return;
      } else {
         it += index;
      }
   }
   sequence_.erase(it);
}

void Sequence::Clear() {
   sequence_.clear();
}

Note Sequence::GetNote(int index, bool circular_buffer_indexing) const {
   if (sequence_.empty()) {
      std::cerr << "Sequence::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "sequence."
                << std::endl;
      return Note{};
   }

   std::vector<Note>::const_iterator it = sequence_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(sequence_, index);
   } else {
      if (index >= sequence_.size() || index < 0) {
         std::cerr << "Sequence::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         return Note{};
      } else {
         it += index;
      }
   }
   return *it;
}

Note &Sequence::GetNote(int index, bool circular_buffer_indexing) {
   if (sequence_.empty()) {
      std::cerr << "Sequence::GetNote(int index, bool circular_buffer_indexing): Zero notes in "
                   "sequence."
                << std::endl;
      // This should throw
      return sequence_[index];
   }

   std::vector<Note>::iterator it = sequence_.begin();

   if (circular_buffer_indexing) {
      it += CircularIndex(sequence_, index);
   } else {
      if (index >= sequence_.size() || index < 0) {
         std::cerr << "Sequence::GetNote(int index, bool circular_buffer_indexing): "
                      "Index out of bounds."
                   << std::endl;
         // This should throw
         return sequence_[index];
      } else {
         it += index;
      }
   }
   return *it;
}

int Sequence::GetSize() const {
   return sequence_.size();
}

void to_json(nlohmann::json &j, const Sequence &val) {
   j = nlohmann::json{{"name", val.name_}, {"sequence", val.sequence_}};
}

void from_json(const nlohmann::json &j, Sequence &val) {
   j.at("name").get_to(val.name_);
   j.at("sequence").get_to(val.sequence_);
}

}