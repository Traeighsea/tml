# Traeighsea's Music Theory Library (TML / tml)

A general purpose Music Theory Library designed for music theory inclined developers and academics using high level music theory concepts.

## Overview

The idea of this library is to turn music theory concepts into tangible data and functionality. From there you can experiment or play around with something that might yield interesting results for whatever you might need music theory for. Ideally the library is easy enough to use without requiring too much music theory knowledge, that would allow the user to learn from it. Additionally, there should be enough functionality for the advanced music theorists that might want to do something say with alternate tunings in a mathematical and programatic way, or say a student that wants to generate all permutations of a tone row for some twelve tone homework.

## Use Cases

Some general use cases can be found in the test directory such as generating all permutations of a given scale, chord, pitch set, etc. Concrete examples to be added here at a later date.

## Design

The current design methodology being invoked is crudely work out the features first and clean it up later. The logic is currently messy, needs work, probably less optimal than I'd like, *but it does work*. In an effort to not overwhelm myself with some of the more complex and edgecase areas of music theory, I'm implicitly making design decisions based on 12-edo that will effectively require a rewrite to support more unique tunings once my current MVP feature set is fleshed out. Eventually I will delve deeper into the specifics of how and why everything is designed as it is, but for now stay tuned.

The [Class Diagrams (WIP)](docs/ClassDiagrams.md) have been added in the docs to give a high level overview of the design, though is a work in progress currently, but gives an idea of the relationship between the classes.

## Potential Projects To Utilize This Library

[Procedural Music Generation](https://gitlab.com/Traeighsea/ProceduralMusicGeneration): A way to procedurally generate music using user configurable rulesets.

[Music Note Board (Guitar Fretboard Mappings)](https://gitlab.com/traeighseas-music-box/music-note-board): Tool to export pdfs containing various music information such as notes on a guitar fretboard for any given set of notes such as a scale, arpeggio, or more. Especially useful for visualizing open tunings.

Interactive Music Game Engine Framework: A framework to use midi in real time composing on the fly depending on in game events to manipulate any element in various ways, including improvising new melodic ideas.

Aural Trainer: Tool to help learn pitch recognition, chord recognition, chord progression recognition, frequency recognition, and any other aural ability that I could think of.

Music Theory Catalogue: Free educational tool that generates a collection of pre rendered and compiled pdf's of common music theory documents such as scales in a searchable dictionary. Could potentially fork [OpenMusicTheory](http://openmusictheory.com/contents.html) for this.

Music Theory Analyzer: A tool to aid in analysis of a composition.

## Development

The development roadmap currently consists of 4 milestones varying by size.

### Milestone 1 - MVP Feature Roadmap

The first step is to get an MVP (Minimum Viable Product) / Prototype up that would provide with the following list of information. This will be less structured development that will allow me to try implementations quickly without needing to work out the perfect solution and heavily document it.

Representation and available information for the following:

- [x] [Pitch](https://en.wikipedia.org/wiki/Pitch_(music))
- [x] [Pitch Names](https://en.wikipedia.org/wiki/Letter_notation)
- [x] [Duration](https://en.wikipedia.org/wiki/Duration_(music))
- [x] [Note Value](https://en.wikipedia.org/wiki/Note_value)
- [x] [Tuplets](https://en.wikipedia.org/wiki/Tuplet)
- [x] [Dotted Notes](https://en.wikipedia.org/wiki/Dotted_note)
- [x] [Dynamics](https://en.wikipedia.org/wiki/Dynamics_(music))
- [x] [Keys](https://en.wikipedia.org/wiki/Key_(music))
- [x] [Circle of Fifths](https://en.wikipedia.org/wiki/Circle_of_fifths)
- [x] [Intervals](https://en.wikipedia.org/wiki/Interval_(music))
- [x] [Scales](https://en.wikipedia.org/wiki/Scale_(music))
- [x] [List of Scales](https://en.wikipedia.org/wiki/List_of_musical_scales_and_modes)
- [x] [Scale Degrees](https://en.wikipedia.org/wiki/Degree_(music))
- [x] [Chords](https://en.wikipedia.org/wiki/Chord_(music))
- [x] [List of Chords](https://en.wikipedia.org/wiki/List_of_chords)
- [x] [Chord Degrees](https://en.wikipedia.org/wiki/Degree_(music))
- [ ] [Chord Progressions](https://en.wikipedia.org/wiki/Chord_progression)
- [ ] [List of (common) Chord Progressions](https://en.wikipedia.org/wiki/List_of_chords)
- [x] [Rhythm](https://en.wikipedia.org/wiki/Rhythm)
- [ ] [List of Rhythms](https://en.wikipedia.org/wiki/Rhythm)
- [x] [Time Signatures](https://en.wikipedia.org/wiki/Time_signature)
- [ ] [Harmonic Function](https://en.wikipedia.org/wiki/Function_(music))
- [x] [Pitch Classes](https://en.wikipedia.org/wiki/Pitch_class)
- [x] [Interval Classes](https://en.wikipedia.org/wiki/Interval_class)
- [x] [Pitch Sets](https://en.wikipedia.org/wiki/Set_(music))
- [x] [Tone Rows](https://en.wikipedia.org/wiki/Tone_row)
- [x] [Motif](https://en.wikipedia.org/wiki/Motif_(music))
- [x] [Composition](https://en.wikipedia.org/wiki/Musical_composition)

- [-] [List of Intervals*](https://en.wikipedia.org/wiki/List_of_pitch_intervals)
- [-] [Phrase*](https://en.wikipedia.org/wiki/Phrase_(music))
- [-] [SubSection/Period*](https://en.wikipedia.org/wiki/Period_(music))
- [-] [Section*](https://en.wikipedia.org/wiki/Section_(music))

- [ ] [Chord Alterations**](https://en.wikipedia.org/wiki/Altered_chord)
- [ ] [Chord Inversion**](https://en.wikipedia.org/wiki/Inversion_(music))
- [x] [Chord Extensions**](https://en.wikipedia.org/wiki/Extended_chord)

\*Unimplemented, need to revisit the design of these at a later date
\**Unimplemented, on a branch, need to revisit design

### Milestone 2 - Cleanup and Formal Development Process

The next step is to clean up the code and get the codebase in a state that can be considered presentable to the public.

- [x] Start Using Issue Board for Developmental Organization
- [ ] [Add Doxygen Documentation](https://www.doxygen.nl/index.html)
- [ ] Add In Depth Class Diagrams
- [ ] Cleanup Code
- [ ] Add Implementation Comments
- [ ] [Unicode Support](https://en.wikipedia.org/wiki/List_of_musical_symbols)
- [ ] Add test coverage (probably gtest)
- [x] Add JSON serialization
- [ ] Add Base64 serialization
- [ ] Revisit Implementations

### Milestone 3 - Feature Support

The next step is to support a variety of features that would be useful for integration with other widely used music languages, as well as a few features that weren't originally added as part of the MVP.

- [ ] [Scientific Pitch Notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation)
- [ ] [Helmholtz Pitch Notation](https://en.wikipedia.org/wiki/Helmholtz_pitch_notation)
- [ ] [Twelve-Tone Technique](https://en.wikipedia.org/wiki/Twelve-tone_technique)
- [ ] Theory Analysis
- [ ] [Solfege](https://en.wikipedia.org/wiki/Solf%C3%A8ge)
- [ ] [MIDI Support](https://en.wikipedia.org/wiki/MIDI)
- [ ] [MusicXML Support](https://en.wikipedia.org/wiki/MusicXML)

### Milestone 4 - Microtonal Support

The next step would be a case study in microtonal music and composition of such music. I do not know much about alternate tunings or microtonal music, but it has been fascinating me as of late and I want to learn more.

- [ ] Alternate Tuning Support Redesign
- [ ] [Just Intonation](https://en.wikipedia.org/wiki/Just_intonation)
- [ ] [Arbitrary Number Equal Temperament](https://en.wikipedia.org/wiki/Equal_temperament)
- [ ] [Xenharmonic](https://en.wikipedia.org/wiki/Xenharmonic_music)
- [ ] Custom Tunings
- [ ] Custom Interval Names
- [ ] Alternate Tuning Notation [(Example: Diamond-mos Notation)](https://en.xen.wiki/w/Diamond-mos_notation)
- [ ] More (As I learn more about this fascinating side of music theory)

### Future

This is when I would want to seek user feedback and feature requests.

- [ ] Lua Bindings
- [ ] Python Bindings
- [ ] JUCE Integration

## Resources Used

- [Tonal Harmony 7th Edition](https://www.mheducation.com/highered/product/tonal-harmony-kostka-payne/M9781259447099.authorbio.html)
- [Musimathics, Volume 1: The Mathematical Foundations of Music](https://mitpress.mit.edu/books/musimathics-volume-1)
- [Musimathics, Volume 2: The Mathematical Foundations of Music](https://mitpress.mit.edu/books/musimathics-volume-2)
- [Music Theory for the 21st-Century Classroom](https://musictheory.pugetsound.edu/mt21c/MusicTheory.html)
- [OpenMusicTheory](http://openmusictheory.com/contents.html)
- [MusicTheory](https://www.musictheory.net/)
- [Wikipedia](https://en.wikipedia.org/wiki/Music_theory)
- [Scala](https://www.huygens-fokker.org/scala/)
- [Sound Programming](https://soundprogramming.net/)

Special thanks to various music professors and teachers I've had over the years.
